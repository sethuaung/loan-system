/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : mkt_demo

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 01/09/2022 20:04:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 274 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2015_08_04_131614_create_settings_table', 1);
INSERT INTO `migrations` VALUES (4, '2017_09_10_033847_create_holiday_schedules_table', 1);
INSERT INTO `migrations` VALUES (5, '2018_10_29_101242_create_general_journals_table', 1);
INSERT INTO `migrations` VALUES (6, '2018_10_29_101702_create_general_journal_details_table', 1);
INSERT INTO `migrations` VALUES (7, '2018_11_15_122652_create_classes_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_01_07_013932_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (9, '2019_01_07_021418_create_product_categories_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_01_07_025538_create_account_sections_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_01_07_030416_create_account_charts_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_01_07_054725_create_currencies_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_01_07_055317_create_taxes_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_01_07_060921_create_payment_terms_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_01_07_064556_create_supplies_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_01_13_064202_create_jobs_table', 1);
INSERT INTO `migrations` VALUES (17, '2019_01_14_192058_create_clients_table', 1);
INSERT INTO `migrations` VALUES (18, '2019_01_14_200834_create_working_status_table', 1);
INSERT INTO `migrations` VALUES (19, '2019_01_15_204330_create_guarantors_table', 1);
INSERT INTO `migrations` VALUES (20, '2019_01_16_190528_create_compulsory_products_table', 1);
INSERT INTO `migrations` VALUES (21, '2019_01_16_191323_create_loan_calculator_table', 1);
INSERT INTO `migrations` VALUES (22, '2019_01_16_200642_create_compulsory_product_types_table', 1);
INSERT INTO `migrations` VALUES (23, '2019_01_17_120530_create_default_saving_deposits_table', 1);
INSERT INTO `migrations` VALUES (24, '2019_01_17_123602_create_default_saving_interests_table', 1);
INSERT INTO `migrations` VALUES (25, '2019_01_17_123744_create_default_saving_interest_payables_table', 1);
INSERT INTO `migrations` VALUES (26, '2019_01_17_123828_create_default_saving_withdrawals_table', 1);
INSERT INTO `migrations` VALUES (27, '2019_01_17_123901_create_default_saving_interest_withdrawals_table', 1);
INSERT INTO `migrations` VALUES (28, '2019_01_18_185226_create_charges_table', 1);
INSERT INTO `migrations` VALUES (29, '2019_01_21_182118_create_loan_products_table', 1);
INSERT INTO `migrations` VALUES (30, '2019_01_22_095402_add_currency_to_journal_details', 1);
INSERT INTO `migrations` VALUES (31, '2019_01_23_030127_add_type_to_journals', 1);
INSERT INTO `migrations` VALUES (32, '2019_01_23_185607_create_capitals_table', 1);
INSERT INTO `migrations` VALUES (33, '2019_01_23_190310_create_asset_types_table', 1);
INSERT INTO `migrations` VALUES (34, '2019_01_23_191852_create_assets_table', 1);
INSERT INTO `migrations` VALUES (35, '2019_01_23_193209_create_current_asset_values_table', 1);
INSERT INTO `migrations` VALUES (36, '2019_01_23_204005_create_tests_table', 1);
INSERT INTO `migrations` VALUES (37, '2019_01_23_205643_create_collateral_types_table', 1);
INSERT INTO `migrations` VALUES (38, '2019_01_24_185546_create_audits_table', 1);
INSERT INTO `migrations` VALUES (39, '2019_01_24_190657_create_charge_strings_table', 1);
INSERT INTO `migrations` VALUES (40, '2019_01_24_201952_create_expenses_table', 1);
INSERT INTO `migrations` VALUES (41, '2019_01_24_203828_create_expense_types_table', 1);
INSERT INTO `migrations` VALUES (42, '2019_01_28_072324_create_close_alls_table', 1);
INSERT INTO `migrations` VALUES (43, '2019_01_28_172451_create_account_sub_section_table', 1);
INSERT INTO `migrations` VALUES (44, '2019_01_30_085407_create_branches_table', 1);
INSERT INTO `migrations` VALUES (45, '2019_01_30_184300_create_loans_table', 1);
INSERT INTO `migrations` VALUES (46, '2019_01_30_190843_create_loan_objectives_table', 1);
INSERT INTO `migrations` VALUES (47, '2019_01_30_191136_create_transaction_types_table', 1);
INSERT INTO `migrations` VALUES (48, '2019_01_30_194817_create_loan_product_branches_table', 1);
INSERT INTO `migrations` VALUES (49, '2019_01_31_195429_create_transfers_table', 1);
INSERT INTO `migrations` VALUES (50, '2019_01_31_201844_create_loan_calculate_table', 1);
INSERT INTO `migrations` VALUES (51, '2019_01_31_214723_create_surveys_table', 1);
INSERT INTO `migrations` VALUES (52, '2019_01_31_220749_create_ownerships_table', 1);
INSERT INTO `migrations` VALUES (53, '2019_01_31_222251_create_ownership_farmlands_table', 1);
INSERT INTO `migrations` VALUES (54, '2019_02_01_014033_create_center_leaders_table', 1);
INSERT INTO `migrations` VALUES (55, '2019_02_01_124534_add_address', 1);
INSERT INTO `migrations` VALUES (56, '2019_02_05_220103_create_loan_payments_table', 1);
INSERT INTO `migrations` VALUES (57, '2019_02_11_104210_create_supply_address_table', 1);
INSERT INTO `migrations` VALUES (58, '2019_02_12_205055_create_paid_disbursements_table', 1);
INSERT INTO `migrations` VALUES (59, '2019_02_13_192416_create_group_loans_table', 1);
INSERT INTO `migrations` VALUES (60, '2019_02_13_194124_create_group_loan_details_table', 1);
INSERT INTO `migrations` VALUES (61, '2019_02_13_203957_create_cash_withdrawals_table', 1);
INSERT INTO `migrations` VALUES (62, '2019_02_21_204118_create_loan_charge_table', 1);
INSERT INTO `migrations` VALUES (63, '2019_02_21_204224_create_loan_compulsory_table', 1);
INSERT INTO `migrations` VALUES (64, '2019_02_26_143134_add_status_to_product_categories', 1);
INSERT INTO `migrations` VALUES (65, '2019_02_27_225546_create_loan_deposits_table', 1);
INSERT INTO `migrations` VALUES (66, '2019_02_28_114044_create_deposit_service_charges_table', 1);
INSERT INTO `migrations` VALUES (67, '2019_03_01_112915_create_disbursement_service_charges_table', 1);
INSERT INTO `migrations` VALUES (68, '2019_03_07_131844_create_compulsory_saving_transaction_table', 1);
INSERT INTO `migrations` VALUES (69, '2019_03_13_192541_create_payment_service_charges_table', 1);
INSERT INTO `migrations` VALUES (70, '2019_03_22_061941_create_notifications_table', 1);
INSERT INTO `migrations` VALUES (71, '2019_03_23_002756_add_user_by_to_all_table', 1);
INSERT INTO `migrations` VALUES (72, '2019_03_28_203103_create_user_centers_table', 1);
INSERT INTO `migrations` VALUES (73, '2019_03_28_203114_create_user_branches_table', 1);
INSERT INTO `migrations` VALUES (74, '2019_04_02_092324_create_client_guarantor_table', 1);
INSERT INTO `migrations` VALUES (75, '2019_04_02_095422_create_client_branch_seq_table', 1);
INSERT INTO `migrations` VALUES (76, '2019_04_04_203437_create_group_deposits_table', 1);
INSERT INTO `migrations` VALUES (77, '2019_04_04_204215_create_group_deposit_details_table', 1);
INSERT INTO `migrations` VALUES (78, '2019_04_26_112820_create_nrc_prefix_table', 1);
INSERT INTO `migrations` VALUES (79, '2019_05_17_113500_create_account_chat_external_details_table', 1);
INSERT INTO `migrations` VALUES (80, '2019_05_18_113500_create_account_chat_externals_table', 1);
INSERT INTO `migrations` VALUES (81, '2019_05_24_131818_add_total_service', 1);
INSERT INTO `migrations` VALUES (82, '2019_05_24_132920_add_total_service_to_disburse', 1);
INSERT INTO `migrations` VALUES (83, '2019_05_24_154306_add_total_service_to_payment', 1);
INSERT INTO `migrations` VALUES (84, '2019_05_25_100347_create_service_charge_transaction_table', 1);
INSERT INTO `migrations` VALUES (85, '2019_05_27_092903_add_fields_to_charge_transaction', 1);
INSERT INTO `migrations` VALUES (86, '2019_05_29_090713_create_group_loan_transactions_table', 1);
INSERT INTO `migrations` VALUES (87, '2019_05_29_100225_add_group_tran_id', 1);
INSERT INTO `migrations` VALUES (88, '2019_05_29_132920_add_override_to_compulsory_products', 1);
INSERT INTO `migrations` VALUES (89, '2019_05_29_142920_add_override_to_loan_compulsory', 1);
INSERT INTO `migrations` VALUES (90, '2019_05_30_123020_add_field_to_cash_withdrawals', 1);
INSERT INTO `migrations` VALUES (91, '2019_05_30_132920_add_field_to_saving_transaction', 1);
INSERT INTO `migrations` VALUES (92, '2019_05_30_132920_add_field_to_saving_transactions', 1);
INSERT INTO `migrations` VALUES (93, '2019_05_31_095226_create_compulsory_accrue_interests_table', 1);
INSERT INTO `migrations` VALUES (94, '2019_06_02_142920_add_tran_reference_to_general_journals', 1);
INSERT INTO `migrations` VALUES (95, '2019_06_09_110354_add_section_id_ext', 1);
INSERT INTO `migrations` VALUES (96, '2019_06_09_110605_add_external_acc_id', 1);
INSERT INTO `migrations` VALUES (97, '2019_06_11_105910_add_user_code_to_users_table', 1);
INSERT INTO `migrations` VALUES (98, '2019_06_19_160316_create_customer_groups_table', 1);
INSERT INTO `migrations` VALUES (99, '2019_06_20_140744_add_customer_group_id_ext', 1);
INSERT INTO `migrations` VALUES (100, '2019_06_20_152439_create_employee_status_table', 1);
INSERT INTO `migrations` VALUES (101, '2019_06_21_231942_add_loan_cycle', 1);
INSERT INTO `migrations` VALUES (102, '2019_06_29_114537_add_priciple_to_compulsory', 1);
INSERT INTO `migrations` VALUES (103, '2019_07_02_104655_create_loan_cycle_table', 1);
INSERT INTO `migrations` VALUES (104, '2019_07_02_134508_add_expense_type', 1);
INSERT INTO `migrations` VALUES (105, '2019_07_02_141913_add_cash_acc_id_journal', 1);
INSERT INTO `migrations` VALUES (106, '2019_07_09_105655_create_shareholders_table', 1);
INSERT INTO `migrations` VALUES (107, '2019_07_16_201845_add_first_date', 1);
INSERT INTO `migrations` VALUES (108, '2019_07_17_113116_add_monthly_base', 1);
INSERT INTO `migrations` VALUES (109, '2019_08_01_103738_add_more_fields_calculate', 1);
INSERT INTO `migrations` VALUES (110, '2019_08_01_111002_add_paid_field', 1);
INSERT INTO `migrations` VALUES (111, '2019_08_07_161813_add_client_prefix_code', 1);
INSERT INTO `migrations` VALUES (112, '2019_08_16_093414_add_client_id_format', 1);
INSERT INTO `migrations` VALUES (113, '2019_08_22_090708_add_expense_attach', 1);
INSERT INTO `migrations` VALUES (114, '2019_08_22_202914_add_loan_request', 1);
INSERT INTO `migrations` VALUES (115, '2019_08_25_194052_add_seq_to_group_tran', 1);
INSERT INTO `migrations` VALUES (116, '2019_08_27_142740_create_note_tickets_table', 1);
INSERT INTO `migrations` VALUES (117, '2019_09_02_093815_add_acc_found_to_loan_product', 1);
INSERT INTO `migrations` VALUES (118, '2019_09_02_112122_create_paid_support_fund_table', 1);
INSERT INTO `migrations` VALUES (119, '2019_09_02_112136_create_paid_support_fund_details_table', 1);
INSERT INTO `migrations` VALUES (120, '2019_09_02_192528_add_support-fund-seq', 1);
INSERT INTO `migrations` VALUES (121, '2019_09_03_115737_add_plan_disbuse_date', 1);
INSERT INTO `migrations` VALUES (122, '2019_09_03_163137_add_cancel_type', 1);
INSERT INTO `migrations` VALUES (123, '2019_09_03_191700_add_update_fund_type', 1);
INSERT INTO `migrations` VALUES (124, '2019_09_04_091243_add_fund_detail_fields', 1);
INSERT INTO `migrations` VALUES (125, '2019_09_04_144806_add_supporting_fund', 1);
INSERT INTO `migrations` VALUES (126, '2019_09_04_170926_add_branch_id', 1);
INSERT INTO `migrations` VALUES (127, '2019_09_05_202833_add_dead_writeoff_status', 1);
INSERT INTO `migrations` VALUES (128, '2019_09_07_182752_add_number', 1);
INSERT INTO `migrations` VALUES (129, '2019_09_07_191503_add_exact_interests', 1);
INSERT INTO `migrations` VALUES (130, '2019_09_23_112938_add_account_parent_id_to_account_chart_external_details_table', 1);
INSERT INTO `migrations` VALUES (131, '2019_09_30_091604_add_more_loan_import_list', 1);
INSERT INTO `migrations` VALUES (132, '2019_10_08_113033_add_cash_account_id_to_branches_table', 1);
INSERT INTO `migrations` VALUES (133, '2019_10_08_154458_add_branch_id_to_paid_disbursements_table', 1);
INSERT INTO `migrations` VALUES (134, '2019_10_14_113126_add_loan_product_id', 1);
INSERT INTO `migrations` VALUES (135, '2019_10_15_143650_add_interest_method', 1);
INSERT INTO `migrations` VALUES (136, '2019_10_23_000001_add_disburse_by_to_paid_disbursements', 1);
INSERT INTO `migrations` VALUES (137, '2019_10_25_210743_add_payment_by_to_loan_payment', 1);
INSERT INTO `migrations` VALUES (138, '2019_11_05_100501_add_branch_id_to_compulsory', 1);
INSERT INTO `migrations` VALUES (139, '2019_11_07_133649_add_interest_type_loan', 1);
INSERT INTO `migrations` VALUES (140, '2019_11_08_195814_add_schedule_type', 1);
INSERT INTO `migrations` VALUES (141, '2019_11_13_083419_create_payment_history_table', 1);
INSERT INTO `migrations` VALUES (142, '2019_11_16_093920_add_penalty_schedule', 1);
INSERT INTO `migrations` VALUES (143, '2019_11_19_103924_create_loan_schedule_backup_table', 1);
INSERT INTO `migrations` VALUES (144, '2019_11_19_162545_create_delete_payment_history_backup_table', 1);
INSERT INTO `migrations` VALUES (145, '2019_11_20_111231_add_balance_payment_history', 1);
INSERT INTO `migrations` VALUES (146, '2019_11_25_112001_create_capital_withdraws_table', 1);
INSERT INTO `migrations` VALUES (147, '2019_11_25_113808_add_capital_withdraw_type', 1);
INSERT INTO `migrations` VALUES (148, '2019_11_28_000001_add_contact_person_to_clients', 1);
INSERT INTO `migrations` VALUES (149, '2019_11_28_000002_add_guarantor2_to_loans', 1);
INSERT INTO `migrations` VALUES (150, '2019_11_28_095113_add_group_id_schedule', 1);
INSERT INTO `migrations` VALUES (151, '2019_11_28_145721_add_loan_product_id_schedule', 1);
INSERT INTO `migrations` VALUES (152, '2019_12_02_000001_add_id_format_to_group_loan', 1);
INSERT INTO `migrations` VALUES (153, '2019_12_09_162241_create_loan_transfers_table', 1);
INSERT INTO `migrations` VALUES (154, '2019_12_09_164845_add_family_phone_number_to_clents_table', 1);
INSERT INTO `migrations` VALUES (155, '2019_12_09_165205_add_work_phone_number_to_employee_status_table', 1);
INSERT INTO `migrations` VALUES (156, '2019_12_10_102726_add_working_experience_to_employee_status_table', 1);
INSERT INTO `migrations` VALUES (157, '2019_12_12_114830_add_multiple_column_to_clients', 1);
INSERT INTO `migrations` VALUES (158, '2019_12_20_094556_add_optional_phone_number_to_clients', 1);
INSERT INTO `migrations` VALUES (159, '2019_12_24_084151_create_saving_products_table', 1);
INSERT INTO `migrations` VALUES (160, '2019_12_24_135723_create_savings_table', 1);
INSERT INTO `migrations` VALUES (161, '2019_12_24_141654_create_saving_transactions_table', 1);
INSERT INTO `migrations` VALUES (162, '2019_12_24_143407_create_saving_schedules_table', 1);
INSERT INTO `migrations` VALUES (163, '2019_12_25_082215_create_saving_deposits', 1);
INSERT INTO `migrations` VALUES (164, '2019_12_25_082622_create_saving_accrued_interests', 1);
INSERT INTO `migrations` VALUES (165, '2019_12_25_082946_create_saving_withdrawals', 1);
INSERT INTO `migrations` VALUES (166, '2019_12_25_083908_create_saving_compound_interests', 1);
INSERT INTO `migrations` VALUES (167, '2019_12_27_081945_create_saving_calculate', 1);
INSERT INTO `migrations` VALUES (168, '2019_12_28_105522_add_paid_to_loans_table', 1);
INSERT INTO `migrations` VALUES (169, '2019_12_28_115722_create_client_histories_table', 1);
INSERT INTO `migrations` VALUES (170, '2020_01_02_101247_add_refrence_column_to_transfers_table', 1);
INSERT INTO `migrations` VALUES (171, '2020_01_02_232330_add_seq_to_transfer', 1);
INSERT INTO `migrations` VALUES (172, '2020_01_13_114057_add_address_column_to_employee_status_table', 1);
INSERT INTO `migrations` VALUES (173, '2020_01_14_132126_add_company_address_clients', 1);
INSERT INTO `migrations` VALUES (174, '2020_01_18_122107_create_frd_account_details_table', 1);
INSERT INTO `migrations` VALUES (175, '2020_01_18_131236_create_frd_account_settings_table', 1);
INSERT INTO `migrations` VALUES (176, '2020_01_21_000001_add_address1_to_branches', 1);
INSERT INTO `migrations` VALUES (177, '2020_01_21_113354_create_inspectors_table', 1);
INSERT INTO `migrations` VALUES (178, '2020_01_21_151327_add_inspector_to_loans_table', 1);
INSERT INTO `migrations` VALUES (179, '2020_01_23_110756_add_remark_to_transfers_table', 1);
INSERT INTO `migrations` VALUES (180, '2020_01_23_201735_create_loan_write_offs_table', 1);
INSERT INTO `migrations` VALUES (181, '2020_01_28_201657_add_type_loan_write_offs', 1);
INSERT INTO `migrations` VALUES (182, '2020_01_29_103157_add_condition_to_clients_table', 1);
INSERT INTO `migrations` VALUES (183, '2020_01_31_204458_create_frd_account_settings2_table', 1);
INSERT INTO `migrations` VALUES (184, '2020_01_31_204622_create_frd_account_details2_table', 1);
INSERT INTO `migrations` VALUES (185, '2020_02_01_094527_add_lat_lng_to_clients', 1);
INSERT INTO `migrations` VALUES (186, '2020_02_03_114643_add_user_id_to_center_leaders', 1);
INSERT INTO `migrations` VALUES (187, '2020_02_14_091458_create_client_pending_table', 1);
INSERT INTO `migrations` VALUES (188, '2020_02_14_092134_create_loan_pending_table', 1);
INSERT INTO `migrations` VALUES (189, '2020_02_14_100556_add_status_to_client_pending', 1);
INSERT INTO `migrations` VALUES (190, '2020_02_14_153629_add_client_pending_id_to_clients', 1);
INSERT INTO `migrations` VALUES (191, '2020_02_14_163918_add_photo_to_clients', 1);
INSERT INTO `migrations` VALUES (192, '2020_02_18_193728_create_loan_payment_tem_table', 1);
INSERT INTO `migrations` VALUES (193, '2020_02_19_130302_add_status_to_loan_payment_tem', 1);
INSERT INTO `migrations` VALUES (194, '2020_02_19_200601_add_status_to_loan', 1);
INSERT INTO `migrations` VALUES (195, '2020_03_03_141407_add_paid_type_deposit', 1);
INSERT INTO `migrations` VALUES (196, '2020_03_03_142847_add_saving_number_savings', 1);
INSERT INTO `migrations` VALUES (197, '2020_03_04_192119_create_saving_schedules_table2', 1);
INSERT INTO `migrations` VALUES (198, '2020_03_06_142011_add_note_check_date_to_loan_payment_tem', 1);
INSERT INTO `migrations` VALUES (199, '2020_03_06_202829_add_interest_saving_product', 1);
INSERT INTO `migrations` VALUES (200, '2020_03_07_000641_add_column_to_address', 1);
INSERT INTO `migrations` VALUES (201, '2020_03_11_170442_add_columns_to_users', 1);
INSERT INTO `migrations` VALUES (202, '2020_03_16_154821_add_remark_to_loan_disbursement_calculate_table', 1);
INSERT INTO `migrations` VALUES (203, '2020_03_17_162641_add_paid_to_users_table', 1);
INSERT INTO `migrations` VALUES (204, '2020_03_17_170746_add_paid_to_loan_schedule_table', 1);
INSERT INTO `migrations` VALUES (205, '2020_03_21_090551_add_sequences_to_users_table', 1);
INSERT INTO `migrations` VALUES (206, '2020_03_23_120121_add_excel_to_loans_table', 1);
INSERT INTO `migrations` VALUES (207, '2020_03_23_164427_add_excel_to_clients_table', 1);
INSERT INTO `migrations` VALUES (208, '2020_03_23_171003_add_excel_to_loan_payments_table', 1);
INSERT INTO `migrations` VALUES (209, '2020_03_25_094321_add_excel_to_general_journals_table', 1);
INSERT INTO `migrations` VALUES (210, '2020_03_30_170804_add_payment_pending_id', 1);
INSERT INTO `migrations` VALUES (211, '2020_04_08_163040_add_is_mobile_to_loan_disbursement_calculate', 1);
INSERT INTO `migrations` VALUES (212, '2020_04_22_000001_add_new_saving_product', 1);
INSERT INTO `migrations` VALUES (213, '2020_04_29_000001_add_new_savings', 1);
INSERT INTO `migrations` VALUES (214, '2020_05_04_105148_add_late_repayment_to_loan_payments_table', 1);
INSERT INTO `migrations` VALUES (215, '2020_05_18_091232_add_enum_value_to_loan_table', 1);
INSERT INTO `migrations` VALUES (216, '2020_05_27_103613_add_repayment_term_value_to_loan_calculator_table', 1);
INSERT INTO `migrations` VALUES (217, '2020_06_09_000001_add_purchase_payment_to_general_journals', 1);
INSERT INTO `migrations` VALUES (218, '2020_06_09_000001_add_type_saving_to_general_journals', 1);
INSERT INTO `migrations` VALUES (219, '2020_06_10_095827_add_attach_document_to_transfer_table', 1);
INSERT INTO `migrations` VALUES (220, '2020_06_15_141512_create_products_table', 1);
INSERT INTO `migrations` VALUES (221, '2020_06_15_145436_add_product_id_loans', 1);
INSERT INTO `migrations` VALUES (222, '2020_06_15_160134_add_product_type_journal', 1);
INSERT INTO `migrations` VALUES (223, '2020_06_17_130208_add_product_id_disbursement', 1);
INSERT INTO `migrations` VALUES (224, '2020_06_17_130216_add_product_id_deposit', 1);
INSERT INTO `migrations` VALUES (225, '2020_06_18_000001_add_branch_id_saving_transaction', 1);
INSERT INTO `migrations` VALUES (226, '2020_06_19_000001_add_dob_to_client_pending', 1);
INSERT INTO `migrations` VALUES (227, '2020_06_23_152543_create_payments_table', 1);
INSERT INTO `migrations` VALUES (228, '2020_06_23_153123_create_payment_details_table', 1);
INSERT INTO `migrations` VALUES (229, '2020_06_23_162030_create_units_table', 1);
INSERT INTO `migrations` VALUES (230, '2020_06_23_182030_create_products2_table', 1);
INSERT INTO `migrations` VALUES (231, '2020_06_23_184148_create_warehouses_table', 1);
INSERT INTO `migrations` VALUES (232, '2020_06_23_191159_create_branch_marks_table', 1);
INSERT INTO `migrations` VALUES (233, '2020_06_23_193146_create_stock_movements_table', 1);
INSERT INTO `migrations` VALUES (234, '2020_06_24_151906_create_product_unit_variants_table', 1);
INSERT INTO `migrations` VALUES (235, '2020_06_24_155554_create_product_warehouses_table', 1);
INSERT INTO `migrations` VALUES (236, '2020_06_25_090739_create_purchases_table', 1);
INSERT INTO `migrations` VALUES (237, '2020_06_25_100130_create_ap_trains_table', 1);
INSERT INTO `migrations` VALUES (238, '2020_06_25_101343_create_supply_deposit_table', 1);
INSERT INTO `migrations` VALUES (239, '2020_06_25_101839_create_supply_payment_method_table', 1);
INSERT INTO `migrations` VALUES (240, '2020_06_25_114222_add_round_id_gen', 1);
INSERT INTO `migrations` VALUES (241, '2020_06_25_114222_add_round_id_gen_de', 1);
INSERT INTO `migrations` VALUES (242, '2020_06_25_145704_create_pre_fix_seq_table', 1);
INSERT INTO `migrations` VALUES (243, '2020_06_25_151320_create_stock_movement_serials_table', 1);
INSERT INTO `migrations` VALUES (244, '2020_06_26_140213_create_opens_table', 1);
INSERT INTO `migrations` VALUES (245, '2020_06_26_141110_create_attributes_table', 1);
INSERT INTO `migrations` VALUES (246, '2020_06_26_144550_create_price_groups_table', 1);
INSERT INTO `migrations` VALUES (247, '2020_06_26_145316_create_category_warehouses_table', 1);
INSERT INTO `migrations` VALUES (248, '2020_06_26_151548_create_user_warehouses_table', 1);
INSERT INTO `migrations` VALUES (249, '2020_06_29_162900_add_deposit_disbursement', 1);
INSERT INTO `migrations` VALUES (250, '2020_06_30_102829_add_business_proposal_to_loans_table', 1);
INSERT INTO `migrations` VALUES (251, '2020_06_30_112440_add_annual_income_to_grauntators_table', 1);
INSERT INTO `migrations` VALUES (252, '2020_07_03_155030_add_product_id_loan_payment', 1);
INSERT INTO `migrations` VALUES (253, '2020_07_16_134822_add_general_type_journal', 1);
INSERT INTO `migrations` VALUES (254, '2020_07_16_135528_create_general_journal_backups_table', 1);
INSERT INTO `migrations` VALUES (255, '2020_07_22_000001_add_marital_status_to_client_pending', 1);
INSERT INTO `migrations` VALUES (256, '2020_07_24_000001_create_general_journal_details_tem_table', 1);
INSERT INTO `migrations` VALUES (257, '2020_07_24_201515_create_loan_schedule_tem_table', 1);
INSERT INTO `migrations` VALUES (258, '2020_07_27_102146_create_loan_outstanding_tem_table', 1);
INSERT INTO `migrations` VALUES (259, '2020_08_10_160921_add_dead_date_to_paid_support_fund_table', 1);
INSERT INTO `migrations` VALUES (260, '2020_08_29_125118_add_old_branch_to_loan_transfers_table', 1);
INSERT INTO `migrations` VALUES (261, '2020_09_03_143323_create_transaction_histories_table', 1);
INSERT INTO `migrations` VALUES (262, '2020_09_05_000001_add_branch_to_compulsory_accrue_interest_table', 1);
INSERT INTO `migrations` VALUES (263, '2020_09_05_000002_add_branch_to_compulsory_saving_transaction_table', 1);
INSERT INTO `migrations` VALUES (264, '2020_09_07_142726_add_description_and_amount_to_transaction_histories_table', 1);
INSERT INTO `migrations` VALUES (265, '2020_09_11_000001_add_ref_to_compulsory_saving_transaction_table', 1);
INSERT INTO `migrations` VALUES (266, '2020_09_29_222034_add_excel_to_cash_withdrawal_table', 1);
INSERT INTO `migrations` VALUES (267, '2020_11_09_160421_add_api_columns_to_clients_table', 1);
INSERT INTO `migrations` VALUES (268, '2020_11_11_105502_add_print_to_saving_transactions_table', 1);
INSERT INTO `migrations` VALUES (269, '2020_11_17_134745_add_new_branch_to_loan_payments_table', 1);
INSERT INTO `migrations` VALUES (270, '2020_12_01_111831_add_old_branch_to_loans_table', 1);
INSERT INTO `migrations` VALUES (271, '2020_12_23_095730_add_interest_compound_enum_to_savings_table', 1);
INSERT INTO `migrations` VALUES (272, '2021_02_16_144708_add_second_branch_id_to_journals_table', 1);
INSERT INTO `migrations` VALUES (273, '2022_09_01_194850_add_guarantor3_to_loans', 1);

SET FOREIGN_KEY_CHECKS = 1;
