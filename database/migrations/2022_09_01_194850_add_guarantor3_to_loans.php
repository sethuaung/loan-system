<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Branch;

class AddGuarantor3ToLoans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $branches = Branch::All();
        foreach ($branches as $branch){
            if (!Schema::hasColumn("loans_$branch->id", 'guarantor3_id'))
            {
                Schema::table("loans_$branch->id", function (Blueprint $table) {
                    $table->text('guarantor3_id')->nullable();
                });
            }
        }
       

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loans', function (Blueprint $table) {
          
        });
    }
}
