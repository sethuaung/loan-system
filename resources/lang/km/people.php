<?php

return [
    'gov_id' => 'អត្តលេខមន្ត្រីរាជការ',
    'full_name' => 'នាម និងគោត្តនាម',
    'name_en' => 'ឈ្មោះឡាតាំង',
    'gender' => 'ភេទ',
    'sex' => 'ភេទ',
    'phone_number' => 'លេខទូរស័ព្ទ',
    'email' => 'អ៊ីមែល',
    'status' => 'ស្ថានភាព',
    'address' => 'ឤសយដ្ឋាន',

    'marital_status' => 'ស្ថានភាពគ្រួសារ',
    'date_of_birth' => 'ថ្ងៃខែឆ្នាំកំណើត',
    'ministry' => 'ក្រសួង',
    'general_department' => 'អគ្គនាយកដ្ឋាន',
    'department' => 'នាយកដ្ឋាន',
    'office' => 'ការិយាល័យ',
    'image' => 'រូបថត',
    'position' => 'មុខតំណែង',
    'note' => 'កំណត់សម្គាល់ផ្សេងៗ',
    'title' => 'តួនាទី',
    'description' => 'បរិយាយ',

    //--------------translate on tabs -------------------
    'people_info'=>'ព័ត៌មានផ្ទាល់ខ្លួន',
    'people_work'=>'ការងារ',
    'people_address'=>'អាសយដ្ឋាន',
    'select_a_ministry'=>'សូមជ្រើសរើសក្រសួង',
    'select_a_general_department'=>'សូមជ្រើសរើសអគ្គនាយកដ្ឋាន',
    'select_a_department'=>'សូមជ្រើសរើសនាយកដ្ឋាន',
    'select_a_office'=>'សូមជ្រើសរើសការិយាល័',
    'select_a_province'=>'សូមជ្រើសរើសរាជធានី/េខត្ត',
    'select_a_district'=>'សូមជ្រើសរើសក្រុង/ស្រុក/ខណ្ឌ',
    'select_a_commune'=>'សូមជ្រើសរើសឃុំ/សង្កាត់',
    'select_a_village'=>'សូមជ្រើសរើសភូមិ',




];