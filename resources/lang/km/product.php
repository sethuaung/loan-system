<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'title' => 'ឈ្មោះ',
    'parent' => 'អនុនៃ',
    'category_type' => 'ប្រភេទប្រភេទ',
    'description' => 'ការពិពណ៌នា',
    'category_id' => 'ប្រភេទ',
    'status' => 'ស្ថានភាព',


    'location_name' => 'ឈ្មោះទីតាំង',
    'address' => 'អាសយដ្ឋាន',
    'name' => 'ឈ្មោះ',
    'position' => 'តំណែង',
    'phone' => 'ទូរស័ព្ទ',
    'e-mail' => 'អ៊ីម៉ែល',
    'contact' => 'ទំនាក់ទំនង',

    'note' => 'ចំណាំ',
    'add_serial' => 'បញ្ចូលលេខកូដ',


    'serial_code' => 'Serial Code',
    'location' => 'ទីកន្លែង',
    'color' => 'ពណ៌',
    'size' => 'ទំហំ',

    'qty' => 'បរិមាណ',
    'cost' => 'តម្លៃ',
    'country' => 'ប្រទេសផលិត',
    'manufacturer_by' => 'ក្រុមហ៊ុនផលិត',
    'currency' => 'រូបិយប័ណ្ណ',
    'source_name' => 'ប្រភព',
    'delivery_date' => 'ថ្ងៃនាំចូល',
    'exchange_rate' => 'អត្រាប្ដូរប្រាក់',
    'image' => 'រូបភាព',
    'attachment_files' => 'ឯកសារភ្ជាប់',


    'serial' => 'លេខកូដស៊េរី',
    'product' => 'ផលិតផល',
    'assign_to' => 'អ្នកទទួល',
    'assign_date' => 'កាលបរិច្ឆេទ',
    'serial_status' => 'ស្ថា.ពេលចេញ',
    'assign_note' => 'កំណត់ចំណាំ',
    'approve_note' => 'ចំំណាំការអនុម័ត',
    'approve_by' => 'អនុម័តដោយ',
    'return_by' => 'ទទួលមកវិញដោយ',
    'return_date' => 'ថ្ងៃប្រគល់អោយវិញ',
    'return_note' => 'សម្គាល់ពេលសង',
    'return_serial_status' => 'ស្ថានភាពពេលសង',
    'approve_date' => 'ថ្ងៃអនុម័ត',
    'assign_by' => 'អ្នកប្រគល់',
    'approve​_​by' => 'អនុម័តដោយ',
    'serials' => 'កូដស៊េរី',


    'manufacturer' => 'ក្រុមហ៊ុនផលិត',
    'supply' => 'អ្នកផ្គត់ផ្គង់',
    'asset_id' => 'Asset Code',

    'fund' => 'Fund',
    'reference_no' => 'Reference No',
    'contract_no' => 'Contract No',
    'feature' => 'លក្ខណៈពិសេស',
    'id' => 'ID',
    'transaction' => 'ដំណើរការ',
    'activity' => 'សម្មភាព',
    'return' => 'ពិនិត្យ',
    'to_date'=>'មកដល់ពេលនេះ',
    'between'=>'ចន្លោះពេល',


    'bon' => 'BON-ល្អ',
    'moy' => 'MOY-មធ្យម',
    'mau' => 'MAU-ខូចជួសជុលបាន',
    'ref' => 'REF ខូច100%',


];
