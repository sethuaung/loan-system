<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'house_no' => 'No.',
    'street_no' => 'Street',
    'province_label' => 'State',
    'province' => 'State',
    'district_1' => '',
    'district_2' => '',
    'commune_label' => 'Township',
    'commune_1' => '',
    'commune_2' => '',
    'village' => '',
    'city_1' => '',
    'city_2' => '',
];
