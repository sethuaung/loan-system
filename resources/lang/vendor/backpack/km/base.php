<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'registration_closed'    => 'Registration is closed.',
    'first_page_you_see'     => 'ទំព័រដំបូងដែលអ្នកឃើញបន្ទាប់ពីចូល',
    'login_status'           => 'ស្ថានភាពចូល',
    'logged_in'              => 'អ្នកបានចូល!',
    'toggle_navigation'      => 'Toggle navigation',
    'administration'         => 'ADMINISTRATION',
    'user'                   => 'អ្នក​ប្រើ',
    'logout'                 => 'ចាកចេញ',
    'login'                  => 'ចូល',
    'register'               => 'ចុះឈ្មោះ',
    'name'                   => 'ឈ្មោះ',
    'email_address'          => 'អាសយ​ដ្ឋាន​អ៊ី​ម៉េ​ល',
    'password'               => 'ពាក្យសម្ងាត់',
    'old_password'           => 'ពាក្យសម្ងាត់​ចាស់',
    'new_password'           => 'ពាក្យសម្ងាត់​ថ្មី',
    'confirm_password'       => 'បញ្ជាក់ពាក្យសម្ងាត់',
    'remember_me'            => 'ចងចាំខ្ញុំ',
    'forgot_your_password'   => 'ភ្លេចពាក្យសម្ងាត់របស់អ្នក?',
    'reset_password'         => 'កំណត់ពាក្យសម្ងាត់ឡើងវិញ',
    'send_reset_link'        => 'ផ្ញើពាក្យសម្ងាត់កំណត់តំណឡើងវិញ',
    'click_here_to_reset'    => 'ចុចទីនេះដើម្បីកំណត់ពាក្យសម្ងាត់របស់អ្នក',
    'change_password'        => 'ផ្លាស់ប្តូរពាក្យសម្ងាត់',
    'unauthorized'           => 'គ្មានការអនុញ្ញាត',
    'dashboard'              => 'ផ្ទាំងគ្រប់គ្រង',
    'handcrafted_by'         => 'បង្កើតដោយ',
    'powered_by'             => 'Powered by',
    'my_account'             => 'គណនី​របស់ខ្ញុំ',
    'update_account_info'    => 'ធ្វើបច្ចុប្បន្នភាពព័ត៌មានគណនី',
    'save'                   => 'រក្សាទុក',
    'cancel'                 => 'បោះបង់',
    'error'                  => 'កំហុស',
    'success'                => 'ជោគជ័យ',
    'old_password_incorrect' => 'ពាក្យសម្ងាត់ចាស់មិនត្រឹមត្រូវ',
    'password_dont_match'    => 'ពាក្យសម្ងាត់មិនត្រូវគ្នា។',
    'password_empty'         => 'ធ្វើឱ្យប្រាកដថាវាលពាក្យសម្ងាត់ទាំងពីរត្រូវបានបំពេញ',
    'password_updated'       => 'បានធ្វើបច្ចុប្បន្នភាពពាក្យសម្ងាត់.',
    'account_updated'        => 'គណនីត្រូវបានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ.',
    'unknown_error'          => 'កំហុសមិនស្គាល់មួយបានកើតឡើង! សូម​ព្យាយាម​ម្តង​ទៀត!',
    'error_saving'           => 'មានកំហុសខណៈពេលរក្សាទុក!​ សូម​ព្យាយាម​ម្តង​ទៀត!',
];
