<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Payment</title>
</head>

<body>



    <div class="print-payment">
        <div id="DivIdToPrintPop">
            <style>
                table {
                    border-collapse: collapse;
                }

                .border th,
                .border td {
                    border: 1px solid rgba(188, 188, 188, 0.96);
                    padding: 5px;
                }

                .right {
                    text-align: right;
                }

                .my-table tr td {
                    font-size: 12px;
                    padding: 10px;
                }

                .print-payment {
                    background-color: #fff;
                    height: 842px;
                }
            </style>
            <?php
            $m = getSetting();
            $logo = getSettingKey('logo', $m);
            $company = getSettingKey('company-name', $m);
            $client = App\Models\Client::find($row->client_id);
            ?>
            @if ($row != null)
                <table style="width: 100%" class="my-table">
                    <tr>
                        <td>
                            <img src="{{ asset('uploads/0.png') }}" width="130" />
                        </td>

                        <td style="font-size:20px; text-align: center; font-weight: bold;">
                            {{ $company }} <br>
                            Payment<br>

                        </td>
                        <td>
                            <img src="{{ asset($client->photo_of_client) }}" width="130" />
                        </td>
                    </tr>
                </table>
                <table style="width: 100%; margin-top: 30px;" class="my-table">
                    <tbody>
                        <tr>
                            @php
                                $l = App\Models\Loan::find($row->disbursement_id);
                                $branch = App\Models\Branch::find($l->branch_id);
                                //dd($row);
                            @endphp
                            <td><b>Payment Number</b></td>
                            <td>{{ $row->payment_number }}</td>
                            <td><b>Branch Name</b></td>
                            <td>{{ optional($branch)->title }}</td>
                        </tr>
                        <tr>
                            <td><b>Receipt No</b></td>
                            <td>{{ $row->receipt_no }}</td>
                            <td><b>Client ID</b></td>
                            <td>{{ optional(optional($row)->client_name)->client_number }}</td>
                            
                           
                        </tr>
                        <tr>
                            <td><b>Over Days</b></td>
                            <td>{{ $row->over_days }}</td>
                            <td><b>Client Name</b></td>
                            <td>{{ optional(optional($row)->client_name)->name }}</td>
                           
                        </tr>
                        <tr>
                            <td><b>Principle</b></td>
                            <td>{{ number_format($row->principle), 2 }}</td>
                            <td><b>Date</b></td>
                            <td>{{ \Carbon\Carbon::parse($row->payment_date)->format('Y-m-d') }}</td>
                           
                          
                           
                        </tr>
                        <tr>
                            <td><b>Interest</b></td>
                            <td>{{ number_format($row->interest), 2 }}</td>
                            <td><b>Penalty Amount</b></td>
                            <td>{{ number_format($row->penalty_amount), 2 }}</td>
                           
                          
                        </tr>
                        <tr>
                            <td><b>Effective Interest</b></td>
                            <td>{{ number_format($row->effective_interest), 2 }}</td>
                            <td><b></b></td>
                            <td></td>
                           
                          
                        </tr>
                        <tr>
                            <td><b>Total Payment</b></td>
                            <td>{{ number_format($row->total_payment + $row->effective_interest), 2 }}</td>
                            <td><b>Payment Method</b></td>
                            <td>{{ $row->payment_method }}</td>
                           
                            
                        </tr>
                        <tr>
                            @php
                                $principal_p = optional($loan)->principle_repayment;
                                $principal_out = optional($loan)->loan_amount - $principal_p;
                            @endphp
                            <td><b>Owed Balance</b></td>
                            <td>{{ number_format($row->owed_balance), 2 }}</td>
                            <td><b>Other Pay</b></td>
                            <td>{{ number_format($row->other_payment), 2 }}</td>
                           
                        </tr>
                        <tr>
                            @php
                                $total_interest = \App\Models\LoanCalculate::where('disbursement_id', $loan->id)->sum('interest_s');
                                $interest_p = optional($loan)->interest_repayment;
                                $interest_out = $total_interest - $interest_p;
                                if ($interest_out < 0) {
                                    $interest_out = 0;
                                } 
                            @endphp
                             <td><b>Principle Outstanding</b></td>
                             <td>{{ $principal_out }}</td>
                             <td><b>Payment</b></td>
                             <td>{{ number_format($row->payment + $row->effective_interest), 2 }}</td>
                        </tr>
                        <tr>
                            @php
                                $total = $principal_out + $interest_out;
                            @endphp
                            <td><b>Interest Outstanding</b></td>
                            <td>{{ $interest_out }}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>Total Outstanding</b></td>
                            <td>{{ number_format($total) }}</td>
                            <td></td>
                            <td></td>
                        </tr>

                    </tbody>
                </table>
        </div>
        <div class="action" style="float: right;">
            <button type="button " onclick="printDiv()"
                style="cursor: pointer; background: #0b58a2;padding: 10px 20px; color: #fff; margin-bottom: 10px;">PRINT</button>
        </div>
        @endif
    </div>


    <script src="{{ asset('vendor/adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>


    <script>
        // if (typeof printDiv !== 'undefined') {


        function printDiv() {
            var DivIdToPrintPop = document.getElementById('DivIdToPrintPop'); //DivIdToPrintPop

            if (DivIdToPrintPop != null) {
                var newWin = window.open('', 'Print-Window');

                newWin.document.open();

                newWin.document.write('<html><body onload="window.print()">' + DivIdToPrintPop.innerHTML +
                    '</body></html>');

                newWin.document.close();

                setTimeout(function() {
                    newWin.close();
                }, 10);
            }
        }


        // }
    </script>



</body>

</html>
