<div id="DivIdToPrint" style="overflow-x:auto;">
    @include('partials.reports.header',
    ['report_name'=>'Portfolio And Outreach','from_date'=>$start_date,'to_date'=>$end_date,'use_date'=>1])

    <table class="table-data" id="table-data">
        <tbody>

        </tbody>
    </table>

    <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="1284"
           style="width:963.0pt;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;">
        <tbody>
        <tr style="mso-yfti-irow:4;height:15.8pt">
            <td width="44" nowrap="" valign="bottom"
                style="width:32.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="50" nowrap="" valign="bottom"
                style="width:37.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="80" nowrap="" valign="bottom"
                style="width:59.9pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="4" valign="bottom"
                style="width:42.7pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="2" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" colspan="2" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt; height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="75" nowrap="" style="width:56.4pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="150" nowrap="" colspan="2"
                style="text-align: right;width:112.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt">
                <p class="MsoNormal"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:-439pt;mso-char-indent-count:5.0;line-height:normal"><span
                            style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Date</p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt">{{date("d/m/Y")}}</td>
            <td width="33" nowrap="" style="width:24.4pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt; height:15.8pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height: normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif; mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></p>
            </td>
        </tr>


        <tr style="mso-yfti-irow:5;height:15.8pt">
            <td width="44" nowrap="" valign="bottom"
                style="width:32.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="50" nowrap="" valign="bottom"
                style="width:37.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="80" nowrap="" valign="bottom"
                style="width:59.9pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="4" valign="bottom"
                style="width:42.7pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" colspan="2" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" colspan="2" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" valign="bottom"
                style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="56" nowrap="" style="width:42.3pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="75" nowrap="" style="width:56.4pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="150" nowrap="" colspan="2"
                style="text-align: right;width:112.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt">
                <p class="MsoNormal"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:-439pt;mso-char-indent-count:5.0;line-height:normal">
                    <span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Reporting Period:</span>
                </p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt"></td>
            <td width="33" nowrap=""
                style="width:24.4pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:15.8pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal">
                    <span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span>
                </p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:6;height:12.4pt">
            <td width="44" nowrap="" valign="bottom"
                style="width:32.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="50" nowrap="" valign="bottom"
                style="width:37.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="80" nowrap="" valign="bottom"
                style="width:59.9pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="114" nowrap="" colspan="5" valign="top"
                style="width:85.5pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (1%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="4" valign="top"
                style="width:85.1pt;border:none; border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (10%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (50%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (75%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (100%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (50%)</span>
                </p>
            </td>
            <td width="113" nowrap="" colspan="2" valign="top"
                style="width:85.1pt;border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><span
                            style="font-size:7.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">#LLR (100%)</span>
                </p>
            </td>
            <td width="75" nowrap="" valign="bottom"
                style="width:56.4pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="72" nowrap="" valign="bottom" style="width:.75in;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="78" nowrap="" valign="bottom"
                style="width:58.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="57" nowrap="" valign="bottom"
                style="width:42.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
            <td width="33" nowrap="" valign="bottom"
                style="width:24.4pt;padding:0in 5.4pt 0in 5.4pt;height:12.4pt"></td>
        </tr>


        <tr style="mso-yfti-irow:7;height:28.3pt">
            <td width="44" rowspan="2" valign="top"
                style="width:32.8pt;border:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;mso-border-alt:solid windowtext .5pt; mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">No.</span></b>
                </p>
            </td>
            <td width="50" rowspan="2" valign="top"
                style="width:37.8pt;border-top:solid windowtext 1.0pt;border-left:none;border-bottom:solid black 1.0pt;border-right:solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Region</span></b>
                </p>
            </td>
            <td width="80" rowspan="2" valign="top"
                style="width:59.9pt;border-top:solid windowtext 1.0pt;border-left:none;border-bottom:solid black 1.0pt;border-right:solid windowtext 1.0pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Loan Outstanding</span></b>
                </p>
            </td>
            <td width="114" colspan="5" valign="top"
                style="width:85.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Current</span></b>
                </p>
            </td>
            <td width="113" colspan="4" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Sub-Standard (1-30) Days *NPL</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Watch (31 - 60) Days *NPL</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Doubtful (61 - 90) Days *NPL</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt; mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt; padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Loss <span
                                    class="GramE">( Over</span> 91 ) Days *NPL</span></b></p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span
                                style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Rescheduled 1 Time</span></b>
                </p>
            </td>
            <td width="113" colspan="2" valign="top"
                style="width:85.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center"
                   style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Rescheduled 2 Time</span></b></p>
            </td>
            <td width="75" valign="top" style="width:56.4pt;border-top:solid windowtext 1.0pt;border-left:none;border-bottom:solid black 1.0pt;border-right:none;mso-border-left-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt; padding:0in 5.4pt 0in 5.4pt;height:28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Total *NPL</span></b></p>
            </td>
            <td width="72" valign="top" style="width:.75in;border-top:solid windowtext 1.0pt; border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;border-right:none;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
 font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Loan Loss Reserve</span></b></p>
            </td>
            <td width="78" valign="top" style="width:58.8pt;border-top:solid windowtext 1.0pt;
  border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;
  border-right:none;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Total *NPL / Loan Outstanding</span></b></p>
            </td>
            <td width="90" colspan="2" valign="top" style="width:67.2pt;border:solid windowtext 1.0pt;
  border-right:solid black 1.0pt;mso-border-alt:solid windowtext .5pt;
  mso-border-right-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt;height:
  28.3pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Write Off</span></b></p>
            </td>
        </tr>


        <tr style="mso-yfti-irow:8;height:39.8pt">
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="57" colspan="4" valign="top" style="width:42.7pt;border-top:none; border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" colspan="2" valign="top" style="width:42.8pt;border-top:none; border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" colspan="2" valign="top" style="width:42.3pt;border-top:none;  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt;  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';  mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="57" valign="top" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="56" valign="top" style="width:42.3pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
            <td width="75" style="width:56.4pt;border:none;border-bottom:solid black 1.0pt;  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;  mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:39.8pt"></td>
            <td width="72" style="width:.75in;border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;border-right:none;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:39.8pt"></td>
            <td width="78" style="width:58.8pt;border-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;border-right:none;mso-border-top-alt:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-bottom-alt:solid black .5pt;padding:0in 5.4pt 0in 5.4pt; height:39.8pt"></td>
            <td width="57" valign="top" style="width:42.8pt;border:solid windowtext 1.0pt; border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt: solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding: 0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal"><b><span style="font-size:9.0pt; font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Number of Clients</span></b></p>
            </td>
            <td width="33" valign="top" style="width:24.4pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:39.8pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal"><b><span style="font-size:9.0pt;  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman'; mso-bidi-language:KHM">Amount</span></b></p>
            </td>
        </tr>



        {{--no 1--}}
        @php
            $branches = App\Models\Branch::All();
            $total_client = $total_client??0;
            $total_amount = $total_amount??0;
        @endphp
        @foreach ($branches as $branch)
        @php
             $clients = App\Models\Client::where('branch_id',$branch->id)->count();
             $loan_release_total =  \App\Models\Loan::whereIn('disbursement_status',['Activated','Closed','Written-Off'])
                                                ->where('branch_id',$branch->id)
                                                ->where('status_note_date_activated','<=',$end_date)
                                                ->sum('loan_amount');
            $principal_collection_total = \App\Models\LoanPayment::join('loans','loans.id','=','loan_payments.disbursement_id')
                                                                ->where('loans.branch_id',$branch->id)
                                                                ->where('loan_payments.payment_date','<=',$end_date)
                                                                ->sum('loan_payments.principle');
            $total_outstanding = ($loan_release_total - $principal_collection_total);

            $one_month = [];
            $two_month = [];
            $three_month = [];
            $over_three_month = [];
            $loans = App\Models\Loan::where('branch_id',$branch->id)->get();
            $write_off_loans = App\Models\Loan::where('branch_id',$branch->id)->where('disbursement_status','Written-Off')->count();
            $writeoff_loans = App\Models\Loan::where('branch_id',$branch->id)->where('disbursement_status','Written-Off')->get();
            $principle = $principle??0;
            $interest = $interest??0;
            foreach($writeoff_loans as $writeoff_loan){
                $principle += $writeoff_loan->principle_receivable;
                $interest += $writeoff_loan->interest_receivable;
            }
            $write_off_amount = $principle + $interest;
            foreach($loans as $loan){
                $row = App\Models\LoanCalculate::select('date_s')
                ->where('disbursement_id', $loan->id)
                ->where('payment_status','pending')
                ->where('date_p', NULL)
                ->first();

                $over_due = Carbon\Carbon::parse(optional($row)->date_s)->diffInDays();
                if($over_due > 0 && $over_due <= 30){
                    array_push($one_month,$loan->id);
                }
                elseif($over_due > 30 && $over_due <= 60){
                    array_push($two_month,$loan->id);
                }
                elseif($over_due > 60 && $over_due <= 90){
                    array_push($three_month,$loan->id);
                }
                elseif($over_due > 90){
                    array_push($over_three_month,$loan->id);
                }
            }

            $loan_release_total_one =  \App\Models\Loan::whereIn('disbursement_status',['Activated','Closed','Written-Off'])
                                                    ->whereIn('id',$one_month)
                                                    ->sum('loan_amount');

            $principal_collection_total_one = \App\Models\LoanPayment::whereIn('disbursement_id',$one_month)->sum('principle');
           
            $total_outstanding_one = ($loan_release_total_one - $principal_collection_total_one);

            $loan_release_total_two =  \App\Models\Loan::whereIn('disbursement_status',['Activated','Closed','Written-Off'])
                                                        ->whereIn('id',$two_month)
                                                        ->sum('loan_amount');

            $principal_collection_total_two = \App\Models\LoanPayment::whereIn('disbursement_id',$two_month)->sum('principle');
        
            $total_outstanding_two = ($loan_release_total_two - $principal_collection_total_two);

            $loan_release_total_three =  \App\Models\Loan::whereIn('disbursement_status',['Activated','Closed','Written-Off'])
                                                        ->whereIn('id',$three_month)
                                                        ->sum('loan_amount');

            $principal_collection_total_three = \App\Models\LoanPayment::whereIn('disbursement_id',$three_month)->sum('principle');
    
            $total_outstanding_three = ($loan_release_total_three - $principal_collection_total_three);
            $loan_release_total_four =  \App\Models\Loan::whereIn('disbursement_status',['Activated','Closed','Written-Off'])
                                                        ->whereIn('id',$over_three_month)
                                                        ->sum('loan_amount');

            $principal_collection_total_four = \App\Models\LoanPayment::whereIn('disbursement_id',$over_three_month)->sum('principle');
            
            $total_outstanding_four = ($loan_release_total_three - $principal_collection_total_three);

            $total = count($two_month) + count($one_month) + count($three_month) + count($over_three_month);
        @endphp
        <tr style="mso-yfti-irow:9;height:22.45pt">
            <td width="44" nowrap="" style="width:32.8pt;border:solid windowtext 1.0pt; border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt: solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding: 0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp; </span>{{$branch->id}} </span></p>
            </td>
            <td width="50" nowrap="" style="width:37.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height: normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif; mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;{{$branch->title}}</span></p>
            </td>
            <td width="80" nowrap="" style="width:59.9pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;{{$total_outstanding}}</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;{{$clients}}</span></p>
            </td>
            <td width="57" nowrap="" colspan="4" style="width:42.7pt;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;{{$total_outstanding}}</span></p>
            </td>
            <td width="57" nowrap="" colspan="2" style="width:42.8pt;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;</span>{{count($one_month)}}</p>
            </td>
            <td width="56" nowrap="" colspan="2" style="width:42.3pt;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span>{{$loan_release_total_one}}</p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;{{count($two_month)}}</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;{{$loan_release_total_two}}</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;{{count($three_month)}}</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;{{$loan_release_total_three}}</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language: KHM">&nbsp;{{count($over_three_month)}}</span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt; text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family: 'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:  KHM">&nbsp;{{$loan_release_total_four}}</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM">&nbsp; - </span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM">&nbsp; - </span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM">&nbsp; - </span></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM">&nbsp; - </span></p>
            </td>
            @php
                if($total_outstanding == 0){
                    $total_outstanding = 1;
                }
            @endphp
            <td width="75" nowrap="" style="width:56.4pt;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM"><span style="mso-spacerun:yes">&nbsp; {{$total / $total_outstanding}}</span></span></p>
            </td>
            <td width="72" nowrap="" style="width:.75in;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM"><span style="mso-spacerun:yes">&nbsp; {{$total_outstanding * 0.01}}</span></span></p>
            </td>
            <td width="78" nowrap="" style="width:58.8pt;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM">&nbsp;{{$total}}</span></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM">&nbsp; {{$write_off_loans}}</span></p>
            </td>
            <td width="33" nowrap="" style="width:24.4pt;border-top:none;border-left:none;
                border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
                text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
                'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
                KHM">&nbsp;{{$write_off_amount}}</span></p>
            </td>
        </tr>
        @endforeach


        <tr style="mso-yfti-irow:24;height:22.45pt">
            <td width="44" nowrap="" style="width:32.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></p>
            </td>
            <td width="50" nowrap="" style="width:37.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal"><b><span style="font-size:9.0pt;
  font-family:'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';
  mso-bidi-language:KHM">Total</span></b></p>
            </td>
            <td width="80" nowrap="" style="width:59.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" colspan="4" style="width:42.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" colspan="2" style="width:42.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;</span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
  </span></span></b></p>
            </td>
            <td width="56" nowrap="" colspan="2" style="width:42.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span><span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
  </span></span></b></p>
            </td>
            <td width="56" nowrap="" style="width:42.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="75" nowrap="" style="width:56.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></p>
            </td>
            <td width="72" nowrap="" style="width:.75in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" align="right" style="margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal"><span style="font-size:9.0pt;font-family:
  'Times New Roman',serif;mso-fareast-font-family:'Times New Roman';mso-bidi-language:
  KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></p>
            </td>
            <td width="78" nowrap="" style="width:58.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM">&nbsp;</span></b></p>
            </td>
            <td width="57" nowrap="" style="width:42.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
            <td width="33" nowrap="" style="width:24.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.45pt">
                <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:9.0pt;font-family:'Times New Roman',serif;
  mso-fareast-font-family:'Times New Roman';mso-bidi-language:KHM"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>-<span
                                    style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span></span></b></p>
            </td>
        </tr>


        </tbody>
    </table>

</div>
