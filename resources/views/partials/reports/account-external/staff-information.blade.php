<div id="DivIdToPrint">
    @include('partials.reports.header',
    ['report_name'=>'Staff Information','use_date'=>1])

    <table class="table-data" id="table-data">
        <div class="panel panel-default">
            <div class="panel-body" style="padding: 0px !important;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 col-p-6">&nbsp;</div>
                            <div class="col-md-4 col-p-4">
                                <p class="text-right">
                                    <span style="font-family:&quot;Times New Roman&quot;,serif;">Date (dd-mmm-yyyy) :</span>
                                </p>
                            </div>
                            <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;">
                                dd-mm-yyyy
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6 col-p-6">&nbsp;</div>
                            <div class="col-md-4 col-p-4">
                                <p class="text-right">
                                    <span style="font-family:&quot;Times New Roman&quot;,serif;">Reporting Period:</span>
                                </p>
                            </div>
                            <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;">mm</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                No.
                            </th>
                            <th class="text-center" style="border: solid windowtext 1.0pt;">
                                Region
                            </th>
                            <th class="text-center" style="border: solid windowtext 1.0pt;">
                                No.of Field Staff (Credit Officers)
                            </th>
                            <th class="text-center" style="border: solid windowtext 1.0pt;">
                                Managerial Staff
                            </th>
                            <th class="text-center" style="border: solid windowtext 1.0pt;">
                                Other Staff
                            </th>
                            <th class="text-center" style="border: solid windowtext 1.0pt;">
                                Total Staff
                            </th>
                            <th class="text-center" style="border: solid windowtext 1.0pt;">
                                No. of Staff Recruited/Joined
                            </th>
                            <th class="text-center" style="border: solid windowtext 1.0pt;">
                                No.of Staff Terminated/Quit/Left
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                1
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                2
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                3
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                4
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                5
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                6
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                7
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                8
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                9
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="border: solid windowtext 1.0pt;width: 1%;">
                                10
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;"></td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td class="text-center" colspan="2" style="border: solid windowtext 1.0pt;width: 1%;">
                                Total
                            </td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;">0</td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;">0</td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;">0</td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;">0</td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;">0</td>
                            <td class="text-center" style="border: solid windowtext 1.0pt;">0</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 col-p-6">&nbsp;</div>
                            <div class="col-md-4 col-p-4">
                                <p class="text-right">
                                    <span style="font-family:&quot;Times New Roman&quot;,serif;">Prepared by (Name/Signature) :</span>
                                </p>
                            </div>
                            <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;">&nbsp;</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6 col-p-6">&nbsp;</div>
                            <div class="col-md-4 col-p-4">
                                <p class="text-right">
                                    <span style="font-family:&quot;Times New Roman&quot;,serif;">Checked by (Name/Signature):</span>
                                </p>
                            </div>
                            <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;"> &nbsp;</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6 col-p-6">&nbsp;</div>
                            <div class="col-md-4 col-p-4">
                                <p class="text-right">
                                    <span style="font-family:&quot;Times New Roman&quot;,serif;">Approved by (Name/Signature):</span>
                                </p>
                            </div>
                            <div class="col-md-2 col-p-2" style="border-bottom: 1px solid #333;"> &nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </table>


</div>
