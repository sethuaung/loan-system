@php
    $new_acc = App\Models\AccountChart::where('created_at', '>=', '2019-09-18 00:00:00')
        ->pluck('code')
        ->toArray();
@endphp
<div class="modal fade" id="show-detail-modal" tabindex="-1" role="dialog" aria-labelledby="show-detail-modal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="DivIdToPrintPop">

            </div>
            <div class="action" style="float: right;">
                <button type="button" onclick="printDiv() "
                    style="cursor: pointer; background: #0b58a2;padding: 10px 20px; color: #fff; margin-bottom: 10px;">PRINT</button>
            </div>
        </div>
    </div>
</div>
<style>
    a {
        text-decoration: underline;
        cursor: pointer;
    }
</style>
<input type="hidden" id="start_date" value="{{ $start_date }}">
<input type="hidden" id="end_date" value="{{ $end_date }}">
<div id="DivIdToPrint">
    @if ($bals != null)
        <?php
        
        ?>

        @include('partials.reports.header', [
            'report_name' => 'Balance Sheet',
            'from_date' => $start_date,
            'to_date' => $end_date,
            'use_date' => 1,
        ])

        <table border="1" class="table-data" id="table-data">
            @if (companyReportPart() != 'company.mkt')
                <thead>
                    <th></th>
                    <?php $sp = count($branches) + 1; ?>

                    @foreach ($branches as $branch)
                        <th>{{ optional(\App\Models\Branch::find($branch))->title }}</th>
                    @endforeach
                </thead>
                <tbody>
            @endif
            @if (companyReportPart() == 'company.mkt')
                <thead>
                    <th></th>
                    <?php $sp = count($branches) + 2; ?>

                    @foreach ($branches as $branch)
                        <th colspan="5">{{ optional(\App\Models\Branch::find($branch))->title }}</th>
                    @endforeach
                </thead>
                <tbody>


            @endif
            {{-- Ordinary Income --}}

            <tr>
                <td colspan="5" style="color: blue;"><b>Ordinary Income / Expense (MKT)</b></td>
                <td></td>
            </tr>

            @php
                $total_income = [];
                $sections = [10,12,14,16,18,20,22,24,26,30];
            @endphp
            @foreach ($sections as $section)
                @php
                    // echo($section);
                    $acc_section = App\Models\AccountSection::find($section);
                    $sectionAccounts = App\Models\AccountChart::where('section_id', $section)
                        ->whereNull('parent_id')
                        ->whereNull('sub_section_id')
                        ->get();
                    
                @endphp
                @foreach ($sectionAccounts as $sectionAccount)
                    @php
                        $countAcc = App\Models\AccountChart::where('parent_id', $sectionAccount->id)
                            ->where('section_id', $section)
                            ->count();
                    @endphp
                    @if ($countAcc > 0)
                        @php
                            // if ($loop->index != 0) {
                            //     dd($sectionAccounts[$loop->index],$loop->index);
                            //     echo "////";
                            // }
                        @endphp
                        <tr>
                            <td style="padding-left: 10px;"><b>{{ optional($sectionAccount)->code }}</b></td>
                            <td style="padding-left: 10px;" colspan="4"><b>{{ optional($sectionAccount)->name }}</b>
                            </td>

                            @foreach ($branches as $b_id)
                                <td></td>
                            @endforeach
                        </tr>

                        @php
                            $sub_section_total = 0;
                            $accounts = App\Models\AccountChart::where('section_id', $section)
                                ->where('parent_id', $sectionAccount->id)
                                ->get();
                            $acc_arrays = [];
                            foreach ($accounts as $account) {
                                if (!in_array($account->id, $acc_arrays) && $account->sub_section_id == null && $account->parent_id != null) {
                                    array_push($acc_arrays, $account->id);
                                }
                            }
                        @endphp

                        @foreach ($acc_arrays as $acc_array)
                            @php
                                $sub_section = App\Models\AccountChart::find($acc_array);
                                $acc_charts = App\Models\AccountChart::where('parent_id', $acc_array)
                                    ->where('section_id', $section)
                                    ->get();
                                
                            @endphp

                            @php
                                
                                $sum = 0;
                                $parents = [];
                                foreach ($acc_charts as $acc_chart) {
                                    if (!in_array($acc_chart->parent_id, $parents) && $acc_chart->parent_id != null) {
                                        array_push($parents, $acc_chart->parent_id);
                                    }
                                }
                            @endphp
                            <tr>
                                <td style="padding-left: 30px;"><b>{{ optional($sub_section)->code }}</b></td>
                                <td style="padding-left: 30px;" colspan="3"><b>{{ optional($sub_section)->name }}</b>
                                </td>
                                @foreach ($branches as $b)
                                    @if (isset($bals[$section][$sub_section->id][$b]))
                                        @php
                                            $sum += $bals[$section][$sub_section->id][$b];
                                            $sub_section_total += $bals[$section][$sub_section->id][$b];
                                        @endphp
                                        <td style="text-align:center;">
                                            {{ $bals[$section][$sub_section->id][$b] }}
                                        </td>
                                    @else
                                        <td style="text-align:center;">0</td>
                                    @endif
                                @endforeach
                                <td style="text-align:center;">
                                    {{ $sum ?? 0 }}
                                </td>
                            </tr>

                            @foreach ($parents as $parent)
                                @php
                                    $acc_charts = App\Models\AccountChart::where('parent_id', $parent)
                                        ->where('section_id', $section)
                                        ->whereNotIn('id', $acc_arrays)
                                        ->get();
                                    $parentAcc = App\Models\AccountChart::find($parent);
                                    $sub_total = 0;
                                    // dd($acc_charts);
                                @endphp
                                @foreach ($acc_charts as $acc_chart)
                                    <tr>
                                        <td></td>
                                        {{-- <td></td> --}}
                                        <td style="padding-left: 30px;" colspan="2"><b>{{ $acc_chart->code }}</b>
                                        </td>
                                        <td style="padding-left: 30px;"><b>{{ $acc_chart->name }}</b></td>

                                        @foreach ($branches as $b)
                                            @php
                                                $total = 0;
                                            @endphp
                                            @if (isset($bals[$section][$acc_chart->id][$b]))
                                                @php
                                                    $total += $bals[$section][$acc_chart->id][$b];
                                                    $sub_total += $total;
                                                    $sub_section_total += $bals[$section][$acc_chart->id][$b];
                                                    $sum += $bals[$section][$acc_chart->id][$b];
                                                @endphp
                                                <td style="text-align:center;">
                                                    {{ $bals[$section][$acc_chart->id][$b] }}
                                                </td>
                                            @else
                                                <td style="text-align:center;">0</td>
                                            @endif
                                        @endforeach
                                        <td style="text-align:center;">{{ $total }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    {{-- <td style="padding-left: 30px;"></td> --}}
                                    <td style="padding-left: 30px; color:green"></td>
                                    <td style="padding-left: 30px; color:green" colspan="2"><b>Total
                                            {{ optional($parentAcc)->name }}</b></td>

                                    @foreach ($branches as $b)
                                        <td style="text-align:center;color:green">{{ $sum }}</td>
                                    @endforeach
                                    <td style="text-align:center; color:green">{{ $sum }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        <tr>

                            <td style="padding-left: 30px; color:green"></td>
                            <td style="padding-left: 30px; color:green" colspan="3"><b>Total
                                    {{ optional($sectionAccount)->name }}</b></td>

                            @foreach ($branches as $b)
                                <td style="text-align:center;color:green">{{ $sub_section_total }}</td>
                            @endforeach
                            <td style="text-align:center; color:green">{{ $sub_section_total }}</td>
                        </tr>
                    @endif
                @endforeach
            @endforeach
            </tbody>
        </table>
        <script>
            $(".general-leg40, .general-leg50, .general-leg60, .general-leg70, .general-leg80").click(function() {
                getReport($(this).attr("id"));
            });

            function getReport(id) {
                var acc_chart_id = [id];
                var start_date = $('[name="start_date"]').val();
                var end_date = $('[name="end_date"]').val();
                var month = $('[name="month"]').val();
                var show_zero = $('[name="show_zero"]').val();
                var branch_id = $('[name="branch_id[]"]').val();
                var branch_id = 1;
                $.ajax({
                    url: "{{ url('report/general-leger') }}",
                    type: 'GET',
                    async: false,
                    dataType: 'html',
                    data: {
                        start_date: start_date,
                        end_date: end_date,
                        month: month,
                        acc_chart_id: acc_chart_id,
                        show_zero: show_zero,
                        branch_id: branch_id
                    },
                    success: function(d) {
                        $('.modal-body').html(d);
                    },
                    error: function(d) {
                        alert('error');
                        $('.modal-body').hide();
                    }
                });
            }

            function printDiv() {

                var DivIdToPrintPop = document.getElementById('DivIdToPrintPop'); //DivIdToPrintPop
                if (DivIdToPrintPop != null) {
                    var newWin = window.open('', 'Print-Window');

                    newWin.document.open();

                    newWin.document.write('<html><body onload="window.print()">' + DivIdToPrintPop.innerHTML +
                        '</body></html>');

                    newWin.document.close();

                    setTimeout(function() {
                        newWin.close();
                    }, 10);
                }
            }
        </script>
    @else
        <h1>No data</h1>
    @endif
</div>
