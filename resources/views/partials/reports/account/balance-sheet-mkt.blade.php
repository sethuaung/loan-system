<div class="modal fade" id="show-detail-modal" tabindex="-1" role="dialog" aria-labelledby="show-detail-modal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="DivIdToPrintPop">

            </div>
            <div class="action" style="float: right;">
                <button type="button" onclick="excelGeneral('DivIdToPrintPop') "
                    style="cursor: pointer; background: #0b58a2;padding: 10px 20px; color: #fff; margin-bottom: 10px;">EXCEL</button>
            </div>
            <div class="action" style="float: right;">
                <button type="button" onclick="printDiv() "
                    style="cursor: pointer; background: #0b58a2;padding: 10px 20px; color: #fff; margin-bottom: 10px;">PRINT</button>
            </div>
        </div>
    </div>

    <script>
        function excelGeneral(tableID, filename = '') {
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            // console.log(tableID);
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

            // Specify file name
            filename = filename ? filename + '.xls' : 'excel_data.xls';

            // Create download link element
            downloadLink = document.createElement("a");

            document.body.appendChild(downloadLink);

            if (navigator.msSaveOrOpenBlob) {
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob(blob, filename);
            } else {
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                // Setting the file name
                downloadLink.download = filename;

                //triggering the function
                downloadLink.click();
            }
        }
    </script>

</div>
<input type="hidden" id="start_date" value="{{ $start_date }}">
<input type="hidden" id="end_date" value="{{ $end_date }}">
<input type="hidden" id="company" value="{{ companyReportPart() }}">
<style>
    .modal-lg {
        width: 75%;
    }
</style>
<div id="DivIdToPrint">
    @include('partials.reports.header', [
        'report_name' => 'Balance Sheet',
        'from_date' => $start_date,
        'to_date' => $end_date,
        'use_date' => 1,
    ])
</div>
@php
    $today = date('Y-m-d');
@endphp
<div>{{ $today }}</div>
@foreach ($branchs as $branch)
    @php
        $a = \App\Models\Branch::find($branch);
    @endphp
    <div><span class="pull-right" style="margin-bottom:4px;">{{ optional($a)->title }},</span></div>
@endforeach
<table border="1" class="table-data" id="table-data">
    <tbody>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="0" style="color: blue;"><b></b></td>
            @foreach ($branchs as $b)
                @php
                    $c = \App\Models\BranchU::find($b);
                @endphp
                <td colspan="0" style="color: blue;"><b>{{ optional($c)->title }}</b></td>
            @endforeach
        </tr>
        <tr>
            <td>100001</td>
            <td>Fixed Assets</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>

        </tr>
        <tr>
            @php
                $total_intangible = 0;
            @endphp
            <td>100101</td>
            <td>Intangible Assets</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        <tr>
            <td></td>
            <td>100102</td>
            <td><b>Formation Expenses</b></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        {{-- Formation Expenses --}}
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [100120, 100130])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            @php
                
            @endphp
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_intangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach

        {{-- Software Cap --}}
        <tr>
            <td></td>
            <td>100200</td>
            <td><strong>Software Cap</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [100210, 100220])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_intangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach

        {{-- Other Intangible Assets --}}
        <tr>
            <td></td>
            <td>100300</td>
            <td><strong>Other Intangible Assets</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [100700, 100710])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_intangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color:blue"><strong>Total Intangible Assets</strong></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color:blue">{{ $total_intangible }}</td>
        </tr>
        <br>
        {{-- ------------------------------------------------------------------------------------------------------------------ --}}
        <tr>
            @php
                $total_tangible = 0;
            @endphp
            <td>105000</td>
            <td>Tangible Assets</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        <tr>
            <td></td>
            <td>105001</td>
            <td><b>Land</b></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        {{-- Buildings --}}
        <tr>
            <td></td>
            <td>105100</td>
            <td><b>Buildings</b></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [105110, 105120])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            @php
                
            @endphp
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_tangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach

        {{-- Furniture Fixtures & Fittings --}}
        <tr>
            <td></td>
            <td>107000</td>
            <td><strong>Furniture Fixtures & Fittings</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [107010, 107020])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_tangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach

        {{-- Electronic and Electrical equipment --}}
        <tr>
            <td></td>
            <td>107300</td>
            <td><strong>Electronic and Electrical equipment</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [107310, 107390])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_tangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        {{-- Electronic and Electrical equipment --}}
        <tr>
            <td></td>
            <td>107500</td>
            <td><strong>Computer and IT equipment</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [107510, 107520])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_tangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        {{-- Electronic and Electrical equipment --}}
        <tr>
            <td></td>
            <td>108000</td>
            <td><strong>Motor Vehicles</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [108010, 108040])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_tangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach

        {{-- Tools and Equipment --}}
        <tr>
            <td></td>
            <td>108500</td>
            <td><strong>Tools and Equipments</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $loop_accounts = \App\Models\AccountChart::whereIn('code', [108510, 108520])->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_tangible += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color:blue"><strong>Total Tangible Assets</strong></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color:blue">{{ $total_tangible }}</td>
        </tr>
        @php
            $total_fixed_assets = $total_tangible + $total_intangible;
        @endphp
        <tr>
            <td></td>
            <td style="color:green"><strong>Total Fixed Assets</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color:green">{{ $total_fixed_assets }}</td>
        </tr>

        {{-- Start Current Assets --}}
        <tr>
            @php
                $total_current_asset = 0;
            @endphp
            <td>150000</td>
            <td>Current Assets</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>

        </tr>
        <tr>
            <td></td>
            <td>150001</td>
            <td><b>Cash And Bank Equivalents</b></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        {{-- Bank Account --}}
        <tr>
            @php
                $total_bank = 0;
            @endphp
            <td></td>
            <td>150100</td>
            <td><b>Bank Account</b></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $bank_accounts = [150140, 150202, 150101, 150102, 150103, 150153, 150110, 150111, 150137, 150115, 150116, 150127, 150108, 150109, 150120, 150121, 150122, 150106, 150107, 150125, 150126, 150131, 150132, 150133, 150135, 150136, 150138, 150139, 15014, 150141, 150145, 150146, 150147, 150148, 150150, 150151, 150142, 150143, 150105, 150104, 150152, 150154, 150361, 150362, 150112, 150113, 150510, 150511, 150512, 150513, 150160, 150161, 150190, 150519, 150514, 150515, 150501, 150502, 150503, 150520, 150521, 150506, 150507, 150179, 150180, 150508, 150509, 150350, 150351, 151502, 150201, 150205, 150206, 150203, 150204, 150252, 150251, 150250, 150260, 150253, 150254, 150255];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $bank_accounts)->get();
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_bank += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td>Total Bank</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td>{{ $total_bank }}</td>
        </tr>

        {{-- Cash in hand --}}
        <tr>
            @php
                $total_cash_in_hand = 0;
            @endphp
            <td></td>
            <td>153500</td>
            <td><strong>Cash In Hand</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $cash_accounts = [153501, 153510, 153515, 153520, 153525, 153530, 153535, 153540, 153545, 153550, 153555, 153560, 153565, 153570, 153575, 153580, 153585, 153590, 153595, 153600, 153605, 153610, 153620, 153626, 153627, 153625];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $cash_accounts)->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_cash_in_hand += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Cash</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $total_cash_in_hand }}</td>
        </tr>
        <tr>
            <td></td>
            <td><strong>Total Cash And Bank Equivalents</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td>{{ $total_cash_in_hand ?? (0 + $total_bank ?? 0) }}</td>
        </tr>
        {{-- Cash In Transit --}}
        <tr>
            @php
                $total_cash_in_transit = 0;
            @endphp
            <td>2-96-500</td>
            <td>157000</td>
            <td><strong>CASH IN TRANSIT</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $transit_accounts = [157001, 157002, 157003, 157004, 157005, 157006, 157007, 157008, 157009, 157010, 157011, 157012, 157013, 157014, 157015, 157016, 157017, 157018, 157019, 157020, 157021, 157023, 157024, 157025, 157026, 157099];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $transit_accounts)->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_cash_in_transit += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Cash In Transit</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $total_cash_in_transit }}</td>
        </tr>

        {{-- Inventorys --}}
        <tr>
            @php
                $total_inventory = 0;
            @endphp
            <td>2-22-500</td>
            <td>165000</td>
            <td><strong>INVENTORYS</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $accounts = [179900, 179910, 179940];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_inventory += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Inventory</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $total_inventory }}</td>
        </tr>

        {{-- ACCOUNT  RECEIVABLES --}}
        <tr>
            @php
                $total_account_receivable = 0;
            @endphp
            <td></td>
            <td>183000</td>
            <td><strong>ACCOUNT RECEIVABLES</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>

        @php
            $accounts = [185000, 185810, 185820, 185830, 185840, 185850, 185860, 185910, 185920, 185930, 185950, 185960, 185965];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $total_account_receivable += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Account Receivable</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $total_account_receivable }}</td>
        </tr>

        {{-- Interest Receivable --}}
        @php
            $interest_receivable = 0;
            $accounts = [186110, 186120, 186130, 186140, 186200, 186210, 186150, 186250, 186260, 186160, 186230, 186220, 186170];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderbydesc('code')
                ->get();
            $title_account = \App\Models\AccountChart::whereIn('code', [186400])->first();
            //dd($loop_accounts);
        @endphp
        <tr>
            <td></td>
            <td>186400</td>
            <td style="color: black"><strong>Interst Receivable</strong></td>
            <td></td>
            <td></td>
            @foreach ($branchs as $b)
                @php
                    if (isset($bals_dr[$loop_account->section_id][$title_account->id][$b])) {
                        // dd("shi tal");
                        $amount = $bals_dr[$loop_account->section_id][$title_account->id][$b];
                    } else {
                        $amount = 0;
                    }
                    $interest_receivable += $amount;
                @endphp
                <td>{{ $amount }}</td>
            @endforeach
        </tr>

        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $interest_receivable += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Interest Receivable</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $interest_receivable }}</td>
        </tr>

        {{-- Provisions For Doubtful Debts --}}
        <tr>
            <td>1-71</td>
            <td>189000</td>
            <td><strong>Provisions For Doubtful Debts</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $doubtful_debts = 0;
            $accounts = [189010];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderbydesc('code')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $doubtful_debts += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Provisions For Doubtful Debts</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $doubtful_debts }}</td>
        </tr>

        {{-- Other Receivables --}}
        <tr>
            <td>1-71</td>
            <td>189000</td>
            <td><strong>Other Receivables</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $other_receivable = 0;
            $accounts = [190030, 190090, 190100];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderbydesc('code')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $other_receivable += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Other Receivables</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $other_receivable }}</td>
        </tr>
        {{-- Tax Receivable --}}
        <tr>
            <td>2-89-800</td>
            <td>190500</td>
            <td><strong>Tax Receivable</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $tax_receivable = 0;
            $accounts = [190510];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderbydesc('code')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $tax_receivable += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Tax Receivable</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $tax_receivable }}</td>
        </tr>

        {{-- Tax Receivable --}}
        <tr>
            <td></td>
            <td>192200</td>
            <td><strong>Other Receivable Group</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $other_receivable_gp = 0;
            $accounts = [190250, 190240, 190245, 192213];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderbydesc('code')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $other_receivable_gp += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Other Receivable Group</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $other_receivable_gp }}</td>
        </tr>

        {{-- Tax Receivable --}}
        <tr>
            <td></td>
            <td>193000</td>
            <td><strong>Prepayments & Accrued Income</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $p_and_a = 0;
            $accounts = [193020, 193240, 193050, 193060, 193120, 193250];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderbydesc('code')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $p_and_a += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Prepayments & Accrued Income</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $p_and_a }}</td>
        </tr>
        @php
            $total_current_assets = $total_bank + $total_cash_in_hand + $total_cash_in_transit + $total_inventory + $total_account_receivable + $interest_receivable + $doubtful_debts + $other_receivable + $tax_receivable + $other_receivable_gp + $p_and_a;
            $total_assets = $total_fixed_assets + $total_current_assets;
        @endphp
        <tr>
            <td></td>
            <td style="color: green">Total Current Assets</td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: green">{{ $total_current_assets }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="color: green">Total Assets</td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: green">{{ $total_assets }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        {{-- CurrentLiA --}}
        <tr>
            <td>CAPITAL AND LIABILITIES</td>
            <td></td>
            <td><strong></strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        <tr>
            <td></td>
            <td>250000</td>
            <td><strong>Long Term Liabilities</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $long_term_lib = 0;
            $accounts = [250030, 250040, 250050, 250080];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $long_term_lib += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Long Term Liabilities</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $long_term_lib }}</td>
        </tr>
        {{-- CurrentLiA --}}

        <tr>
            <td></td>
            <td>200001</td>
            <td><strong>CURRENT LIABILITIES</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        <tr>
            <td></td>
            <td>203000</td>
            <td><strong>Service Creditors(Saving)</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $current_lib_1 = 0;
            $accounts = [203852, 203851];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $current_lib_1 += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $current_lib_1 }}</td>
        </tr>

        @php
            $current_lib_2 = 0;
            $accounts = [203853, 203854];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $current_lib_2 += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $current_lib_2 }}</td>
        </tr>

        @php
            $current_lib_3 = 0;
            $accounts = [213520, 213521];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $current_lib_3 += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $current_lib_3 }}</td>
        </tr>
        @php
            $total_service_cre = $current_lib_3 + $current_lib_2 + $current_lib_1;
        @endphp
        <tr>
            <td></td>
            <td style="color: blue">Total Service Creditors(Saving)</td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $total_service_cre }}</td>
        </tr>

        {{-- Other Current Lia --}}
        <tr>
            <td></td>
            <td>210000</td>
            <td><strong>Other CURRENT LIABILITIES</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $other_current_lib = 0;
            $accounts = [210001, 250020, 210010, 210030, 210040];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $other_current_lib += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Other CURRENT LIABILITIES</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $other_current_lib }}</td>
        </tr>

        {{-- Tax Payables --}}
        <tr>
            <td>3-63-100</td>
            <td>213000</td>
            <td><strong>Tax Payables</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $tax_payable = 0;
            $accounts = [213020];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $tax_payable += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Tax Payables</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $tax_payable }}</td>
        </tr>

        {{-- Other Personal Payables --}}
        <tr>
            <td>3-61-100</td>
            <td>213500</td>
            <td><strong>Other Personnel Payable</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $other_personal_p = 0;
            $accounts = [213510, 213570, 213580, 213611];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $other_personal_p += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Other Personnel Payable</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $other_personal_p }}</td>
        </tr>

        {{-- Accurals --}}
        <tr>
            <td>3-62-100</td>
            <td>215000</td>
            <td><strong>Accruals </strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $accural = 0;
            $accounts = [215080, 215120];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $accural += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Accruals</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $accural }}</td>
        </tr>

        {{--  Deferred Income --}}
        <tr>
            <td></td>
            <td>216000</td>
            <td><strong> Deferred Income </strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $deferred_income = 0;
            $accounts = [215180, 215181, 215182, 215183, 215184, 215185, 215186, 215187, 215188, 215189, 215190, 215191, 215192, 215193, 215194];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $deferred_income += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Deferred Income</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $deferred_income }}</td>
        </tr>

        {{--  Other Payables --}}
        <tr>
            <td></td>
            <td>217000</td>
            <td><strong>Other Payables </strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $other_payable = 0;
            $accounts = [217130, 274030, 274040];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $other_payable += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Other Payables</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $other_payable }}</td>
        </tr>
        @php
            $total_current_lib = $total_service_cre + $other_current_lib + $tax_payable + $other_personal_p + $tax_payable + $accural + $deferred_income + $other_payable;
        @endphp
        <tr>
            <td></td>
            <td></td>
            <td style="color: green">Total CURRENT LIABILITIES</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: green">{{ $total_current_lib }}</td>
        </tr>
        <tr>
            <td></td>
            <td><strong>Total LIABILITIES</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td>{{ $long_term_lib + $total_current_lib }}</td>
        </tr>
        {{--   SHAREHOLDERS' EQUITY --}}
        <tr>
            <td></td>
            <td></td>
            <td><strong>Equity</strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        <tr>
            <td></td>
            <td>300000</td>
            <td><strong>SHAREHOLDERS' EQUITY </strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $s_e = 0;
            $accounts = [310000, 310010];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $s_e += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total SHAREHOLDERS' EQUITY</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $s_e }}</td>
        </tr>
        {{-- Reserves --}}
        <tr>
            <td></td>
            <td>321000</td>
            <td><strong> Reserves </strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $reserves = 0;
            $accounts = [321200, 321500, 323000];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $reserves += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Reserves</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $reserves }}</td>
        </tr>
        {{-- Retained Earning --}}
        <tr>
            <td></td>
            <td>321000</td>
            <td><strong> Retained Earning </strong></td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
        </tr>
        @php
            $re_earning = 0;
            $accounts = [332010, 332020, 340100];
            $loop_accounts = \App\Models\AccountChart::whereIn('code', $accounts)
                ->orderby('code', 'ASC')
                ->get();
            //dd($loop_accounts);
        @endphp
        @foreach ($loop_accounts as $loop_account)
            <tr>
                <td></td>
                <td></td>
                <td>{{ $loop_account->code }}</td>
                <td>{{ $loop_account->name }}</td>
                <td></td>
                @foreach ($branchs as $b)
                    @php
                        if (isset($bals_dr[$loop_account->section_id][$loop_account->id][$b])) {
                            // dd("shi tal");
                            $amount = $bals_dr[$loop_account->section_id][$loop_account->id][$b];
                        } else {
                            $amount = 0;
                        }
                        $re_earning += $amount;
                    @endphp
                    <td>{{ $amount }}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="color: blue">Total Retained Earning</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: blue">{{ $re_earning }}</td>
        </tr>
        @php
            $total_equity = $s_e + $reserves + $re_earning;
            $total_capital_lib = $total_equity + $total_current_lib;
        @endphp
        <tr>
            <td></td>
            <td></td>
            <td style="color: green">Total Equity</td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color: green">{{ $total_equity }}</td>
        </tr>

        <tr>
            <td></td>
            <td style="color:green">TOTAL CAPITAL AND LIABILITIES</td>
            <td></td>
            <td></td>
            <td colspan="{{ count($branchs) }}"></td>
            <td style="color:green">{{ $total_capital_lib }}</td>
        </tr>
    </tbody>
</table>
