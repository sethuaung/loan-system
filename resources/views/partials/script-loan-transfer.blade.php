@push('crud_fields_scripts')
<script>
    $('.change-branch').on('change',function(){
        //console.log("branch");
        var branch_id = $('#aioConceptName').find(":selected").val();
        $.ajax({
            type: 'GET',
            url: '{{url('api/change-branch')}}',
            data: {
                branch_id: branch_id,
            },
            success: function (res) {
               //console.log(res);
            }

        });
    });
</script>
@endpush