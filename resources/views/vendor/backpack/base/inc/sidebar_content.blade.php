@if (companyReportPart() == 'company.bolika' ||
    companyReportPart() == 'company.active_people' ||
    companyReportPart() == 'company.moeyan' )
    @include('vendor.backpack.base.inc.' . companyReportPart())
@else
    <!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
    <li><a href="{{ backpack_url('dashboard_mkt') }}"><i
                class="fa fa-dashboard"></i><span>{{ trans('backpack::base.dashboard') }}</span></a></li>

    @if (backpack_user()->can('add-client') ||
        backpack_user()->can('client-list') ||
        backpack_user()->can('center-leader') ||
        backpack_user()->can('add-guarantor') ||
        backpack_user()->can('guarantor-list') ||
        backpack_user()->can('add-address') ||
        backpack_user()->can('nrc-prefix') ||
        backpack_user()->can('survey') ||
        backpack_user()->can('ownership') ||
        backpack_user()->can('ownership-farmland') ||
        backpack_user()->can('customer-type') ||
        backpack_user()->can('import-client'))
        <li class="treeview">
            <a href="#"><i class="fa fa-users"></i> <span>MANAGE CLIENT</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">


                @if (backpack_user()->can('add-client'))
                    <li><a href='{{ backpack_url('client/create') }}'><i class="fa fa-plus-square-o"></i> <span>Add
                                Client</span></a></li>
                @endif
                @if (backpack_user()->can('client-list'))
                <li class="treeview">
                    <a href="#"><i class="fa fa-user"></i> <span>Client List</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
    
                    <ul class="treeview-menu">
                       
                            <li><a href="{{ backpack_url('client') }}"><i class="fa fa-user"></i> All Client List</a></li>
                 
                        @php
                            $customer_types = App\Models\CustomerGroup::select('id', 'name')->get();
                        @endphp
                        @foreach ($customer_types as $customer_type)
                          
                                @if (isset($_REQUEST['type_id']))
                                    @if ($_REQUEST['type_id'] == $customer_type->id)
                                    <li class="active"><a href="{{ backpack_url('client') }}?type_id={{ $customer_type->id }}&search_name={{ $customer_type->name }}"><i
                                        class="fa fa-user"></i> {{ $customer_type->name }}</a></li>
                                    @else
                                    <li><a href="{{ backpack_url('client') }}?type_id={{ $customer_type->id }}&search_name={{ $customer_type->name }}"><i
                                        class="fa fa-user"></i> {{ $customer_type->name }}</a></li>
                                    @endif
                                @else
                                    <li><a href="{{ backpack_url('client') }}?type_id={{ $customer_type->id }}&search_name={{ $customer_type->name }}"><i
                                                class="fa fa-user"></i> {{ $customer_type->name }}</a></li>
                                @endif
                          
                        @endforeach
                        {{-- @if (_can('list-client'))<li><a href="{{ backpack_url('active-member-client') }}"><i class="fa fa-user"></i> Active Member</a></li>@endif
                        @if (_can('list-client'))<li><a href="{{ backpack_url('drop_out-member-client') }}"><i class="fa fa-user"></i> Drop-out Member</a></li>@endif
                        @if (_can('list-client'))<li><a href="{{ backpack_url('rejoin-member-client') }}"><i class="fa fa-user"></i> Rejoin Member</a></li>@endif
                        @if (_can('list-client'))<li><a href="{{ backpack_url('substitute-member-client') }}"><i class="fa fa-user"></i> Substitute Member</a></li>@endif
                        
                        @if (_can('list-client'))<li><a href="{{ backpack_url('dead-member-client') }}"><i class="fa fa-user"></i> Dead Member</a></li>@endif --}}
    
                    </ul>
                </li>
                @endif
                @if (backpack_user()->can('center-leader'))
                    <li><a href='{{ backpack_url('clientcenterleader') }}'><i class="fa fa-plus-square-o"></i>
                            <span>Center Leader</span></a>
                @endif

                @if (backpack_user()->can('add-guarantor'))
                    <li><a href='{{ backpack_url('guarantor/create') }}'><i class="fa fa-plus-square-o"></i><span>Add
                                Guarantor</span></a></li>
                @endif
                @if (backpack_user()->can('guarantor-list'))
                    <li><a href='{{ backpack_url('guarantor') }}'><i class="fa fa-plus-square-o"></i><span>Guarantor
                                List</span></a></li>
                @endif
                @if (backpack_user()->can('add-address'))
                    <li><a href='{{ backpack_url('/add-address') }}'><i class="fa fa-plus-square-o"></i> <span>Add
                                Address</span></a></li>
                @endif
                <li class="sep-p">
                    <hr>
                </li>

                @if (backpack_user()->can('nrc-prefix'))
                    <li><a href="{{ backpack_url('nrc-prefix') }}"><i class='fa fa-tag'></i> <span>NRC
                                Prefix</span></a></li>
                @endif
                @if (backpack_user()->can('survey'))
                    <li><a href="{{ backpack_url('survey') }}"><i class='fa fa-tag'></i> <span>Survey</span></a></li>
                @endif
                @if (backpack_user()->can('ownership'))
                    <li><a href="{{ backpack_url('ownership') }}"><i class='fa fa-tag'></i> <span>Ownership</span></a>
                    </li>
                @endif
                @if (backpack_user()->can('ownership-farmland'))
                    <li><a href="{{ backpack_url('ownershipfarmland') }}"><i class='fa fa-tag'></i><span>Ownership
                                Farmland</span></a></li>
                @endif
                @if (backpack_user()->can('customer-type'))
                    <li><a href="{{ backpack_url('customergroup') }}"><i class='fa fa-tag'></i> <span>CUSTOMER
                                TYPE</span></a></li>
                @endif
                @if (backpack_user()->can('import-client'))
                    <li><a href="{{ backpack_url('import-client/create') }}"><i class='fa fa-download'></i>
                            <span>Import Client</span></a></li>
                @endif
            </ul>
        </li>
    @endif
    @if (backpack_user()->can('loan-calculator') ||
        backpack_user()->can('create-loan') ||
        backpack_user()->can('add-group-loan') ||
        backpack_user()->can('group-loan') ||
        backpack_user()->can('view-all-loan') ||
        backpack_user()->can('pending-approval') ||
        backpack_user()->can('loan-approved') ||
        backpack_user()->can('loan-activated') ||
        backpack_user()->can('loan-completed') ||
        backpack_user()->can('loan-declined') ||
        backpack_user()->can('loan-canceled') ||
        backpack_user()->can('loan-write-off') ||
        backpack_user()->can('prepaid-loan') ||
        backpack_user()->can('loan-objective') ||
        backpack_user()->can('transaction-type') ||
        backpack_user()->can('import-loans'))
        <li class="treeview">
            <a href="#"><i class="fa fa-cog"></i> <span>MANAGE LOAN</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">

                @if (backpack_user()->can('loan-calculator'))
                    <li><a href='{{ backpack_url('loan-calculator/create') }}'><i class="fa fa-plus-square-o"></i>
                            <span>Loan Calculator</span></a></li>
                @endif
                @if (backpack_user()->can('create-loan'))
                    <li><a href="{{ backpack_url('loandisbursement/create') }}"><i class='fa fa-tag'></i><span>Create
                                Loan</span></a></li>
                @endif
                @if (backpack_user()->can('add-group-loan'))
                    <li><a href="{{ backpack_url('grouploan') }}"><i class="fa fa-plus-square-o"></i> <span>Add Group
                                Loan</span></a></li>
                @endif

                @if (backpack_user()->can('group-loan'))
                    <li class="treeview"><a href="#"><i class="fa fa-users"></i> <span>Group Loan</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            @if (backpack_user()->can('group-pending-approve'))
                            <li><a href='{{ backpack_url('group-loan-approve') }}'><i class="fa fa-plus-square-o"></i>
                                    <span>Group Pending Approve</span></a></li>
                            @endif
                            @if (backpack_user()->can('group-deposit'))
                            <li><a href='{{ backpack_url('group-loan-deposit') }}'><i class="fa fa-plus-square-o"></i>
                                    <span>Group Deposit</span></a></li>
                            @endif
                            @if (backpack_user()->can('group-disburse'))
                            <li><a href='{{ backpack_url('group-dirseburse') }}'><i class="fa fa-plus-square-o"></i>
                                    <span>Group Diburse</span></a></li>
                            @endif
                            @if (backpack_user()->can('group-repayment'))
                            <li><a href='{{ backpack_url('group-repayment-new') }}'><i class="fa fa-plus-square-o"></i>
                                    <span>Group Repayment</span></a></li>
                            @endif
                            @if (backpack_user()->can('group-due-repayment'))
                            <li><a href='{{ backpack_url('group-due-repayment') }}'><i class="fa fa-plus-square-o"></i>
                                    <span>Group Due Repayment</span></a></li>
                            @endif
                            {{-- @if (backpack_user()->can('list-group-due-repayment'))<li><a href='{{ backpack_url('group-due-repayment') }}'><i class="fa fa-plus-square-o"></i> <span>Group Due Repayment</span></a></li>@endif --}}
                        </ul>
                    </li>
                @endif

                @if (backpack_user()->can('view-all-loan'))
                    <li><a href="{{ backpack_url('loandisbursement') }}"><i class='fa fa-tag'></i> <span>View All
                                Loan</span><span class="pull-right-container"><span
                                    class="label label-info pull-right">{{ \App\Models\Loan::count('id') }}</span></span></a>
                    </li>
                @endif
                @if (backpack_user()->can('change-branch-of-loan'))
                    <li><a href="{{ backpack_url('loan-disbursement-branch') }}"><i class='fa fa-tag'></i> <span>Change
                                Branch of Loan</span></a></li>
                @endif
                @if (backpack_user()->can('pending-approval'))
                    <li><a href="{{ backpack_url('disbursependingapproval') }}"><i class='fa fa-tag'></i><span>Pending
                                Approval</span><span class="pull-right-container"><span
                                    class="label label-warning pull-right">{{ \App\Models\DisbursePendingApproval::count('id') }}</span></span></a>
                    </li>
                @endif
                @if (backpack_user()->can('loan-approved'))
                    <li><a href="{{ backpack_url('disburseawaiting') }}"><i class='fa fa-tag'></i>
                            <span>Loan Approved</span><span class="pull-right-container"><span
                                    class="label label-success pull-right">{{ \App\Models\LoanAwaiting::count('id') }}</span></span></a>
                    </li>
                @endif
                @if (backpack_user()->can('loan-activated'))
                    <li><a href="{{ backpack_url('loanoutstanding') }}"><i class='fa fa-tag'></i>
                            <span>Loan Activated</span><span class="pull-right-container"><span
                                    class="label label-success pull-right">{{ \App\Models\LoanOutstanding::count('id') }}</span></span></a>
                    </li>
                @endif
                @if (backpack_user()->can('loan-completed'))
                    <li><a href="{{ backpack_url('disburseclosed') }}"><i class='fa fa-tag'></i> <span>Loan
                                Completed</span><span class="pull-right-container"><span
                                    class="label label-success pull-right">{{ \App\Models\LoanClosed::count('id') }}</span></span></a>
                    </li>
                @endif
                @if (backpack_user()->can('loan-declined'))
                    <li><a href="{{ backpack_url('disbursedeclined') }}"><i class='fa fa-tag'></i> <span>Loan
                                Declined</span><span class="pull-right-container"><span
                                    class="label label-danger pull-right">{{ \App\Models\LoanDeclined::count('id') }}</span></span></a>
                    </li>
                @endif
                @if (backpack_user()->can('loan-canceled'))
                    <li><a href="{{ backpack_url('disbursecanceled') }}"><i class='fa fa-tag'></i> <span>Loan
                                Canceled</span><span class="pull-right-container"><span
                                    class="label label-danger pull-right">{{ \App\Models\LoanCanceled::count('id') }}</span></span></a>
                    </li>
                @endif
                {{-- @if (backpack_user()->can('list-disbursement-withdrawn'))<li><a href="{{ backpack_url('disbursewithdrawn') }}"><i class='fa fa-tag'></i> <span>Loan Withdrawn</span><span class="pull-right-container"><span class="label label-danger pull-right">{{\App\Models\DisburseWithdrawn::count('id')}}</span></span></a></li>@endif --}}
                @if (backpack_user()->can('loan-write-off'))
                    <li><a href="{{ backpack_url('disbursewrittenoff') }}"><i class='fa fa-tag'></i><span>Loan Write
                                Off</span><span class="pull-right-container"><span
                                    class="label label-danger pull-right">{{ \App\Models\DisburseWrittenOff::count('id') }}</span></span></a>
                    </li>
                @endif

                <li class="sep-p">
                    <hr>
                </li>

                @if (backpack_user()->can('loan-objective'))
                    <li><a href="{{ backpack_url('loanobjective') }}"><i class='fa fa-tag'></i> <span>Loan
                                Objective</span></a></li>
                @endif
                @if (backpack_user()->can('transaction-type'))
                    <li><a href="{{ backpack_url('transactiontype') }}"><i class='fa fa-tag'></i><span>Transaction
                                Type</span></a></li>
                @endif
                @if (backpack_user()->can('import-loans'))
                    <li><a href="{{ backpack_url('import-loan/create') }}"><i class='fa fa-download'></i>
                            <span>Import Loans</span></a></li>
                @endif
                <li><a href="{{ backpack_url('import-for-mkt/create') }}"><i class='fa fa-download'></i>
                    <span>Import Loan with Repayment</span></a></li>

            </ul>
        </li>
    @endif

    @if (backpack_user()->can('deposit-payment') ||
        backpack_user()->can('paid-disbursement') ||
        backpack_user()->can('due-repayments') ||
        backpack_user()->can('late-repayments') ||
        backpack_user()->can('add-loan-repayment') ||
        backpack_user()->can('list-repayments') ||
        backpack_user()->can('import-loan-repayments') ||
        backpack_user()->can('paid-support-funds') ||
        backpack_user()->can('coming-repayments'))
        <li class="treeview"><a href="#"><i class="fa fa-users"></i> <span>MANAGE PAYMENT</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">

                @if (backpack_user()->can('deposit-payment'))
                    <li class="treeview"><a href="#"><i class="fa fa-users"></i> <span>Deposit Payment</span>
                            <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">

                            <li><a href='{{ backpack_url('depositpending') }}'><i class="fa fa-plus-square-o"></i>
                                    <span>List Pending Deposit</span></a></li>

                            <li><a href='{{ backpack_url('loandisburesementdepositu/create') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Add Deposits</span></a></li>

                            <li><a href='{{ backpack_url('loandisburesementdepositu') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>List Deposits</span></a></li>


                        </ul>
                    </li>
                @endif

                @if (backpack_user()->can('paid-disbursement'))
                    <li class="treeview"><a href="#"><i class="fa fa-users"></i> <span>Paid Disbursement</span>
                            <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href='{{ backpack_url('disbursementpending') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>List Pending Disbursement</span></a>
                            </li>

                            <li><a href='{{ backpack_url('my-paid-disbursement/create') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Add Disbursement</span></a></li>

                            <li><a href='{{ backpack_url('my-paid-disbursement') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>List Disbursement</span></a></li>
                        </ul>
                    </li>
                @endif

                @if (backpack_user()->can('due-repayments'))
                    <li><a href="{{ url('admin/due-repayment-list') }}"><i class='fa fa-tag'></i> <span>Due
                                Repayments</span></a></li>
                @endif
                @if (backpack_user()->can('late-repayments'))
                    <li><a href="{{ url('admin/late-repayment-list') }}"><i class='fa fa-tag'></i> <span>Late
                                Repayments</span></a></li>
                @endif
                @if (backpack_user()->can('add-loan-repayment'))
                    <li><a href='{{ backpack_url('addloanrepayment') }}'><i class="fa fa-plus-square-o"></i>
                            <span>Add Loan Repayment</span></a></li>
                @endif
                @if (backpack_user()->can('list-repayments'))
                    <li><a href='{{ backpack_url('/report/loan-repayments') }}'><i class="fa fa-plus-square-o"></i>
                            <span>List Repayments</span></a></li>
                @endif
                @if (backpack_user()->can('import-loan-repayments'))
                    <li><a href="{{ backpack_url('import-loan-repayment/create') }}"><i class='fa fa-download'></i>
                            <span>Import Loan Repayments</span></a></li>
                @endif

                @if (backpack_user()->can('paid-support-funds'))
                    <li><a href="{{ url('admin/paid-support-fund') }}"><i class='fa fa-tag'></i> <span>Paid Support
                                Funds</span></a></li>
                @endif
                @if (companyReportPart() == 'company.mkt')
                    @if (backpack_user()->can('coming-repayments'))
                        <li><a href="{{ url('admin/comming-repayment') }}"><i class='fa fa-plus-square-o'></i>
                                <span>Coming Repayments</span></a></li>
                    @endif
                @endif


            </ul>
        </li>
    @endif

    @if (backpack_user()->can('compulsories-list') ||
        backpack_user()->can('compulsory-active') ||
        backpack_user()->can('compulsory-completed') ||
        backpack_user()->can('compulsory-transactions') ||
        backpack_user()->can('cash-withdrawals') ||
        backpack_user()->can('import-saving-withdrawal'))
        <li class="treeview">
            <a href="#"><i class="fa fa-users"></i> <span>COMPULSORY SAVING</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">

                @if (backpack_user()->can('compulsories-list'))
                    <li><a href='{{ backpack_url('compulsorysavinglist') }}'><i class='fa fa-tag'></i>
                            <span>Compulsories List</span></a></li>
                @endif
                @if (backpack_user()->can('compulsory-active'))
                    <li><a href='{{ backpack_url('compulsorysavingactive') }}'><i class='fa fa-tag'></i>
                            <span>Compulsory Active</span></a></li>
                @endif
                @if (backpack_user()->can('compulsory-completed'))
                    <li><a href='{{ backpack_url('compulsorysavingcompleted') }}'><i class='fa fa-tag'></i>
                            <span>Compulsory Completed</span></a></li>
                @endif
                @if (backpack_user()->can('compulsory-transactions'))
                    <li><a href='{{ backpack_url('compulsory-saving-transaction') }}'><i class='fa fa-tag'></i>
                            <span>Compulsory Transactions</span></a></li>
                @endif
                @if (backpack_user()->can('cash-withdrawals'))
                    <li><a href='{{ backpack_url('cashwithdrawal') }}'><i class='fa fa-tag'></i> <span>Cash
                                Withdrawal</span></a></li>
                @endif

                @if (backpack_user()->can('import-saving-withdrawal'))
                    <li><a href='{{ backpack_url('import-compulsory-saving/create') }}'><i class='fa fa-tag'></i>
                            <span>Import
                                Saving Withdrawal</span></a></li>
                @endif

            </ul>
        </li>
    @endif

    @if (backpack_user()->can('open-saving-account') ||
        backpack_user()->can('activated-account') ||
        backpack_user()->can('add-deposit') ||
        backpack_user()->can('add-withdrawal') ||
        backpack_user()->can('transaction-list'))
        <li class="treeview">
            <a href="#"><i class="fa fa-users"></i> <span>MANAGE SAVING</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">


                @if (backpack_user()->can('open-saving-account'))
                    <li><a href='{{ backpack_url('open-saving-account/create') }}'><i
                                class="fa fa-plus-square-o"></i>
                            <span>Open Saving Account</span></a></li>
                @endif

                @if (backpack_user()->can('activated-account'))
                    <li><a href='{{ backpack_url('/saving-activated') }}'><i class='fa fa-tag'></i> <span>Activated
                                Account</span></a></li>
                @endif

                @if (backpack_user()->can('add-deposit'))
                    <li><a href='{{ backpack_url('/saving-deposit/create') }}'><i class='fa fa-tag'></i> <span>Add
                                Deposit</span></a></li>
                @endif

                @if (backpack_user()->can('add-withdrawal'))
                    <li><a href='{{ backpack_url('/saving-withdrawal/create') }}'><i class='fa fa-tag'></i> <span>Add
                                Withdrawal</span></a></li>
                @endif

                @if (backpack_user()->can('transaction-list'))
                    <li><a href='{{ backpack_url('/saving-transaction') }}'><i class='fa fa-tag'></i>
                            <span>Transaction
                                List</span></a></li>
                @endif
            </ul>
        </li>
    @endif

    @if (backpack_user()->can('loans-transfer'))
        <li class="treeview">
            <a href="#"><i class="fa fa-bookmark"></i> <span>MANAGE TRANSFER</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">

                <li><a href='{{ backpack_url('loan-pending-transfer') }}'><i class='fa fa-tag'></i> <span>Loans
                            Transfer</span></a></li>

                {{-- @if (backpack_user()->can('list-transfer'))<li><a href='{{ backpack_url('loan-transfer') }}'><i class='fa fa-tag'></i> <span>Loan Transfer</span></a></li>@endif --}}
            </ul>
        </li>
    @endif

    @if (backpack_user()->can('loan-products') ||
        backpack_user()->can('saving-products') ||
        backpack_user()->can('compulsory-products') ||
        backpack_user()->can('service-charges'))
        <li class="treeview">
            <a href="#"><i class="fa fa-money"></i> <span>MANAGE PRODUCT</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                @if (backpack_user()->can('loan-products'))
                    <li><a href='{{ backpack_url('loan-product') }}'><i class="fa fa-plus-square-o"></i><span>Loan
                                Product</span></a></li>
                @endif
                @if (backpack_user()->can('saving-products'))
                    <li><a href='{{ backpack_url('saving-product') }}'><i class="fa fa-plus-square-o"></i>
                            <span>Saving Product</span></a></li>
                @endif
                @if (backpack_user()->can('compulsory-products'))
                    <li><a href='{{ backpack_url('compulsory-product') }}'><i class='fa fa-tag'></i> <span>Compulsory
                                Product</span></a></li>
                @endif
                @if (backpack_user()->can('service-charges'))
                    <li><a href='{{ backpack_url('charge') }}'><i class='fa fa-tag'></i> <span>Service
                                Charges</span></a></li>
                @endif

                {{-- <li><a href='{{ backpack_url('compulsory-product-type') }}'><i class="fa fa-plus-square-o"></i> <span>Compulsory Product Type</span></a></li> --}}
            </ul>
        </li>

    @endif


    @if (backpack_user()->can('general-journal') ||
        backpack_user()->can('transfers') ||
        backpack_user()->can('expense') ||
        backpack_user()->can('other-income') ||
        backpack_user()->can('chart-of-account') ||
        backpack_user()->can('currency') ||
        backpack_user()->can('import-journal') ||
        backpack_user()->can('frd-accounts'))
        <li class="treeview">
            <a href="#"><i class="fa fa-book"></i> <span>ACCOUNTING</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                @if (backpack_user()->can('general-journal'))
                    <li><a href="{{ backpack_url('general-journal') }}"><i
                                class="fa fa-plus-square-o"></i><span>{{ _t('General Journal') }}</span></a></li>
                @endif

                @if (backpack_user()->can('transfers'))
                    <li><a href="{{ backpack_url('transfer') }}"><i class='fa fa-tag'></i> <span>Transfer</span></a>
                    </li>
                @endif

                @if (backpack_user()->can('expense'))
                    <li><a href="{{ backpack_url('expense') }}"><i class="fa fa-plus-square-o"></i> <span>
                                @if (companyReportPart() == 'company.moeyan')
                                    Cash Out
                                @else
                                    Expense
                                @endif
                            </span>
                        </a></li>
                @endif
                @if (backpack_user()->can('other-income'))
                    <li><a href="{{ backpack_url('journal-profit') }}"><i class="fa fa-plus-square-o"></i> <span>
                                @if (companyReportPart() == 'company.moeyan')
                                    Cash In
                                @else
                                    Other income
                                @endif
                            </span></a></li>

                @endif
                @if (backpack_user()->can('chart-of-account'))
                    <li><a href='{{ backpack_url('account-chart') }}'><i class="fa fa-plus-square-o"></i><span>Chart
                                of Account</span></a></li>
                @endif

                <li class="sep-p">
                    <hr>
                </li>
                @if (backpack_user()->can('currency'))
                    <li><a href='{{ backpack_url('currency') }}'><i
                                class="fa fa-plus-square-o"></i><span>Currency</span></a></li>
                @endif
                @if (backpack_user()->can('import-journal'))
                    <li><a href='{{ backpack_url('import-journal') }}'><i
                                class="fa fa-plus-square-o"></i><span>Import Journal / Expense</span></a></li>
                @endif

                @if (backpack_user()->can('frd-accounts'))
                    <li><a href='{{ backpack_url('frd-account-setting/create') }}'><i
                                class='fa fa-plus-square-o'></i><span>FRD Accounts</span></a></li>
                @endif
                {{-- @if (backpack_user()->can('list-frd-account-setting2'))
                    <li><a href='{{ backpack_url('frd-account-setting2/create') }}'><i
                                class='fa fa-plus-square-o'></i><span>FRD Account Setting 2</span></a></li>
                @endif --}}
            </ul>
        </li>
    @endif
    @if (backpack_user()->can('add-capital') ||
        backpack_user()->can('withdraw-capital') ||
        backpack_user()->can('shareholder'))
        <li class="treeview">
            <a href="#"><i class="fa fa-bookmark"></i> <span>MANAGE CAPITAL</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                @if (backpack_user()->can('add-capital'))
                    <li><a href='{{ backpack_url('capital') }}'><i class='fa fa-tag'></i> <span>Add
                                Capital</span></a></li>
                @endif
                @if (backpack_user()->can('withdraw-capital'))
                    <li><a href='{{ backpack_url('capital-withdraw') }}'><i class='fa fa-tag'></i> <span>Withdraw
                                Capital</span></a></li>
                @endif
                @if (backpack_user()->can('shareholder'))
                    <li><a href='{{ backpack_url('shareholder') }}'><i class='fa fa-tag'></i>
                            <span>Shareholder</span></a></li>
                @endif

            </ul>
        </li>
    @endif
    @if (backpack_user()->can('summary-report') ||
        backpack_user()->can('accounting-report') ||
        backpack_user()->can('group-loan-report') ||
        backpack_user()->can('loan-reports') ||
        backpack_user()->can('payment-reports') ||
        backpack_user()->can('saving-reports') ||
        backpack_user()->can('compulsory-saving-reports') ||
        backpack_user()->can('staff-reports'))
        <li class="treeview">
            <a href="#"><i class="fa fa-list"></i> <span>REPORT</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                {{-- <li><a href="{{ backpack_url('') }}"><i class="fa fa-circle-o"></i> <span>Borrows Report</span></a></li> --}}
                @if (backpack_user()->can('summary-report'))
                    <li><a href='{{ backpack_url('summary-report') }}'><i class='fa fa-tag'></i><span>Summary
                                Report</span></a></li>
                @endif
                @if (backpack_user()->can('accounting-report'))
                    <li><a href='{{ backpack_url('report-accounting') }}'><i class='fa fa-tag'></i><span>Accounting
                                Report</span></a></li>
                @endif

                @if (backpack_user()->can('accounting-frd-report'))
                    <li><a href='{{ backpack_url('report-account-external') }}'><i
                                class='fa fa-tag'></i><span>Accounting FRD Report </span></a></li>
                @endif

                @if (backpack_user()->can('group-loan-report'))
                    <li><a href='{{ backpack_url('group-report') }}'><i class='fa fa-tag'></i><span>Group Loan
                                Report</span></a></li>
                @endif

                @if (backpack_user()->can('loan-reports'))
                    <li class="treeview"><a href="#"><i class="fa fa-tag"></i> <span>Loan Reports</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            @if (backpack_user()->can('customer-info'))
                            <li><a href='{{ backpack_url('/report/client-information') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Customer Info</span></a></li>
                            @endif
                            @if (backpack_user()->can('loan-outstanding'))
                            <li><a href='{{ backpack_url('/report/loan-outstanding') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Loan Outstanding</span></a></li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if (backpack_user()->can('payment-reports'))
                    <li class="treeview"><a href="#"><i class="fa fa-tag"></i> <span>Payment Reports</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            @if (backpack_user()->can('payment-deposits'))
                            <li><a href='{{ backpack_url('/report/payment-deposits') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Payment Deposits</span></a></li>
                            @endif
                            @if (backpack_user()->can('loan-disbursements'))
                            <li><a href='{{ backpack_url('/report/loan-disbursements') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Loan Disbursements</span></a></li>
                            @endif
                            @if (backpack_user()->can('loan-repayments'))
                            <li><a href='{{ backpack_url('/report/loan-repayments') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Loan Repayments</span></a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if (_can('saving-reports'))
                    <li class="treeview"><a href="#"><i class="fa fa-tag"></i> <span>Saving Reports</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">

                            <li><a href='{{ backpack_url('/report/saving-deposits') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Saving Deposits</span></a></li>

                            <li><a href='{{ backpack_url('/report/saving-withdrawals') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Saving Withdrawals</span></a></li>

                            <li><a href='{{ backpack_url('/report/saving-interests') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Saving Interests</span></a></li>

                        </ul>
                    </li>
                @endif
                @if (_can('compulsory-saving-reports'))
                    <li class="treeview"><a href="#"><i class="fa fa-tag"></i> <span>Compulsory Saving
                                Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            @if (backpack_user()->can('compulsory-saving-report'))
                            <li><a href='{{ backpack_url('/report/saving-report') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Saving Report</span></a></li>
                            @endif
                            @if (backpack_user()->can('compulsory-saving-deposits'))
                            <li><a href='{{ backpack_url('/report/compulsory-saving-deposits') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Saving Deposits</span></a></li>
                            @endif
                            @if (backpack_user()->can('compulsory-saving-withdrawals'))
                            <li><a href='{{ backpack_url('/report/compulsory-saving-withdrawals') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Saving Withdrawals</span></a></li>
                            @endif
                            @if (backpack_user()->can('compulsory-saving-accrued-interests'))
                            <li><a href='{{ backpack_url('/report/com-saving-accrued-interests') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Saving Accrued Interests</span></a>
                            </li>
                            @endif
                        </ul>
                    </li>
                @endif



                @if (backpack_user()->can('staff-reports'))
                    <li class="treeview"><a href="#"><i class="fa fa-tag"></i> <span>Staff Reports</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            @if (backpack_user()->can('co-transactions'))
                            <li><a href='{{ backpack_url('/report/officer-transaction') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>C.O Transactions</span></a></li>
                            @endif
                            @if (backpack_user()->can('staff-performance'))
                            <li><a href='{{ backpack_url('/report/staff-performance') }}'><i
                                        class="fa fa-plus-square-o"></i> <span>Staff Performance</span></a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            </ul>
        </li>

    @endif

    @if (backpack_user()->can('list-users') || backpack_user()->can('role-permission'))
        <li class="treeview">
            <a href="#"><i class="fa fa-group"></i> <span>USER</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">


                @if (backpack_user()->can('list-users'))
                    <li><a href="{{ backpack_url('user') }}"><i class="fa fa-tag"></i> <span>List Users</span></a>
                    </li>
                @endif
                @if (backpack_user()->can('role-permission'))
                    <li><a href="{{ backpack_url('role') }}"><i class="fa fa-tag"></i> <span>Role
                                Permission</span></a></li>
                @endif
                {{-- @if (backpack_user()->can('list-permission'))<li><a href="{{ backpack_url('permission') }}"><i class="fa fa-tag"></i> <span>Permissions</span></a></li>@endif --}}
            </ul>
        </li>
    @endif
    @if (backpack_user()->can('settings') ||
        backpack_user()->can('branches') ||
        backpack_user()->can('centers') ||
        backpack_user()->can('set-holiday') ||
        backpack_user()->can('audit-trails'))
        <li class="treeview">
            <a href="#"><i class="fa fa-cog"></i> <span>GENERAL SETTING</span> <i
                    class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                {{-- <li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-tag"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li> --}}
                @if (backpack_user()->can('settings'))
                    <li><a href='{{ backpack_url('setting') }}'><i class='fa fa-tag'></i> <span>Settings</span></a>
                    </li>
                @endif


                @if (backpack_user()->can('branches'))
                    <li><a href="{{ backpack_url('branch') }}"><i class='fa fa-tag'></i> <span>Branches</span></a>
                    </li>
                @endif
                @if (backpack_user()->can('centers'))
                    <li><a href="{{ backpack_url('centerleader') }}"><i class='fa fa-tag'></i>
                            <span>Centers</span></a></li>
                @endif
                @if (backpack_user()->can('set-holiday'))
                    <li><a href="{{ backpack_url('holidayschedule') }}"><i class='fa fa-tag'></i>
                            <span>Set Holiday</span></a></li>
                @endif
                @if (backpack_user()->can('audit-trails'))
                    <li><a href="{{ backpack_url('audit') }}"><i class='fa fa-area-chart'></i> <span>Audit
                                Trails</span></a></li>
                @endif

            </ul>
        </li>
    @endif

@endif
