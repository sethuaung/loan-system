@extends('backpack::layout')
@push('after_styles')
    <link rel="stylesheet"
        href="{{ asset('vendor/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('vendor/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link href="{{ asset('vendor/adminlte/plugins/iCheck/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet"
        type="text/css" />
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/> --}}
    <link href="{{ asset('vendor/adminlte/plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet"
        type="text/css" />

    {{-- <link rel="stylesheet" href="{{ asset('js/MonthPicker.css') }}"> --}}

    <link href="{{ asset('vendor/adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"
        type="text/css" />
@endpush
@section('header')
    <?php
    
    use App\Models\Loan;
    $year = \Carbon\Carbon::now()->format('Y');
    
    $month = \Carbon\Carbon::now()->format('m');
    
    $start_date = isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m') . '-1';
    $end_date = isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : \App\Helpers\IDate::getLastDayMonth(date('Y-m') . '-1');
    $loan_activated = \App\Models\Loan::where('status_note_date_activated', '>=', $start_date)
        ->where('status_note_date_activated', '<=', $end_date)
        ->where('branch_id', session('s_branch_id'))
        ->whereIn('disbursement_status', ['Activated', 'Closed'])
        ->count('id');
    
    $client_payment = \App\Models\LoanPayment::join(getLoanTable(), getLoanTable() . '.id', '=', 'loan_payments.disbursement_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->where('loan_payments.payment_date', '>=', $start_date)
        ->where('loan_payments.payment_date', '<=', $end_date)
        ->count('loan_payments.id');
    
    $penalty_amount = \App\Models\LoanPayment::where('payment_date', '>=', $start_date)
        ->where('payment_date', '<=', $end_date)
        ->sum('penalty_amount');
    
    $loan_payment = \App\Models\LoanPayment::join(getLoanTable(), getLoanTable() . '.id', '=', 'loan_payments.disbursement_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->whereIn(getLoanTable() . '.disbursement_status', ['Activated', 'Closed', 'Written-Off'])
        ->selectRaw('sum(interest + principle) as payment')
        ->where('payment_date', '>=', $start_date)
        ->where('payment_date', '<=', $end_date)
        ->first();
    
    $interest_collection = \App\Models\LoanPayment::join(getLoanTable(), getLoanTable() . '.id', '=', 'loan_payments.disbursement_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->where('loan_payments.payment_date', '<=', $end_date)
        ->where('loan_payments.payment_date', '>=', $start_date)
        ->sum('loan_payments.interest');
    
    $principal_collection = \App\Models\LoanPayment::join(getLoanTable(), getLoanTable() . '.id', '=', 'loan_payments.disbursement_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->where('loan_payments.payment_date', '<=', $end_date)
        ->where('loan_payments.payment_date', '>=', $start_date)
        ->sum('loan_payments.principle');
    
    $interest_collection_total = \App\Models\LoanPayment::where('payment_date', '<=', $end_date)->sum('interest');
    
    $principal_collection_total = \App\Models\LoanPayment::join(getLoanTable(), getLoanTable() . '.id', '=', 'loan_payments.disbursement_id')
        ->where('loan_payments.payment_date', '<=', $end_date)
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->sum('loan_payments.principle');
    
    //$principal_collection = \App\Models\LoanCalculate::where('date_s','>=',$start_date)
    //->where('date_s','<=',$end_date)->sum('principal_s');
    
    $interest_recive = \App\Models\Loan::where('status_note_date_activated', '>=', $start_date)
        ->where('status_note_date_activated', '<=', $end_date)
        ->where('disbursement_status', '=', 'Activated')
        ->sum('interest_receivable');
    
    $principle_recive = \App\Models\Loan::where('status_note_date_activated', '>=', $start_date)
        ->where('status_note_date_activated', '<=', $end_date)
        ->where('disbursement_status', '=', 'Activated')
        ->sum('principle_receivable');
    
    $saving_cp_deposit = \App\Models\CompulsorySavingActive::join(getLoanTable(), getLoanTable() . '.id', '=', getLoanCompulsoryTable() . '.loan_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->where(getLoanTable() . '.status_note_date_activated', '>=', $start_date)
        ->where(getLoanTable() . '.status_note_date_activated', '<=', $end_date)
        ->where(getLoanCompulsoryTable() . '.compulsory_status', '=', 'Active')
        ->sum(getLoanCompulsoryTable() . '.principles');
    
    $saving_cp_interest = \App\Models\CompulsorySavingTransaction::join(getLoanTable(), getLoanTable() . '.id', '=', 'compulsory_saving_transaction.loan_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->where('compulsory_saving_transaction.tran_date', '>=', $start_date)
        ->where('compulsory_saving_transaction.tran_date', '<=', $end_date)
        ->where('compulsory_saving_transaction.train_type', '=', 'accrue-interest')
        ->sum('compulsory_saving_transaction.amount');
    
    $saving_cp_withdraw = \App\Models\CompulsorySavingTransaction::join(getLoanTable(), getLoanTable() . '.id', '=', 'compulsory_saving_transaction.loan_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->where('compulsory_saving_transaction.tran_date', '>=', $start_date)
        ->where('compulsory_saving_transaction.tran_date', '<=', $end_date)
        ->where('compulsory_saving_transaction.train_type', '=', 'withdraw')
        ->sum('compulsory_saving_transaction.amount');
    //dd($saving_cp_withdraw);
    
    
        $service_fee = \App\Models\LoanDepositU::where('loan_deposit_date','>=',$start_date)
                                                ->where('loan_deposit_date','<=',$end_date)
                                                ->sum('total_service_charge');
    
    
    $expense = \App\Models\GeneralJournalDetail::where('j_detail_date', '>=', $start_date)
        ->where('branch_id', session('s_branch_id'))
        ->where('j_detail_date', '<=', $end_date)
        ->where('tran_type', '=', 'expense')
        ->sum('cr');
    
    /* $outstanding=\App\Models\Loan::selectRaw('sum(interest_receivable + principle_receivable) as outstanding')
                    ->where('status_note_date_activated','>=',$start_date)
                        ->where('status_note_date_activated','<=',$end_date)
                        ->where('disbursement_status','=','Activated')->first();*/
    
    $loan_late = \App\Models\LoanCalculate::whereNull('date_p')
        ->whereDate('date_s', '<=', $start_date)
        ->sum('total_s');
    
    $loan_release = \App\Models\Loan::whereIn('disbursement_status', ['Activated', 'Closed', 'Written-Off'])
        ->where('status_note_date_activated', '<=', $end_date)
        ->where('branch_id', session('s_branch_id'))
        ->where('status_note_date_activated', '>=', $start_date)
        ->sum('loan_amount');
    
    $loan_release_total = \App\Models\Loan::whereIn('disbursement_status', ['Activated', 'Closed', 'Written-Off'])
        ->where('status_note_date_activated', '<=', $end_date)
        ->where('branch_id', session('s_branch_id'))
        ->sum('loan_amount');
    
    $total_interest = \App\Models\LoanCalculate::join(getLoanTable(), getLoanTable() . '.id', '=', getLoanCalculateTable() . '.disbursement_id')
        ->where(getLoanTable() . '.branch_id', session('s_branch_id'))
        ->whereIn(getLoanTable() . '.disbursement_status', ['Activated', 'Closed', 'Written-Off'])
        ->where('status_note_date_activated', '<=', $end_date)
        ->sum('interest_s');
    /*$loan_principle=\App\Models\loan::where('status_note_date_activated','>=',$start_date)
                    ->where('status_note_date_activated','<=',$end_date)
                    ->where('disbursement_status', 'Activated')
                    ->sum('principle_receivable');*/
    
    $pending = \App\Models\Loan::whereDate('loan_application_date', '>=', $start_date)
        ->whereDate('loan_application_date', '<=', $end_date)
        ->where('disbursement_status', 'Pending')
        ->count('id');
    
    $approved = \App\Models\Loan::whereDate('loan_application_date', '>=', $start_date)
        ->whereDate('loan_application_date', '<=', $end_date)
        ->where('disbursement_status', 'Approved')
        ->count('id');
    
    $active = \App\Models\Loan::whereDate('loan_application_date', '>=', $start_date)
        ->whereDate('loan_application_date', '<=', $end_date)
        ->where('disbursement_status', 'Activated')
        ->count('id');
    
    $declined = \App\Models\Loan::whereDate('loan_application_date', '>=', $start_date)
        ->whereDate('loan_application_date', '<=', $end_date)
        ->where('disbursement_status', 'Declined')
        ->count('id');
    
    $closed = \App\Models\Loan::whereDate('loan_application_date', '>=', $start_date)
        ->whereDate('loan_application_date', '<=', $end_date)
        ->where('disbursement_status', 'Closed')
        ->count('id');
    
    $p_outstanding = 0;
    $disburse = \App\Models\PaidDisbursement::whereDate('paid_disbursement_date', '<=', $end_date)->sum('loan_amount');
    $prin_repay = \App\Models\LoanPayment::whereDate('payment_date', '<=', $end_date)->sum('principle');
    $p_outstanding = $disburse - $prin_repay;
    
    //interest_outstanding
    $loan_cal = \App\Models\LoanCalculate::leftJoin(getLoanTable(), getLoanTable() . '.id', '=', getLoanCalculateTable() . '.disbursement_id')
        ->where('status_note_date_activated', '>=', $start_date)
        ->where('status_note_date_activated', '<=', $end_date)
        ->where('disbursement_status', 'Activated')
        ->sum(getLoanCalculateTable() . '.interest_s');
    
    $int_outstanding = $loan_cal - $interest_collection;
    $int_outstanding = $int_outstanding > 0 ? $int_outstanding : 0;
    
    //
    $st_date = isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m') . '-1';
    $e_date = isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : \App\Helpers\IDate::getLastDayMonth(date('Y-m') . '-1');
    
    //dd($st_date);
    
    ?>

    <section class="content-header">
        <h1>
            {{ trans('backpack::base.dashboard') }}
            {{--            <small>{{ trans('backpack::base.first_page_you_see') }}</small> --}}
        </h1>
        <ol class="breadcrumb">
            {{--            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li> --}}
            <li class="active">{{ trans('backpack::base.dashboard') }}</li>
        </ol>
    </section>
@endsection
@section('content')

    <div class="row">
        @if (_can('form-serch'))
            <div class="col-md-8 col-xs-12">
                <form action="{{ url('api/dashboard_search') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">

                        <div class="input-group input-group-sm">
                            <div class="input-group-addon date-range">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="reservation">
                            <input type="hidden" id="start-date" name="start_date" value="{{ $st_date }}">
                            <input type="hidden" id="end-date" name="end_date" value="{{ $e_date }}">
                            <span class="input-group-btn">
                                <input type="submit" class="btn btn-info btn-round radius-all"
                                    value="{{ _t('Search') }}">
                            </span>
                        </div>
                    </div>
                </form>

            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-12">
            <section class="">
                @if (_can('dashboard-amount'))
                    <div class="row">

                        <span style="padding-left: 50px">Loans Release</span> <br>
                        <!-- @if (companyReportPart() == 'company.quicken')
    <div class="col-lg-3 col-xs-6">
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h4>{{ $new_clients }}</h4>
                                        <p>Waiting Clients</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                    </div>
                                    <a href="{{ url('admin/client?condition=Waiting+Client') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
    @endif -->

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            {{-- <div class="small-box bg-aqua">
                            <div class="inner">
                                <h4>{{$client}}</h4>
                                <p>Total Clients</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-users" aria-hidden="true"></i>
                            </div>
                            <a href="{{url('admin/client')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div> --}}
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h4>{{ $loan_activated }}</h4>
                                    <p>Client Activated</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/loanoutstanding?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h4>{{ numb_format($loan_release, 0) }}</h4>

                                    <p>LOAN Dibursement</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{ url('admin/loanoutstanding?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>


                        <!-- Outstanding -->

                        {{--                    <div class="col-lg-3 col-xs-6"> --}}
                        {{--                        <!-- small box --> --}}
                        {{--                        <div class="small-box bg-red"> --}}
                        {{--                            <div class="inner"> --}}
                        {{--                                <h4>MMK {{numb_format($principle_recive+$interest_recive,0)}}</h4> --}}

                        {{--                                <p>Outstanding</p> --}}
                        {{--                            </div> --}}
                        {{--                            <div class="icon"> --}}
                        {{--                                <i class="fa fa-money" aria-hidden="true"></i> --}}
                        {{--                            </div> --}}
                        {{--                            --}}{{--                            <a href="{{url('admin/due-repayment-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> --}}
                        {{--                        </div> --}}
                        {{--                    </div> --}}
                        @php
                            if (companyReportPart() == 'company.quicken') {
                                $arr_acc = [];
                                $arr_begin = [];
                                $arr_leger = [];
                                $acc_chart_id = ['22'];
                                $branch_id = ['2'];
                                //dd($start_date,$end_date,$branch_id,$acc_chart_id);
                                $beg_leger = App\Models\ReportAccounting::getBeginGeneralLeger($start_date, $end_date, $branch_id, $acc_chart_id);
                                if ($beg_leger != null) {
                                    foreach ($beg_leger as $b) {
                                        $arr_acc[$b->acc_chart_id] = $b->acc_chart_id;
                                        $arr_begin[$b->acc_chart_id] = $b->amt ?? 0;
                                    }
                                }
                            
                                if ($branch_id == null) {
                                    $branch_id = [1];
                                }
                            
                                $gen_leger = App\Models\ReportAccounting::getGeneralLeger($start_date, $end_date, $branch_id, $acc_chart_id);
                            
                                if ($gen_leger != null) {
                                    foreach ($gen_leger as $k => $b) {
                                        $arr_acc[$b->acc_chart_id] = $b->acc_chart_id;
                                        $arr_leger[$b->acc_chart_id][$k] = $b;
                                    }
                                }
                            }
                            $arr_acc = $arr_acc ?? [];
                        @endphp
                        @foreach ($arr_acc as $acc)
                            <?php
                            $begin = isset($arr_begin[$acc]) ? $arr_begin[$acc] : 0;
                            $ac_o = optional(\App\Models\AccountChart::find($acc));
                            //dd($begin);
                            ?>
                            <?php
                            $current = isset($arr_leger[$acc]) ? $arr_leger[$acc] : [];
                            $t_dr = 0;
                            $t_cr = 0;
                            ?>
                        @endforeach
                        @php
                            $current = $current ?? [];
                            $begin = $begin ?? 0;
                        @endphp
                        @if (count($current) > 0)
                            @foreach ($current as $row)
                                <?php
                                $begin += ($row->dr ?? 0) - ($row->cr ?? 0);
                                
                                ?>
                            @endforeach
                        @endif
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    {{-- <!-- <h4>{{numb_format($principle_recive,0)}}</h4> --> --}}
                                    <h4>{{ numb_format($begin, 0) }}</h4>
                                    <p>Principal Outstanding </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/report/loan-outstanding?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h4>{{ numb_format($total_interest - $interest_collection, 0) }}</h4>

                                    <p>Interest Outstanding </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/report/loan-outstanding?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>





                        <br><span style="padding-left: 50px">Loans Collecttions</span> <br>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4> {{ number_format($client_payment, 0) }}</h4>

                                    <p>Repayment Transactions</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/report/loan-repayments?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4>{{ number_format($loan_payment->payment, 0) }}</h4>

                                    <p>Repayments Collection </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/report/loan-repayments?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4>{{ numb_format($principal_collection, 0) }}</h4>

                                    <p>Principal Collection </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                {{--                            <a href="" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a> --}}
                                <a href="{{ url('admin/report/loan-repayments?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4>{{ number_format($interest_collection, 0) }}</h4>

                                    <p>Interest Collection </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                {{--                            <a href="" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a> --}}
                                <a href="{{ url('admin/report/loan-repayments?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>






                        <br><span style="padding-left: 50px">Other</span> <br>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h4>{{ numb_format($saving_cp_deposit, 0) }}</h4>
                                    <p>Compulsory Saving</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/compulsorysavingactive?status_note_date_activated=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h4>{{ numb_format(str_replace('-', '', $saving_cp_withdraw), 0) }}</h4>
                                    <p>Compulsory Withdraw</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/report/compulsory-saving-withdrawals?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h4>{{ numb_format($saving_cp_interest, 2) }}</h4>
                                    <p>Compulsory Accrue Interest</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/report/com-saving-accrued-interests?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        @if (companyReportPart() == 'company.quicken')
                            <div class="col-lg-3 col-xs-6">
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h4>{{ numb_format($penalty_amount, 0) }}</h4>
                                        <p>Penalty Amount Total</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                    </div>
                                    <a href="{{ url('admin/report/loan-repayments?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                        class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        @endif


                        <?php
                    if(companyReportPart() == "company.quicken"){

                    foreach($fees_arr as $key => $fee){

                        if($key == 260 || $key == 204){?>
                        <div class="col-md-3">
                            <div class="panel panel-flat">
                                <div class="panel-body" style="padding: 0px!important">
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h4>{{ number_format($fee, 0) }}</h4>

                                            <p>@php
                                                if ($key == '260') {
                                                    echo 'Service Fees';
                                                } elseif ($key == '204') {
                                                    echo 'Welfare Fund';
                                                }
                                            @endphp</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </div>
                                        <a href="{{ url('admin/report/loan-disbursements?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                            class="small-box-footer">More info <i
                                                class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }
                    }
                    }else {?>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h4>{{ numb_format($service_fee, 0) }}</h4>
                                    <p>Service Charge</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                </div>
                                <a href="{{ url('admin/report/loan-disbursements?from_to=%7B"from"%3A"' . $start_date . '"%2C"to"%3A"' . $end_date . '"%7D') }}"
                                    class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <?php }?>

                        <div class="col-lg-12 col-xs-12 mt-5">
                            <label for="">Upcoming Payments and Collections</label>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Ref</th>
                                        <th scope="col">Clients</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Principle</th>
                                        <th scope="col">Interest</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 col-xs-12 mt-5">
                            <label for="">Amount Collected for Today</label>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Ref</th>
                                        <th scope="col">Clients</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Principle</th>
                                        <th scope="col">Interest</th>
                                        <th scope="col">L.O Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <!--<div class="col-lg-3 col-xs-6">
                                <div class="small-box bg-red">
                                    <div class="inner">
                                        <h4>{{ numb_format($loan_late, 2) }}</h4>

                                        <p>Late Repayment</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    <a href="{{ url('admin/late-repayment-list') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>-->

                    </div>
                @endif


            </section>
        </div>
    </div>

@endsection


@push('after_scripts')
    @if (companyReportPart() != 'company.mkt')
        {{--    <script src="{{ asset('js/jquery-ui.min.js') }}"></script> --}}
        {{-- <script src="{{ asset('js/MonthPicker.js') }}"></script> --}}

        <script src="{{ asset('vendor/adminlte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/plugins/moment/moment.min.js') }}"></script>

        <script src="{{ asset('vendor/adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

        {{-- <script src="{{ asset('js/jquery.maskedinput.js') }}"></script> --}}
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> --}}
        <script src="{{ asset('vendor/adminlte/plugins/jquery-maskedinput/jquery.maskedinput.min.js') }}"></script>

        <script src="{{ asset('vendor/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>


        <script src="{{ asset('vendor/adminlte') }}/plugins/amchart/amcharts.js" type="text/javascript"></script>
        <script src="{{ asset('vendor/adminlte') }}/plugins/amchart/serial.js" type="text/javascript"></script>
        <script src="{{ asset('vendor/adminlte') }}/plugins/amchart/pie.js" type="text/javascript"></script>
        <script src="{{ asset('vendor/adminlte') }}/plugins/amchart/light.js" type="text/javascript"></script>
        <script src="{{ asset('vendor/adminlte') }}/plugins/amchart/export.js" type="text/javascript"></script>

        {{-- <script src="http://cloudmfi.com/dev/icloudfinance/public/assets/plugins/amcharts/amcharts.js" type="text/javascript"></script> --}}
        {{-- <script src="http://cloudmfi.com/dev/icloudfinance/public/assets/plugins/amcharts/serial.js" type="text/javascript"></script> --}}
        {{-- <script src="http://cloudmfi.com/dev/icloudfinance/public/assets/plugins/amcharts/pie.js" type="text/javascript"></script> --}}
        {{-- <script src="http://cloudmfi.com/dev/icloudfinance/public/assets/plugins/amcharts/themes/light.js" type="text/javascript"></script> --}}
        {{-- <script src="http://cloudmfi.com/dev/icloudfinance/public/assets/plugins/amcharts/plugins/export/export.min.js" type="text/javascript"></script> --}}

        {{-- <script src="{{ asset('vendor/adminlte') }}/bower_components/jquery/dist/jquery.min.js"></script> --}}

        <!-- include select2 js-->
        <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
        <script>
            $(function() {

                //    alert('he');
            })
        </script>

        <script>
            AmCharts.makeChart("monthly_actual_expected_data", {
                "type": "serial",
                "theme": "light",
                "autoMargins": true,
                "marginLeft": 30,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "fontFamily": 'Open Sans',
                "color": '#888',

                "dataProvider": [{
                    "month": "Jan 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Feb 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Mar 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Apr 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "May 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Jun 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Jul 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Aug 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Sep 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Oct 2018",
                    "actual": 0,
                    "expected": 0
                }, {
                    "month": "Nov 2018",
                    "actual": 0,
                    "expected": 56766.6667
                }, {
                    "month": "Dec 2018",
                    "actual": 170300.01,
                    "expected": 56766.6667
                }, {
                    "month": "Jan 2019",
                    "actual": 0,
                    "expected": 56766.6667
                }],
                "valueAxes": [{
                    "axisAlpha": 0,

                }],
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                    "bullet": "round",
                    "bulletSize": 8,
                    "lineColor": "#370fc6",
                    "lineThickness": 4,
                    "negativeLineColor": "#0dd102",
                    "title": "Actual",
                    "type": "smoothedLine",
                    "valueField": "actual"
                }, {
                    "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                    "bullet": "round",
                    "bulletSize": 8,
                    "lineColor": "#d1655d",
                    "lineThickness": 4,
                    "negativeLineColor": "#d1cf0d",
                    "title": "Expected",
                    "type": "smoothedLine",
                    "valueField": "expected"
                }],
                "categoryField": "month",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "labelRotation": 30,

                },
                "export": {
                    "enabled": true,
                    "libs": {
                        "path": "http://cloudmfi.com/dev/icloudfinance/public/assets/plugins/amcharts/plugins/export/libs/"
                    }
                },
                "legend": {
                    "position": "bottom",
                    "marginRight": 100,
                    "autoMargins": false
                },


            });
        </script>
        {{-- <script src="http://cloudmfi.com/dev/icloudfinance/public/assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script> --}}

        {{--    <script src="{{ asset('vendor/adminlte') }}/plugins/amchart/chart.min.js" type="text/javascript"></script> --}}
        {{--    <script src="{{ asset('vendor/adminlte/') }}/bower_components/chart.js/Chart.js"></script> --}}
        {{-- <script src="{{ asset('vendor/adminlte/') }}/bower_components/chart2/Chart.js"></script> --}}

        <script src="{{ asset('vendor/adminlte/') }}/bower_components/chart.js/Chart.js"></script>
        <script>
            window.ChartV1 = Chart;
        </script>
        <script src="{{ asset('vendor/adminlte/') }}/bower_components/chart2/Chart.js"></script>
        <script>
            window.ChartV2 = Chart;
            window.Chart = window.ChartV1;
        </script>

        <script src="{{ asset('vendor/adminlte/') }}/bower_components/chart.js/Chart.js"></script>
        <script>
            window.ChartV1 = Chart;
        </script>
        <script src="{{ asset('vendor/adminlte/') }}/bower_components/chart2/Chart.js"></script>
        <script>
            window.ChartV2 = Chart;
            window.Chart = window.ChartV2;
        </script>


        <script src="{{ asset('vendor/adminlte/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
        </script>
        <script>
            var ctx3 = document.getElementById("loan_status_pie").getContext("2d");
            var data3 = [{
                "label": "Pending",
                "value": "",
                "color": "#FF8A65",
                "highlight": "#FF8A65",
                "link": "{{ url('admin/disbursependingapproval') }}",
                "class": "warning-300"
            }, {
                "label": "Approved",
                "value": 0,
                "color": "#64B5F6",
                "highlight": "#64B5F6",
                "link": "{{ url('admin/disburseawaiting') }}",
                "class": "primary-300"
            }, {
                "label": "Disbursed",
                "value": 3,
                "color": "#1565C0",
                "highlight": "#1565C0",
                "link": "{{ url('admin/disbursementpending') }}",
                "class": "primary-800"
            }, {
                "label": "Declined",
                "value": 20,
                "color": "#EF5350",
                "highlight": "#EF5350",
                "link": "{{ url('admin/disbursedeclined') }}",
                "class": "danger-400"
            }, {
                "label": "Closed",
                "value": 10,
                "color": "#66BB6A",
                "highlight": "#66BB6A",
                "link": "{{ url('admin/disburseclosed') }}",
                "class": "success-400"
            }];

            // var myPieChart = Chart.noConflict();

            var myPieChart = new ChartV1(ctx3).Pie(data3, {
                segmentShowStroke: true,
                segmentStrokeColor: "#fff",
                segmentStrokeWidth: 0,
                animationSteps: 100,
                tooltipCornerRadius: 0,
                animationEasing: "easeOutBounce",
                animateRotate: true,
                animateScale: false,
                responsive: true,

                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 99, 132)'
                    }
                }
            });
        </script>


        <script>
            jQuery(document).ready(function($) {
                $('[data-bs-month]').each(function() {
                    $(this).MonthPicker({
                        Button: false,
                        MonthFormat: 'yy-mm',
                        ShowAnim: 'slideDown',
                        UseInputMask: true,
                        OnAfterChooseMonth: function(e) {
                            //console.log($(this).val());
                        }
                    });
                });
            });
        </script>


        {{-- range date --}}
        @push('after_scripts')
            <script type="text/javascript">
                $(function() {
                    /*       $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                               checkboxClass: 'icheckbox_minimal-red',
                               radioClass: 'iradio_minimal-red'
                           });*/

                    var start = moment().subtract(29, 'days');
                    //console.log(start);
                    var end = moment();

                    function cb(start, end) {
                        $('#reservation span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
                        $('#start-date').val(start.format('YYYY-MM-DD'));
                        $('#end-date').val(end.format('YYYY-MM-DD'));
                    }

                    $('#reservation').daterangepicker({
                        /*     startDate: start,
                                            $st_date = isset($_REQUEST['start_date'])?$_REQUEST['start_date']:(date('Y-m')).'-1';
                                  $e_date
                                                 endDate: end, */


                        startDate: '{{ $st_date }}',
                        endDate: '{{ $e_date }}',
                        locale: {
                            format: 'YYYY-MM-DD'
                        },
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                                'month').endOf('month')],
                            'This Year': [moment().startOf('year'), moment().endOf('year')],
                            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year')
                                .endOf('year')
                            ],
                            'Total': [moment('2019-01-01'), moment()]

                        }
                    }, cb).on('apply.daterangepicker', function(ev, picker) {
                        var st = (picker.startDate.format('YYYY-MM-DD'));
                        var ed = (picker.endDate.format('YYYY-MM-DD'));

                        $('#start-date').val(st);
                        $('#end-date').val(ed);

                    });

                    cb(start, end);

                });
            </script>
        @endpush
        {{-- range date --}}

        <script>
            <?php
            
            $weeks = [
                1 => 'Jan',
                2 => 'Feb',
                3 => 'Mar',
                4 => 'Apr',
                5 => 'May',
                6 => 'Jun',
                7 => 'Jul',
                8 => 'Aug',
                9 => 'Sep',
                10 => 'Oct',
                11 => 'Nov',
                12 => 'Dec',
            ];
            
            if ($weeks != null) {
                $arr = [];
                $n = 0;
                $total = 0;
                foreach ($weeks as $week => $key) {
                    $strdate = strtotime($end_date);
                    $year = Date('Y', $strdate);
                    $paid_disbursement = \App\Models\PaidDisbursement::whereMonth('paid_disbursement_date', '=', $week)
                        ->whereYear('paid_disbursement_date', '=', $year)
                        ->sum('loan_amount');
            
                    $paid_disbursement_count = \App\Models\PaidDisbursement::whereMonth('paid_disbursement_date', '=', $week)
                        ->whereYear('paid_disbursement_date', '=', $year)
                        ->count();
            
                    $total = $paid_disbursement;
            
                    $total_sale = $total;
            
                    if ($paid_disbursement > 0) {
                        $arr[$week] = $total;
                    } else {
                        if ($paid_disbursement_count > 0) {
                            $arr[$week] = $total_sale;
                        } else {
                            $arr[$week] = 0;
                        }
                    }
                }
            }
            
            $data1 = [];
            
            foreach ($weeks as $k => $v) {
                $data1[] = isset($arr[$k]) ? $arr[$k] : 0;
            }
            
            //dd($data1);
            
            ?>


            {{-- var salesChartData = {
            labels: @json(array_values($weeks)),
            datasets: [
                {
                    label: 'Analyse',
                    fillColor: '#3e95cd',
                    strokeColor: '#5c656b',
                    pointColor: '#6ED166',
                    pointStrokeColor: '#c1c7d1',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgb(220,220,220)',
                    data: @json($data1)
                },

            ]
        }; --}}


            var myData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            var myColors = [];

            $.each(myData, function(index, value) {
                if (value % 2) {
                    myColors[index] = '#99a7af';
                } else {
                    myColors[index] = "#6dcd95";
                }
            });

            var ctx = document.getElementById('myChart').getContext('2d');
            // ctx.style.backgroundColor = 'rgba(255,0,0,255)';

            var chart = new ChartV2(ctx, {
                // The type of chart we want to create
                type: 'bar',

                // The data for our dataset
                data: {
                    labels: @json(array_values($weeks)),
                    datasets: [{
                        label: "Total:",
                        backgroundColor: myColors,
                        fill: false,
                        data: @json($data1)
                    }],
                },
                // Configuration options go here
                options: {
                    legend: {
                        display: false
                    },
                    scales: {},
                    tooltips: {
                        callbacks: {
                            /*     labelColor: function (tooltipItem, data) {
                                     if (tooltipItem.datasetIndex === 0) {
                                         return {
                                             // borderColor: "#FFFFFF",
                                             // backgroundColor: "#FFCD2E",
                                         };
                                     }
                                 }*/
                        }
                    }

                }
            });
        </script>

        {{-- <script src="{{ asset('js/chartBar.js?v=1')}}"></script> --}}
    @endif
@endpush
