<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SavingSchedule extends Model
{
    protected $table = 'saving_schedules';
}
