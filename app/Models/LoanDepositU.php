<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class LoanDepositU extends LoanDeposit
{
    use CrudTrait;

    public function addButtonCustom()
    {

        return '<a href="' . url("/admin/print-loan-deposit?loan_deposit_id={$this->id}") . '" data-remote="false" data-toggle="modal" data-target="#show-detail-modal" class="btn btn-xs btn-info"><i class="fa fa-print"></i></a>';

    }
}
