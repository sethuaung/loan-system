<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupDepositDetail extends Model
{
    protected $table = 'group_deposit_details';
}
