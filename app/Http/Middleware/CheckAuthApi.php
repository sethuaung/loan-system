<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckAuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('Authorization');
        $explode = explode("Bearer ",$header);
        if(isset($explode[1])){
            $user = User::where('remember_token',$explode[1])->first();
            if($user){
                return $next($request);
            }
            else{
                return response()->json(['status_code' => 400, 'message' => 'Token not found', 'data' => Null]);
            }
        }
        else{
            return response()->json(['status_code' => 400, 'message' => 'Token not found', 'data' => Null]);
        }
       
    }
}
