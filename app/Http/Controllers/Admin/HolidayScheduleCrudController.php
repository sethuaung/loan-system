<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\HolidayScheduleRequest as StoreRequest;
use App\Http\Requests\HolidayScheduleRequest as UpdateRequest;
use App\Models\HolidaySchedule;
use App\Models\LoanCalculate;

class HolidayScheduleCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\HolidaySchedule');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/holidayschedule');
        $this->crud->setEntityNameStrings('Set holiday', 'Set holiday');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'holiday_year',
            'label' => _t('Year'),

        ]);
        $this->crud->addColumn([
            'name' => 'holiday',
            'label' => _t('Holiday'),
        ]);
        $this->crud->addColumn([
            'name' => 'start_date',
            'label' => _t('Start Date'),
            'type'=>'date'
        ]);
        $this->crud->addColumn([
            'name' => 'end_date',
            'label' => _t('End Date'),
            'type'=>'date'
        ]);

        $this->crud->addColumn([
            'name' => 'change_date',
            'label' => _t('Change Date'),
            'type'=>'date'
        ]);

        $this->crud->addColumn([
            'name' => 'branch_id',
            'label' => 'Branch',
            'type' => 'closure',
           'function' => function($entry) {
                $branch = \App\Models\Branch::find(optional($entry)->branch_id);
                return optional($branch)->title;
           }
        ]);
        $this->crud->addColumn([
            'name' => 'note',
            'label' => _t('Note')
        ]);

//======================================================

        $this->crud->addField([
            'name' => 'holiday',
            'label' => _t('Holiday'),
//            'type' => 'number2',
//            'tab' => 'Information',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]
        ]);
        $this->crud->addField([
            'name' => 'option',
            'label' => _t('Option'),
              'type' => 'enum',
//            'tab' => 'Information',
                'attributes' => [
                    'class' => 'form-control',
                    'readonly' => 'readonly',
                ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]
        ]);



        $this->crud->addField([
            'name' => 'holiday_year',
            'label' => _t('Year'),
            'type' => 'number2',
            'default' => date('Y'),
//            'tab' => 'Information',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]
        ]);

        $this->crud->addField([
            'name' => 'start_date',
            'type' => 'date_picker',
            'label' => 'Start Date',
            'default' => date('Y-m-d') ,
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'yyyy-mm-dd',
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);

        $this->crud->addField([
            'name' => 'end_date',
            'type' => 'date_picker',
            'label' => 'End Date',
            'default' => date('Y-m-d') ,
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'yyyy-mm-dd',
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'name' => 'change_date',
            'type' => 'date_picker',
            'label' => 'Change Date',
            'default' => date('Y-m-d') ,
            // optional:
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'yyyy-mm-dd',
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'label' => "Branch",
            'type' => "select2_from_ajax_multiple",
            'name' => 'branch_ids',
            'entity' => 'branch_name',
            'attribute' => "title",
            'model' => \App\Models\Branch::class,
            'data_source' => url("api/get-branch"),
            'placeholder' => "Select a Branch",
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);

      
        $this->crud->addField([
            'name' => 'change_option',
            'label' => "Change Option",
            'type' => 'select_from_array',
            'options' => ['1' => 'Yes (Old & New Loans)', '0' => 'No (Only New Loans)'],
            'allows_null' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
        ]);

        $this->crud->addField([
            'name' => 'note',
            'label' => _t('Description / Reason to change collection date'),
            'type' => 'textarea',
            //'default' => date('Y'),
//            'tab' => 'Information',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ]
        ]);

        $this->crud->enableExportButtons();


//======================================================

        $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        if (_can2($this,'set-holiday')) {
            $this->crud->allowAccess(['list']);
        }
        if (_can2($this,'set-holiday')) {
            $this->crud->allowAccess(['list', 'create']);
        }
        if (_can2($this,'set-holiday')) {
            $this->crud->allowAccess(['list', 'update']);
        }
        if (_can2($this,'set-holiday')) {
            $this->crud->allowAccess(['list', 'delete']);
        }
    }


    public function store(StoreRequest $request)
    {
        // dd($request);
        $branches = $request->branch_ids;
        foreach ($branches as $branch){
            $store = HolidaySchedule::create([
                'holiday' => $request->holiday,
                'holiday_year' => $request->holiday_year,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'change_date' => $request->change_date,
                'note' => $request->note,
                'option' => $request->option,
                'branch_id' => $branch,
                'change_option' => $request->change_option,
            ]);
            
            if($store->change_option == 1){
                $schedules = LoanCalculate::where('date_s', '>=', date('Y-m-d',strtotime($store->start_date)))
                                           ->where('date_s', '<=', date('Y-m-d',strtotime($store->end_date)))
                                           ->where('payment_status','pending')
                                           ->get();

                foreach ($schedules as $schedule){
                    $schedule->date_s = $store->change_date;
                    $schedule->save();
                }
            }
        }
        return redirect('admin/holidayschedule');
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
