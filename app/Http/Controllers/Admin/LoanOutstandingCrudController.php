<?php

namespace App\Http\Controllers\Admin;
use App\Helpers\MFS;
use App\Models\Client;
use App\Models\Branch;
use App\Models\CompulsorySavingTransaction;
use App\Models\GeneralJournal;
use App\Models\GeneralJournalDetail;
use App\Models\Loan;
use App\Models\Loan2;
use App\Models\LoanCalculate;
use App\Models\LoanDeposit;
use App\Models\LoanPayment;
use App\Models\PaidDisbursement;
use App\Models\PaymentHistory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Carbon\Carbon;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LoanOutstandingRequest as StoreRequest;
use App\Http\Requests\LoanOutstandingRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class LoanOutstandingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class LoanOutstandingCrudController extends CrudController
{

    public function paymentPop(Request $request){

        //dd($request->all());
        $loan_id = $request->loan_id;
        $row = Loan::find($loan_id);
        //dd($row);
        return view ('partials.loan-payment.loan-payment-pop',['row'=>$row]);
    }

    public function showDetailsRow($id)
    {
        //$m_ = ProductSerial::where('product_id', $id)->get();
//        dd($m_);
        $row = Loan::find($id);
        // dd($row);
        return view('partials.loan_disbursement.loan_outstanding_payment', ['row' => $row]);
    }


    public function updateLoanStatus(Request $request)
    {
        $id = $request->id;
        $m = Loan::find($id);
        $m->status_note_approve = $request->status_note_approve;
        $m->status_note_date_approve = $request->status_note_date_approve;
        $m->disbursement_status = $request->disbursement_status;

        $m->save();
    }

    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\LoanOutstanding');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/loanoutstanding');
        $this->crud->setEntityNameStrings('loan activated', 'loan activated');
        $this->crud->disableResponsiveTable();
        $this->crud->setListView('partials.loan_disbursement.payment-loan');
        $this->crud->enableExportButtons();
        $this->crud->orderBy(getLoanTable().'.id','DESC');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        
        if(companyReportPart() == 'company.mkt'){
            $this->crud->addClause('where', getLoanTable().'.branch_id', session('s_branch_id'));
        }

        include('loan_inc.php');
        
        $this->crud->addClause('selectRaw', getLoanTable().'.*');
        $this->crud->enableDetailsRow();
        $this->crud->allowAccess('disburse_details_row');
        $this->crud->denyAccess(['create', 'update', 'delete', 'clone']);

        //$this->crud->addClause('where', 'disbursement_status','Activated');

        //$this->crud->addButtonFromModelFunction('line', 'addButtonCustom', 'addButtonCustom', 'beginning');
        // add asterisk for fields that are required in LoanOutstandingRequest
        $this->crud->addButtonFromModelFunction('line', 'addButtonCustomLoanOutstanding', 'addButtonCustomLoanOutstanding', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        // $this->setPermissions();

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function firstdate(){
        //dd($_GET);
        $loan_id = ($_GET['id']);
        $loan = \App\Models\Loan::where('id',$loan_id)->first();
        $disbursement = \App\Models\PaidDisbursement::where('contract_id',$loan_id)->first();
        
        //dd($disbursement->reference);
        
        // dd($journal_details);
        $first_date = $_GET['firstDate'];
        $repayment_date = $_GET['repaymentDate'];
        $deposit_date = '';
        if(isset($_GET['DepositDate'])){
            $deposit_date = $_GET['DepositDate'];
        }
        //dd($repayment_date);
        if($repayment_date != ""){
            $general_journal = \App\Models\GeneralJournal::where('tran_id',optional($disbursement)->id)
                                                    ->where('tran_type','loan-disbursement')->first();
            $journal_details = \App\Models\GeneralJournalDetail::where('journal_id',optional($general_journal)->id)->get();
            $re_date = Carbon::parse($repayment_date)->format('Y-m-d');
            $loan->status_note_date_activated = $re_date;
            $loan->save();
            // dd($loan);
            $disbursement->paid_disbursement_date = $re_date;
            $disbursement->save();
            $general_journal->date_general = $re_date;
            $general_journal->save();
            foreach($journal_details as $journal_detail){
                // dd($journal_detail->j_detail_date);
                $journal_detail->j_detail_date = $re_date;
                $journal_detail->save();
            }
            // $journal_details->save();
        }
        if($first_date != ""){
            //dd($first_date);
            $fr_date = Carbon::parse($first_date)->format('Y-m-d');
            
            $loanId = \App\Models\LoanCalculate::where('disbursement_id',$loan_id)->first();
            if(companyReportPart() == 'company.mkt'){
                $loan_payments = LoanPayment::where('disbursement_id',$loan->id)->get();

                foreach($loan_payments as $loan_payment){
                    $g_js = GeneralJournal::where('tran_type','payment')
                                        ->where('tran_id',$loan_payment->id)
                                        ->first();
                    GeneralJournalDetail::where('journal_id',$g_js->id)->delete();
                    $g_js->delete();

                }
                LoanCalculate::where('disbursement_id',$loan->id)->delete();
                LoanPayment::where('disbursement_id',$loan->id)->delete();
                PaymentHistory::where('loan_id',$loan->id)->delete();

                
                $obj=Loan::find($loan_id);
                $obj->first_installment_date = $fr_date;
                $obj->principle_repayment = 0;
                $obj->interest_repayment = 0;
                $obj->save();
            }
            
        };
        if($deposit_date != ''){
            $de_date = Carbon::parse($deposit_date)->format('Y-m-d');
            $deposit = \App\Models\LoanDeposit::where('applicant_number_id',$loan_id)->first();
            $compulsory_tran = CompulsorySavingTransaction::where('loan_id',$loan_id)->first();
            if(!isset($deposit)){
                return response('No Deposit Date', 200);
            }
            else{
            $general_journal = \App\Models\GeneralJournal::where('tran_id',optional($deposit)->id)->where('tran_type','loan-deposit')->first();
            $journal_details = \App\Models\GeneralJournalDetail::where('journal_id',optional($general_journal)->id)->get();
            //dd($journal_details);
            $deposit->loan_deposit_date = $de_date;
            $deposit->save();

            $compulsory_tran->tran_date = $de_date;
            $compulsory_tran->save();

            $general_journal->date_general = $de_date;
            $general_journal->save();

            foreach($journal_details as $journal_detail){
                // dd($journal_detail->j_detail_date);
                $journal_detail->j_detail_date = $de_date;
                $journal_detail->save();
            }
        }
        }
    }
    public function changedate(){
        $id = $_GET['id'];
        $date = $_GET['changeDate'];
        $date_s = Carbon::parse($date)->format('Y-m-d 00:00:00.0');
        //dd($date_s);
        $loan = \App\Models\LoanCalculate::where('id',$id)->first();
        if($date != ""){
            $loan->date_s = $date_s;
            $loan->save();
        }else{
            return response('Error', 200);
        }
    }
    public function clientOptions(Request $request) {

        $term = $request->input('term');
        $options = Client::where('name', 'like', '%'.$term.'%')->get()->pluck('name', 'id');
        return $options;
    }
    public function view_payment(Request $request){
        $schedule_id = $request->schedule_id;
        $payments = PaymentHistory::where('schedule_id',$schedule_id)->where('loan_id',$request->loan_id)->get();
        $all_payments = PaymentHistory::where('loan_id',$request->loan_id)->get();
        //dd($payment);
        return view('partials.loan-payment.view-repayment',['payments'=>$payments,'all_payments'=> $all_payments]);
    }

    public function cancel_activated(Request $request){


        $id = $request->id;
        $m = Loan2::find($id);
        if($m != null){
            $m->disbursement_status = 'Canceled';
            $m->cancel_date = date('Y-m-d');
            $m->remark = $request->remark;
            $m->principle_repayment = 0;
            $m->interest_repayment = 0;
            if ($m->save()){
                $paid_disbursement=PaidDisbursement::where('contract_id',$id)->first();
                if ($paid_disbursement != null){
                    GeneralJournal::where('tran_id',$paid_disbursement->id)->where('tran_type','loan-disbursement')->delete();
                    GeneralJournalDetail::where('tran_id',$paid_disbursement->id)->where('tran_type','loan-disbursement')->delete();
                    CompulsorySavingTransaction::where('tran_id',$paid_disbursement->id)->where('train_type_ref','disbursement')->delete();
                }
                PaidDisbursement::where('contract_id',$id)->delete();
                $paid_dis_deposit=LoanDeposit::where('applicant_number_id',$id)->first();
                if ($paid_dis_deposit != null){
                    $m->deposit_paid = 'No';
                    $m->save();
                    GeneralJournal::where('tran_id',$paid_dis_deposit->id)->where('tran_type','loan-deposit')->delete();
                    GeneralJournalDetail::where('tran_id',$paid_dis_deposit->id)->where('tran_type','loan-deposit')->delete();
                    CompulsorySavingTransaction::where('tran_id',$paid_dis_deposit->id)->where('train_type','deposit')->delete();
                }
                LoanDeposit::where('applicant_number_id',$id)->delete();
                $loan_payment=LoanPayment::where('disbursement_id',$id)->get();
                foreach ($loan_payment as $p){
                    GeneralJournal::where('tran_id',$p->id)->where('tran_type','payment')->delete();
                    GeneralJournalDetail::where('tran_id',$p->id)->where('tran_type','payment')->delete();
                    CompulsorySavingTransaction::where('tran_id',$p->id)->where('train_type_ref','saving')->delete();
                }
                LoanPayment::where('disbursement_id',$id)->delete();

                $payment_histories = PaymentHistory::where('loan_id', $id)->get();
                foreach($payment_histories as $payment_history){
                    $payment_history->delete();
                }


                LoanCalculate::where('disbursement_id',$id)
                    ->update([
                       'principal_p' => 0,
                        'interest_p' => 0,
                        'penalty_p' => 0,
                        'service_charge_p' => 0,
                        'total_p' => 0,
                        'balance_p' => 0,
                        'owed_balance_p' => 0,
                        'payment_status' => 'pending',
                        'over_days_p' => 0,
                        'principle_pd' => 0,
                        'interest_pd' => 0,
                        'total_pd' => 0,
                        'penalty_pd' => 0,
                        'payment_pd' => 0,
                        'service_pd' => 0,
                        'compulsory_pd' => 0,
                        'compulsory_p' => 0,
                        'count_payment' => 0,
                        'exact_interest' => 0,
                        'charge_schedule' => 0,
                        'compulsory_schedule' => 0,
                        'total_schedule' => 0,
                        'balance_schedule' => 0,
                        'penalty_schedule' => 0,
                    ]);
            }
        }


         return redirect()->back();

    }

    public function rollbackCompleted(Request $request){
        $id = $request->id;
        $m = Loan2::find($id);

        if($m != null){
            $m->disbursement_status = "Activated";
            // $m->remark = $request->remark;
            if ($m->save()){
                    $payment = LoanPayment::where('disbursement_id', $id)->orderBy('payment_date', 'desc')->first();
                    GeneralJournal::where('reference_no', $payment->payment_number)->where('tran_type', 'payment')->delete();
                    $histories = PaymentHistory::where('payment_id', $payment->id)->get();
                    
                    foreach($histories as $history){
                        LoanCalculate::where('id', $history->schedule_id)->update([
                            'principal_p' => 0,
                            'interest_p' => 0,
                            'penalty_p' => 0,
                            'service_charge_p' => 0,
                            'total_p' => 0,
                            'balance_p' => 0,
                            'owed_balance_p' => 0,
                            'payment_status' => 'pending',
                            'over_days_p' => 0,
                            'principle_pd' => 0,
                            'interest_pd' => 0,
                            'total_pd' => 0,
                            'penalty_pd' => 0,
                            'payment_pd' => 0,
                            'service_pd' => 0,
                            'compulsory_pd' => 0,
                            'compulsory_p' => 0,
                            'count_payment' => 0,
                            'exact_interest' => 0,
                            'charge_schedule' => 0,
                            'compulsory_schedule' => 0,
                            'total_schedule' => 0,
                            'balance_schedule' => 0,
                            'penalty_schedule' => 0,
                        ]);


                    }
                    $payment->delete();
                    CompulsorySavingTransaction::where('tran_id', $payment->id)->where('train_type_ref','saving')->delete();
            }
        }
        return redirect()->back();
    }

}
