<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportMkt;
use App\Imports\ImportloanRepayment;
use App\Models\LoanCalculate;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use File;
/**
 * Class ImportLoanRepaymentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ImportForMkt extends CrudController
{

    public function index()
    {
        return redirect('admin/import-for-mkt/create'); 
    }
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ImportLoanRepayment');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/import-for-mkt');
        $this->crud->setEntityNameStrings('Import For MKT', 'Import For MKT');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addField([
            'name' => 'export-import',
            'type' => 'view',
            'view' => 'partials/payment/export-for-mkt',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-10'
            ],
        ]);

    }

    public function store(Request $request)
    {
        // dd($request);
        // dd("wrong pee youte nay tal");
        if ($request->hasFile('excel_file')){
            $excel = $request->file('excel_file');
          
            $arrays = Excel::toArray(new ImportloanRepayment, $excel);
            $redirect_location = parent::storeCrud($request);

            $extension = $excel->getClientOriginalExtension();
            $filename = now().'.'.$extension;

            $id = $this->crud->entry->id;
            $loan_id = \App\Models\LoanPayment::find($id);
            $loan_id->excel = $filename;
            $loan_id->save();

            \Storage::disk('local_public')->putFileAs(
                'excel/loan_payments/', $excel, now().'.'. $extension
          );

            // dd($arrays);
            foreach($arrays as $row){
                
                if($row != null){
                    //$arr = [];
                    $client_id= isset($row['client_id'])?trim($row['client_id']):null;
                    $you_are_a_group_leader= isset($row['you_are_a_group_leader'])?trim($row['you_are_a_group_leader']):null;
                    $you_are_a_center_leader= isset($row['you_are_a_center_leader'])?trim($row['you_are_a_center_leader']):null;
                    $guarantor_name_myanmar= isset($row['guarantor_name_myanmar'])?trim($row['guarantor_name_myanmar']):null;
                    $loan_number= isset($row['loan_number'])?trim($row['loan_number']):null;
                    $branch_code = isset($row['branch_code'])?trim($row['branch_code']):null;
                    $center_code= isset($row['center_code'])?trim($row['center_code']):null;
                    $loan_officer_code= isset($row['loan_officer_code'])?trim($row['loan_officer_code']):null;
                    $loan_product_code= isset($row['loan_product_code'])?trim($row['loan_product_code']):null;
                    $principle_repay= isset($row['principle_repay'])?trim($row['principle_repay']):null;
                    $interest_repay= isset($row['interest_repay'])?trim($row['interest_repay']):null;
        
                    $loan_application_date=date('Y-m-d');
                    $first_installment_date=date('Y-m-d');
        
                    if($row['loan_application_date']>0) {
                        $UNIX_DATE = ($row['loan_application_date'] - 25569) * 86400;
                        $loan_application_date = gmdate("Y-m-d", $UNIX_DATE);
                    }
        
        
                    if($row['first_installment_date']>0) {
                        $UNIX_DATE2 = ($row['first_installment_date'] - 25569) * 86400;
                        $first_installment_date = gmdate("Y-m-d", $UNIX_DATE2);
                    }
        
        
                    $loan_amount= isset($row['loan_amount'])?trim($row['loan_amount']):null;
                    $interest_rate= isset($row['interest_rate'])?trim($row['interest_rate']):null;
                    $interest_rate_period= isset($row['interest_rate_period'])?trim($row['interest_rate_period']):null;
                    $loan_term= isset($row['loan_term'])?trim($row['loan_term']):'Day';
                    $loan_term_value= isset($row['loan_term_value'])?trim($row['loan_term_value']):null;
                    $repayment_term= isset($row['repayment_term'])?trim($row['repayment_term']):null;
                    $currency= isset($row['currency'])?trim($row['currency']):null;
                    $transaction_type= isset($row['transaction_type'])?trim($row['transaction_type']):null;
                    $group_loan_id= isset($row['group_loan_id'])?trim($row['group_loan_id']):null;
        
                    $c_id= ClientR::where('client_number', $client_id)->first();
                    $g_id= Guarantor::where('full_name_mm', $guarantor_name_myanmar)->first();
                    $b = Branch::where('code', $branch_code)->first();
                    $center_leader= CenterLeader::where('code', $center_code)->first();
                    $loan_officer= User::where('user_code',$loan_officer_code)->first();
                    $l_product= LoanProduct::where('code', $loan_product_code)->first();
                    $cur_id= Currency::where('currency_name', $currency)->first();
                    $t_type= TransactionType::where('title', $transaction_type)->first();
                    $g_loan= GroupLoan::where('group_code', $group_loan_id)->first();
                    $remain_p = $loan_amount - $principle_repay;
                    $remain_i = $loan_amount - $interest_repay;

                    
        
                        $l = Loan2::where('disbursement_number', $loan_number)
                            ->first();
        
                        if ($l != null){
                            $l->client_id= optional($c_id)->id;
                            $l->you_are_a_group_leader= $you_are_a_group_leader;
                            $l->you_are_a_center_leader= $you_are_a_center_leader;
                            $l->guarantor_id= optional($g_id)->id;
                            $l->branch_id= optional($b)->id;
                            $l->center_leader_id= optional($center_leader)->id;
                            $l->loan_officer_id= optional($loan_officer)->id;
                            $l->loan_production_id= optional($l_product)->id;
                            $l->loan_application_date= $loan_application_date;
                            $l->first_installment_date= $first_installment_date;
                            $l->loan_amount= $loan_amount;
                            $l->interest_rate= $interest_rate;
                            $l->interest_rate_period= $interest_rate_period;
                            $l->loan_term= $loan_term;
                            $l->disbursement_status = 'Activated';
                            $l->loan_term_value= $loan_term_value;
                            $l->repayment_term= $repayment_term;
                            $l->currency_id= optional($cur_id)->id;
                            $l->transaction_type_id= optional($t_type)->id;
                            $l->group_loan_id= optional($g_loan)->id;
                        }
                        else{
        
                            $l=new Loan2();
                            $l->disbursement_number= $loan_number;
                            $l->client_id= optional($c_id)->id;
                            $l->you_are_a_group_leader= $you_are_a_group_leader;
                            $l->you_are_a_center_leader= $you_are_a_center_leader;
                            $l->guarantor_id= optional($g_id)->id;
                            $l->branch_id= optional($b)->id;
                            $l->center_leader_id= optional($center_leader)->id;
                            $l->loan_officer_id= optional($loan_officer)->id;
                            $l->loan_production_id= optional($l_product)->id;
                            $l->loan_application_date= $loan_application_date;
                            $l->first_installment_date= $first_installment_date;
                            $l->loan_amount= $loan_amount;
                            $l->interest_rate= $interest_rate;
                            $l->interest_rate_period= $interest_rate_period;
                            $l->loan_term= $loan_term;
                            $l->disbursement_status = 'Activated';
                            $l->loan_term_value= $loan_term_value;
                            $l->repayment_term= $repayment_term;
                            $l->currency_id= optional($cur_id)->id;
                            $l->transaction_type_id= optional($t_type)->id;
                            $l->group_loan_id= optional($g_loan)->id;
                        }
        
                        if ($l->save()){
                            $compulsory_id= optional($l_product)->compulsory_id;
                            $compulsory_product= CompulsoryProduct::where('id', $compulsory_id)->first();
                            $loan_compulsory= LoanCompulsory::where('loan_id', $l->id)->first();
        
                            if ($loan_compulsory != null){
                                $loan_compulsory->client_id = optional($c_id)->id;
                                $loan_compulsory->compulsory_id = $compulsory_id;
                                $loan_compulsory->product_name = optional($compulsory_product)->product_name;
                                $loan_compulsory->saving_amount = optional($compulsory_product)->saving_amount;
                                $loan_compulsory->charge_option = optional($compulsory_product)->charge_option;
                                $loan_compulsory->interest_rate = optional($compulsory_product)->interest_rate;
                                $loan_compulsory->compound_interest = optional($compulsory_product)->compound_interest;
                                $loan_compulsory->override_cycle = optional($compulsory_product)->override_cycle;
                                $loan_compulsory->compulsory_product_type_id =  optional($compulsory_product)->compulsory_product_type_id;
                                $loan_compulsory->branch_id = optional($b)->id;
        
                            }else{
                                $savong_no = LoanCompulsory::getSeqRef('compulsory');
        
                                $loan_compulsory=new LoanCompulsory();
                                $loan_compulsory->loan_id = $l->id;
                                $loan_compulsory->compulsory_number = $savong_no;
                                $loan_compulsory->client_id = optional($c_id)->id;
                                $loan_compulsory->compulsory_id = $compulsory_id;
                                $loan_compulsory->product_name = optional($compulsory_product)->product_name;
                                $loan_compulsory->saving_amount = optional($compulsory_product)->saving_amount;
                                $loan_compulsory->charge_option = optional($compulsory_product)->charge_option;
                                $loan_compulsory->interest_rate = optional($compulsory_product)->interest_rate;
                                $loan_compulsory->compound_interest = optional($compulsory_product)->compound_interest;
                                $loan_compulsory->override_cycle = optional($compulsory_product)->override_cycle;
                                $loan_compulsory->compulsory_product_type_id =  optional($compulsory_product)->compulsory_product_type_id;
                                $loan_compulsory->branch_id = optional($b)->id;
        
                            }
        
        
                            if ($loan_compulsory->save()){
                                $loan_charge_product= ChargeLoanProduct::where('loan_product_id', optional($l_product)->id)->get();
        
                                if ($loan_charge_product != null){
                                    foreach ($loan_charge_product as $l_c){
                                        $charge= Charge::find($l_c->charge_id);
        
        
                                        if ($charge != null){
                                            $loan_charge= LoanCharge::where('loan_id', $l->id)
                                                ->where('charge_id', optional($charge)->id)
                                                ->first();
        
                                            if ($loan_charge != null){
                                                $loan_charge->name = optional($charge)->name;
                                                $loan_charge->amount =optional($charge)->amount;
                                                $loan_charge->charge_option = optional($charge)->charge_option;
                                                $loan_charge->charge_type = optional($charge)->charge_type;
                                                $loan_charge->status = optional($charge)->status;
                                            }else{
                                                $loan_charge= new LoanCharge();
                                                $loan_charge->loan_id = $l->id;
                                                $loan_charge->charge_id = optional($charge)->id;
                                                $loan_charge->name = optional($charge)->name;
                                                $loan_charge->amount =optional($charge)->amount;
                                                $loan_charge->charge_option = optional($charge)->charge_option;
                                                $loan_charge->charge_type = optional($charge)->charge_type;
                                                $loan_charge->status = optional($charge)->status;
                                            }
        
                                            $loan_charge->save();
        
                                        }
        
                                    }
        
        
                                }
                            }
                        }

                        $d_cal = new LoanCalculate;
                        $d_cal->no = 1;
                        $d_cal->day_num = 0;
                        $d_cal->disbursement_id = $l->id;
                        $d_cal->date_s = date('Y-m-d');
                        $d_cal->principal_s = $principle_repay;
                        $d_cal->principal_p = $principle_repay;
                        $d_cal->principle_pd = $principle_repay;
                        $d_cal->interest_s = $interest_repay;
                        $d_cal->interest_p = $interest_repay;
                        $d_cal->interest_pd = $interest_repay;
                        $d_cal->penalty_s = 0;
                        $d_cal->service_charge_s = 0;
                        $d_cal->total_s = $principle_repay + $interest_repay;
                        $d_cal->total_pd = $principle_repay + $interest_repay;
                        $d_cal->payment_status = 'paid';
                        $d_cal->balance_s = $principle_repay + $interest_repay;
                        $d_cal->branch_id = $l->branch_id;
                        $d_cal->group_id = $l->group_loan_id;
                        $d_cal->center_id = $l->center_leader_id;
                        $d_cal->loan_product_id = $l->loan_production_id;
                        $d_cal->save();

                        $d_cal = new LoanCalculate;
                        $d_cal->no = 2;
                        $d_cal->day_num = 0;
                        $d_cal->disbursement_id = $l->id;
                        $d_cal->date_s = date('Y-m-d');
                        $d_cal->principal_s = $remain_p;
                        $d_cal->interest_s = $remain_i;
                        $d_cal->penalty_s = 0;
                        $d_cal->service_charge_s = 0;
                        $d_cal->total_s = $remain_p + $remain_i;
                        $d_cal->payment_status = 'pending';
                        $d_cal->balance_s = $remain_p + $remain_i;
                        $d_cal->branch_id = $l->branch_id;
                        $d_cal->group_id = $l->group_loan_id;
                        $d_cal->center_id = $l->center_leader_id;
                        $d_cal->loan_product_id = $l->loan_production_id;
                        $d_cal->save();
                    
                }
            }
            Session::flash('message','Successfully Imported');
        }
        return redirect()->back();
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function download_excel()
    {
        // dd("hey");
        return Excel::download(new ExportMkt(),'import-custom-loan-mkt-'.date('Y-m-d H:s').".xlsx",\Maatwebsite\Excel\Excel::XLSX);
    }
    public function excel_download($id)
    {
        //dd("hello");
        $loan_id = \App\Models\LoanPayment::find($id);
        $excel_file = $loan_id->excel;
        //$disk = "local_public";
        $file= public_path(). "/excel/loan_payments/".$excel_file;

        // $headers = array(
        //       'Content-Type: application/pdf',
        //     );

        return response()->download($file);
    }
}
