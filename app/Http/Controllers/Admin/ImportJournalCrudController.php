<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportJournal;
use App\Models\GeneralJournalDetail;
use App\Models\GeneralJournal;
use App\Models\ImportJournal;
use App\Models\UserU;
use App\Models\Branch;
use App\Models\AccountChart;
use App\Models\GeneralJournalImport;
use App\Models\GeneralJournalDetailImport;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use DateTime;
use App\User;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ImportJournalRequest as StoreRequest;
use App\Http\Requests\ImportJournalRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\File;
use Maatwebsite\Excel\Facades\Excel;


/**
 * Class ImportJournalCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ImportJournalCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\GeneralJournalImport');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/import-journal');
        $this->crud->setEntityNameStrings('Import Journal', 'Import Journals');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        if(companyReportPart() == 'company.mkt'){
            $this->crud->addFilter([ // daterange filter
                'type' => 'date_range',
                'name' => 'from_to',
                'label'=> _t('Date range')
            ],
                false,
                function($value) { // if the filter is active, apply these constraints
                    $dates = json_decode($value);
                    $this->crud->addClause('where', 'date_general', '>=', $dates->from);
                    $this->crud->addClause('where', 'date_general', '<=', $dates->to . ' 23:59:59');
            });
        }
        $this->crud->addColumn([
            'name' => 'date_general',
            'label' => _t('Date'),
            'type' => 'date'
        ]);
        $this->crud->addColumn([
            'name' => 'reference_no',
            'label' => _t('Reference No'),
        ]);
        $this->crud->addColumn([
            'name' => 'amount',
            'label' => 'Amount',
            'type' => 'closure',
            'function' => function($entry) {
                $g_detail_dr = GeneralJournalDetail::where('journal_id',$entry->id)->sum('dr');
                if($g_detail_dr == "0"){
                    $g_detail_cr = GeneralJournalDetail::where('journal_id',$entry->id)->sum('cr');
                    return $g_detail_cr; 
                }
                else{
                    return $g_detail_dr;
                }
                
            }
        ]);



        $tran_type = array(
            'journal'=>'Journal',
            'expense'=>'Expense',
        );

        $this->crud->addField([
            'name' => 'tran_type',
            'label' => _t('Tran Type'),
            'type' => 'select_from_array',
            'options' => $tran_type,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
            //'tab' => _t('personal_detail'),
        ]);


        $this->crud->addField([
            'name' => 'export-import',
            'type' => 'view',
            'view' => 'partials/journal/script-export-import',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-10'
            ],
        ]);


        // add asterisk for fields that are required in ImportJournalRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
         // $this->setPermissions();
        $this->crud->enableExportButtons();

    }

    public function setPermissions()
    {
        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);

        $fname = 'journal-expense-import';
        if (_can2($this,'list-'.$fname)) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if (_can2($this,'create-'.$fname)) {
            $this->crud->allowAccess('create');
        }

//        // Allow update access
//        if (_can2($this,'update-'.$fname)) {
//            $this->crud->allowAccess('update');
//        }

        // Allow delete access
        if (_can2($this,'delete-'.$fname)) {
            $this->crud->allowAccess('delete');
        }




    }

    public function store(StoreRequest $request)
    {

        if ($request->hasFile('open_detail_file')){
            $excel = $request->file('open_detail_file');
            $redirect_location = parent::storeCrud($request);
            
            $extension = $excel->getClientOriginalExtension();
            $filename =now().'.'.$extension;

            $id = $this->crud->entry->id;
            $general_journal_id = \App\Models\GeneralJournal::find($id);
            $general_journal_id->excel = $filename;
            $general_journal_id->save();

            \Storage::disk('local_public')->putFileAs(
                'excel/general_journals/', $excel, now().'.'. $extension
          );
            // Excel::import(new ImportJournal($request->tran_type), $request->file('open_detail_file'));
            $arrays = Excel::toArray(new ImportJournal($request->tran_type), $excel);
            // dd($arrays);
            foreach($arrays[0] as $row){
                if ($row != null) {
                    //$arr = [];
        
                    $branch_code = trim($row['branch_code']);
                    $reference_no = isset($row['reference_no']) ? $row['reference_no'] : 0;
                    $coa_debit_code = isset($row['coa_debit']) ? trim($row['coa_debit']) : '';
                    $coa_credit_code = isset($row['coa_credit']) ? trim($row['coa_credit']) : '';
                    $amount_debit = isset($row['amount_debit']) ? $row['amount_debit'] : 0;
                    $amount_credit = isset($row['amount_credit']) ? $row['amount_credit'] : 0;
                    $date = isset($row['date']) ? $row['date'] : date('Y-m-d');
                    $note = $row['descriptions'];
                    $staff_id = $row['staff_id'];
                    $staff_name = $row['staff_name'];
                    $tran_type = $request->tran_type;
                    //dd($tran_type);
                    //dd($branch_code);
                    //$user_id =0;
        
                    //dd($amount_credit);
        
        
                    $m = GeneralJournal::where('reference_no', trim($row['reference_no']))->where('tran_type', $tran_type)->first();
                    $u = UserU::where('user_code', trim($row['staff_id']))->first();
                    $b = Branch::where('code', trim($row['branch_code']))->first();
        
                    ///dd($row['reference_no']);
                    $coa_credit = AccountChart::where('code', $coa_credit_code)->first();
                    $coa_debit = AccountChart::where('code', $coa_debit_code)->first();
                    $credit_section_id = optional($coa_credit)->section_id;
                    $debit_section_id = optional($coa_debit)->section_id;
        
                    $credit_acc_id = optional($coa_credit)->id;
                    $debit_acc_id = optional($coa_debit)->id;
        
                    if ($u != null) {
                        $user_id = $u->id;
                    } else {
                        $user = new  UserU();
                        $user->name = $staff_name;
                        $user->user_code = $staff_id;
                        $user->password = bcrypt('123456789');
                        if ($user->save()) {
                            $user_id = $user->id;
                        };
                    }
                    //dd($m);
                    if ($m != null) {
        
                        $g_d = new GeneralJournalDetailImport();
                        //$g_d->name='journal';
                        $g_d->section_id = $amount_credit > 0 ? $credit_section_id : $debit_section_id;
                        $g_d->journal_id = $m->id;
                        $g_d->currency_id = 0;
                        $g_d->acc_chart_id = $amount_credit > 0 ? $credit_acc_id : $debit_acc_id;
                        $g_d->dr = $amount_debit;
                        $g_d->cr = $amount_credit;
                        $g_d->j_detail_date = $m->date_general;
                        $g_d->description = $note;
                        $g_d->tran_type = $tran_type;
                        $g_d->created_by = $user_id;
                        $g_d->updated_by = $user_id;
                        $g_d->branch_id = $b->id ?? 0;
                        $g_d->external_acc_id = $amount_credit > 0 ? $credit_acc_id : $debit_acc_id;
                        $g_d->acc_chart_code = $amount_credit > 0 ? $coa_credit_code : $coa_debit_code;
                        $g_d->external_acc_chart_id = $amount_credit > 0 ? $credit_acc_id : $debit_acc_id;
                        $g_d->external_acc_chart_code = $amount_credit > 0 ? $coa_credit_code : $coa_debit_code;
        
                        $g_d->save();
                    } else {
                        //dd($date);
        
                        $UNIX_DATE = ($date - 25569) * 86400;
                        $datetodb = gmdate("Y-m-d", $UNIX_DATE);
        
                        $g = new GeneralJournalImport();
                        //dd($datetodb);
                        $g->date_general = $datetodb;
                        $g->tran_type = $tran_type;
                        $g->reference_no = $reference_no;
                        $g->note = $note;
                        $g->created_by = $user_id;
                        $g->updated_by = $user_id;
        
        
                        if ($g->save()) {
                            $g_d = new GeneralJournalDetail();
                            //$g_d->name='journal';
                            $g_d->section_id = $amount_credit > 0 ? $credit_section_id : $debit_section_id;
                            $g_d->journal_id = $g->id;
                            $g_d->currency_id = 0;
                            $g_d->acc_chart_id = $amount_credit > 0 ? $credit_acc_id : $debit_acc_id;
                            $g_d->dr = $amount_debit;
                            $g_d->cr = $amount_credit;
                            $g_d->j_detail_date = $g->date_general;
                            $g_d->description = $note;
                            $g_d->tran_type = $tran_type;
                            $g_d->created_by = $user_id;
                            $g_d->updated_by = $user_id;
                            $g_d->branch_id = $b->id ?? 0;
                            $g_d->external_acc_id = $amount_credit > 0 ? $credit_acc_id : $debit_acc_id;
                            $g_d->acc_chart_code = $amount_credit > 0 ? $coa_credit_code : $coa_debit_code;
                            $g_d->external_acc_chart_id = $amount_credit > 0 ? $credit_acc_id : $debit_acc_id;
                            $g_d->external_acc_chart_code = $amount_credit > 0 ? $coa_credit_code : $coa_debit_code;
        
        
                            $g_d->save();
        
        
                        }
                    }
        
        
                }
            }
        }
        
        return redirect()->back();


    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }


    public function download_excel()
    {

        return Excel::download(new ExportJournal(),'import-journal-expense-'.date('Y-m-d H:s').".xlsx",\Maatwebsite\Excel\Excel::XLSX);
    }
    public function excel_download($id)
    {
        $loan_id = \App\Models\GeneralJournal::find($id);
        $excel_file = $loan_id->excel;
        //$disk = "local_public";
        $file= public_path(). "/excel/general_journals/".$excel_file;

        // $headers = array(
        //       'Content-Type: application/pdf',
        //     );

        return response()->download($file);
    }
}
