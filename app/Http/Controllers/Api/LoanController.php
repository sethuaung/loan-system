<?php

namespace App\Http\Controllers\Api;

use App\Models\Loan;
use App\Models\LoanCalculate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanController extends Controller
{
    public function index(Request $request)
    {
        // dd($request);
        $search_term = $request->input('q');
        $page = $request->input('page');
        //dd($search_term);
        if ($search_term)
        {
            $results = Loan::where('disbursement_number', 'LIKE', '%'.$search_term.'%')
                ->where('disbursement_status','Approved')
                // ->where(function ($q){
                //     return $q->where('group_loan_id','=',0)->orWhereNull('group_loan_id');
                // })
                 ->paginate(100);
        }
        else
        {
            $results = Loan::where('disbursement_status','Approved')
                            // ->where(function ($q){
                            //     return $q->where('group_loan_id','=',0)->orWhereNull('group_loan_id');

                            // })
                            ->paginate(100);
        }

        return $results;
    }

    public function show($id)
    {
        return Loan::find($id);
    }

    public function overDue(request $request){
        // dd($request);
        $loan = Loan::find($request->loan_number);
        if($loan){
            $schedule = LoanCalculate::where('disbursement_id',$loan->id)->where('payment_status','pending')->first();
            $payment_date = new \DateTime($request->date);
            $schedule_date = new \DateTime($schedule->date_s);
            if($payment_date > $schedule_date){
                $diffDate = $payment_date->diff($schedule_date);
                $days = $diffDate->format('%a');
                return $days;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
        
        
    }
}
