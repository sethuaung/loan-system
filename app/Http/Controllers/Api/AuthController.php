<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ACC;
use App\Helpers\MFS;
use App\User;
use App\Models\Loan;
use App\Models\Loan2;
use App\Models\Client;
use App\Models\AccountChart;
use App\Models\LoanDepositU;
use App\Models\LoanDeposit;
use App\Models\LoanPayment;
use App\Models\LoanPaymentTem;
use App\Models\GeneralJournal;
use App\Models\GeneralJournalDetail;
use App\Models\GeneralJournalDetailTem;
use App\Models\LoanProduct;
use App\Models\ScheduleBackup;
use App\Models\LoanCalculate;
use App\Models\PaidDisbursement;
use App\Models\LoanCycle;
use Hash;
use App\Models\PaymentHistory;
use App\Models\LoanCompulsory;
use Auth;
use DB;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
       $fields = $request->validate([
        'username' => 'required',
        'password' => 'required',
       ]);
       $check_user = User::where('name',$fields['username'])->first();
       if($check_user){

        $token = Str::random(60);
        $check_user->remember_token = $token;
        $check_user->save();

        if(Hash::check($fields['password'], $check_user->password)){
            $data[] = [
                'username' => $check_user->name,
                'token' => $check_user->remember_token,
            ];
            return response()->json(['status_code' => 200, 'message' => 'Log In Success', 'data' => $data]);
        }
        else{
            return response()->json(['status_code' => 400, 'message' => 'Username and Password dont match', 'data' => Null]);
        }
       }    
       else{
        return response()->json(['status_code' => 400, 'message' => 'User not found', 'data' => Null]);
       }
    }

    public function getLoans(request $request){
        $fields = $request->validate([
            'branch_id' => 'required',
           ]);
        $loans = Loan::orderbydesc('id')->get();
        return response()->json(['status_code' => 200, 'message' => 'Loans Fetch Success', 'data' => $loans]);
    }

    public function getApprovedLoans(request $request){
        $fields = $request->validate([
            'branch_id' => 'required',
           ]);
        $loans = Loan::where('disbursement_status','Approved')->get();
        return response()->json(['status_code' => 200, 'message' => 'Approved Loans Fetch Success', 'data' => $loans]);
    }

    public function createLoan(request $request){
        $fields = $request->validate([
            'branch_id' => 'required',
            'client_id' => 'required',
            'you_are_a_group_leader' => 'required',
            'you_are_a_center_leader' => 'required',
            'disbursement_number' => 'required',
            'loan_officer_id' => 'required',
            'loan_production_id' => 'required',
            'loan_application_date' => 'required',
            'first_installment_date' => 'required',
            'loan_amount' => 'required',
            'currency_id' => 'required',
            'transaction_type_id' => 'required',
            'group_loan_id' => 'required',
            'compulsory_id' => 'required',
            'compound_interest' => 'required',
            'override_cycle' => 'required',
            'product_name' => 'required',
            'saving_amount' => 'required',
            'c_charge_option' => 'required',
            'c_interest_rate' => 'required',
            'compulsory_product_type_id' => 'required',
        ]);
        $loan_product = \App\Models\LoanProduct::find($request->loan_production_id);
        if(!$loan_product){
            return response()->json(['status_code' => 400, 'message' => 'Wrong Loan Product ID', 'data' => $request->loan_production_id]);
        }
        $charges = optional($loan_product)->loan_products_charge;
        
        foreach($charges as $c){
            $rand_id = rand(1,1000).time().rand(1,1000);
            $charge_id[$rand_id] = $c->id;
            $loan_charge_id[$rand_id] = Null;
            $name[$rand_id] = $c->name;
            $amount[$rand_id] = $c->amount;
            $charge_option[$rand_id] = $c->charge_option;
            $charge_type[$rand_id] = $c->charge_type;
            $status[$rand_id] = $c->status;
        }
        $request->request->add([
            'charge_id' => $charge_id,
            'loan_charge_id' => $loan_charge_id,
            'name' => $name,
            'amount' => $amount,
            'charge_option' => $charge_option,
            'charge_type' => $charge_type,
            'status' => $status,
            'interest_rate' => $loan_product->interest_rate_default,
            'interest_rate_period' => $loan_product->interest_rate_period,
            'loan_term' => $loan_product->loan_term,
            'loan_term_value' => $loan_product->loan_term_value,
            'repayment_term' => $loan_product->repayment_term,
        ]);
        // dd($request);
        if($loan_product->join_group == "Yes" && $request->group_loan_id == 0){
            return response()->json(['status_code' => 400, 'message' => 'Please Fill Group Loan ID', 'data' => Null]);
        }
        $checkClient = Client::find($request->client_id);
        if(!$checkClient){
            return response()->json(['status_code' => 400, 'message' => 'Wrong Client ID', 'data' => $request->client_id]);
        }
        $loan = new Loan;
        $loan->disbursement_number = $request->disbursement_number;
        $loan->branch_id = $request->branch_id;
        $loan->center_leader_id = $request->center_leader_id;
        $loan->loan_officer_id = $request->loan_officer_id;
        $loan->transaction_type_id = $request->transaction_type_id;
        $loan->currency_id = $request->currency_id;
        $loan->client_id = $request->client_id;
        $loan->loan_application_date = date('Y-m-d',strtotime($request->loan_application_date));
        $loan->first_installment_date = date('Y-m-d',strtotime($request->first_installment_date));
        $loan->loan_production_id = $request->loan_production_id;
        $loan->loan_amount = $request->loan_amount;
        $loan->principle_receivable = $request->loan_amount;
        $loan->interest_receivable = 0;
        $loan->loan_term_value = $request->loan_term_value;
        $loan->loan_term = $request->loan_term;
        $loan->repayment_term = $request->repayment_term;
        $loan->interest_rate_period = $request->interest_rate_period;
        $loan->interest_rate = $request->interest_rate;
        $loan->guarantor_id = $request->guarantor_id;
        $loan->guarantor2_id = $request->guarantor2_id;
        $loan->guarantor3_id = $request->guarantor3_id;
        $loan->disbursement_status = 'Pending';
        $loan->group_loan_id = $request->group_loan_id;
        $loan->you_are_a_center_leader = $request->you_are_a_center_leader;
        $loan->you_are_a_group_leader = $request->you_are_a_group_leader;
        $loan->deposit_paid = 'No';
        $loan->created_by = $request->created_by;
        $loan->business_proposal = $request->business_proposal;
        $loan->save();
        Loan::saveDetail($request,$loan);
        return response()->json(['status_code' => 200, 'message' => 'Loan Create Success', 'data' => $loan]);
    }

    public function getDeposits(request $request){
        $deposits = LoanDepositU::orderbydesc('id')->get();
        return response()->json(['status_code' => 200, 'message' => 'Deposits Fetch Success', 'data' => $deposits]);
    }

    public function createDeposit(request $request){
        $fields = $request->validate([
            'applicant_number_id' => 'required',
            'loan_deposit_date' => 'required',
            'referent_no' => 'required',
            'customer_name' => 'required',
            'nrc' => 'required',
            'client_id' => 'required',
            'invoice_no' => 'required',
            'compulsory_saving_amount' => 'required',
            'cash_acc_id' => 'required',
            'total_deposit' => 'required',
            'client_pay' => 'required',
            'branch_id' => 'required',
        ]);
        $loan_id = $request->applicant_number_id;
        $loan_disbursement = Loan2::find($loan_id);

        $deposit = new LoanDepositU;
        $deposit->applicant_number_id = $request->applicant_number_id;
        $deposit->cash_acc_id = $request->cash_acc_id;
        $deposit->client_id = $request->client_id;
        $deposit->loan_deposit_date = $request->loan_deposit_date;
        $deposit->referent_no = $request->referent_no;
        $deposit->customer_name = $request->customer_name;
        $deposit->nrc = $request->nrc;
        $deposit->invoice_no = $request->invoice_no;
        $deposit->compulsory_saving_amount = $request->compulsory_saving_amount;
        $deposit->total_deposit = $request->total_deposit;
        $deposit->client_pay = $request->client_pay;
        $deposit->note = $request->note;
        $deposit->save();

        if ($loan_id != null){
            $loan_disbursement = Loan2::find($loan_id);
            $loan_disbursement->deposit_paid = 'Yes';
            $loan_disbursement->save();
        }

        $loan_deposit_id = $deposit->id;
        $service_charge= $request->service_amount;
        $service_charge_id= $request->service_charge_id;
        $charge_id = $request->charge_id;

        if($service_charge != null){
            foreach ($service_charge as $ke => $va){
                $deposit = new DepositServiceCharge();
                $deposit->loan_deposit_id = $loan_deposit_id;
                $deposit->service_charge_amount = isset($service_charge[$ke])?$service_charge[$ke]:0;
                $deposit->service_charge_id = isset($service_charge_id[$ke])?$service_charge_id[$ke]:0;
                $deposit->charge_id = isset($charge_id[$ke]) ? $charge_id[$ke] : 0;
                $deposit->save();

            }
        }

        LoanDeposit::savingTransction($deposit);
        LoanDeposit::accDepositTransaction($deposit);
        $acc = AccountChart::find($deposit->cash_acc_id);

        $depo = LoanDeposit::find($deposit->id);
        $depo->total_service_charge = $request->total_deposit - $request->compulsory_saving_amount;
        $depo->acc_code = optional($acc)->code;
        $depo->save();

        return response()->json(['status_code' => 200, 'message' => 'Deposit Create Success', 'data' => $depo]);

    }

    public function getRepayments(request $request){
        $fields = $request->validate([
            'branch_id' => 'required',
           ]);
        $repayments = DB::table('loan_payments')
                    ->join(getLoanTable(), 'loan_payments.disbursement_id', '=', getLoanTable().'.id')
                    ->where(getLoanTable().'.branch_id',$request->branch_id)
                    ->get();
        return response()->json(['status_code' => 200, 'message' => 'Repayments Fetch Success', 'data' => $repayments]);
    }

    public function createRepayment(request $request){
        $fields = $request->validate([
            'disbursement_id' => 'required',
            'payment_number' => 'required',
            'client_number' => 'required',
            'client_name' => 'required',
            'client_id' => 'required',
            'receipt_no' => 'required',
            'over_days' => 'required',
            'penalty_amount' => 'required',
            'principle' => 'required',
            'interest' => 'required',
            'compulsory_saving' => 'required',
            'other_payment' => 'required',
            'total_payment' => 'required',
            'payment' => 'required',
            'payment_date' => 'required',
            'owed_balance' => 'required',
            'principle_balance' => 'required',
            'payment_method' => 'required',
            'cash_acc_id' => 'required',
            'disbursement_detail_id' => 'required',
           ]);

           $repayment = LoanPayment::create($fields);

           $_param_check = str_replace('"','',$repayment->disbursement_detail_id);
           $arr_check = explode('x',$_param_check);
           $l_s_check = LoanCalculate::find($arr_check['0']);
           $pre_rp = isset($_REQUEST['pre_repayment'])?$_REQUEST['pre_repayment']:0;
           $late_rp = isset($_REQUEST['late_repayment'])?$_REQUEST['late_repayment']:0;
           if ( $l_s_check->payment_status != 'paid'){
   
           $_param = str_replace('"','',$repayment->disbursement_detail_id);
           $penalty = $repayment->penalty_amount;
   
           $arr = explode('x',$_param);
           MFS::updateChargeCompulsorySchedule($repayment->disbursement_id,$arr,$penalty);
   
           $total_service = 0;
           $payment_id = $repayment->id;
   
           $charge_amount = $request->service_charge;
           $charge_id = $request->charge_id;
           //dd($repayment);
           if ($charge_id != null){
               foreach ($charge_id as $key => $va){
                   $payment_charge = new  PaymentCharge();
                   $total_service += isset($charge_amount[$key])?$charge_amount[$key]:0;
                   $payment_charge->payment_id = $payment_id;
                   $payment_charge->charge_id = isset($charge_id[$key])?$charge_id[$key]:0;
                   $payment_charge->charge_amount = isset($charge_amount[$key])?$charge_amount[$key]:0;
                   $payment_charge->save();
               }
           }
           //dd($repayment);
   
           LoanPayment::savingTransction($repayment);
           //dd("nyi nyi");
           $loan_id = $repayment->disbursement_id;
           $principle = $repayment->principle;
           $interest = $repayment->interest;
           $saving = $repayment->compulsory_saving;
           $penalty = $repayment->penalty_amount;
           //$payment = $repayment->total_payment;
           $row = $repayment;
           $arr_charge = [];
   
   
           $acc = AccountChart::find($repayment->cash_acc_id);
   
           $depo = LoanPayment::find($repayment->id);
           $depo->total_service_charge = $total_service;
           $depo->acc_code = optional($acc)->code;
   
           $interest = $repayment->interest;
           $_principle = $repayment->principle;
           $penalty_amount = $repayment->penalty_amount;
           $_payment = $repayment->payment;
           $service = $total_service;
           $saving = $repayment->compulsory_saving;
           $loan = Loan2::find($repayment->disbursement_id);
   
           $principle_repayment = $loan->principle_repayment;
           $interest_repayment = $loan->interest_repayment;
           $principle_receivable = $loan->principle_receivable;
           $interest_receivable = $loan->interest_receivable;
   
           $loan_product = LoanProduct::find(optional($loan)->loan_production_id);
           $repayment_order = optional($loan_product)->repayment_order;
   
           //==============================================
           //==============================================
           //========================Accounting======================
           $acc = GeneralJournal::where('tran_id',$row->id)->where('tran_type','payment')->first();
           if($acc == null){
               $acc = new GeneralJournal();
           }
   
           if(companyReportPart() == "company.moeyan"){
               $branch_id = session('s_branch_id');
           }else{
               $branch_id = optional($loan)->branch_id;
           }
   
           //$acc->currency_id = $row->currency_id;
           $acc->reference_no = $row->payment_number;
           $acc->tran_reference = $row->payment_number;
           $acc->note = $row->note;
           $acc->date_general = $row->payment_date;
           $acc->tran_id = $row->id;
           $acc->tran_type = 'payment';
           $acc->branch_id = $branch_id;
           $acc->save();
   
           ///////Cash acc
           if (companyReportPart() == 'company.mkt'){
               $c_acc = new GeneralJournalDetailTem();
           }else{
               $c_acc = new GeneralJournalDetail();
           }
   
           $c_acc->journal_id = $acc->id;
           $c_acc->currency_id = $currency_id??0;
           $c_acc->exchange_rate = 1;
           $c_acc->acc_chart_id = $row->cash_acc_id;
           $c_acc->dr = $row->payment;
           $c_acc->cr = 0;
           $c_acc->j_detail_date = $row->payment_date;
           $c_acc->description = 'Payment';
           $c_acc->class_id  =  0;
           $c_acc->job_id  =  0;
           $c_acc->tran_id = $row->id;
           $c_acc->tran_type = 'payment';
   
           $c_acc->name = $row->client_id;
           $c_acc->branch_id = $branch_id;
           $c_acc->save();
   
           //==============================================
           //==============================================
           $payment = $request->payment;
           $schedule_id = 0;
   
           foreach ($arr as $s_id){
   
               $l_s = LoanCalculate::find($s_id);
               
               if($l_s != null) {
                   //dd($l_s);
                   $schedule_id = $l_s->id;
                   //================================================
                   //================================================
                   //================================================
                   if($late_rp == 1){
                       $schedule_back = new ScheduleBackup();
                       $schedule_back->loan_id = $loan_id;
                       $schedule_back->schedule_id = $l_s->id;
                       $schedule_back->payment_id = $repayment->id;
                       $schedule_back->principal_p = $l_s->principal_p;
                       $schedule_back->interest_p = $l_s->interest_p;
                       $schedule_back->penalty_p = $l_s->penalty_p;
                       $schedule_back->service_charge_p = $l_s->service_charge_p;
                       $schedule_back->balance_p = $l_s->balance_p;
                       $schedule_back->compulsory_p = $l_s->compulsory_p;
                       $schedule_back->charge_schedule = $l_s->charge_schedule;
                       $schedule_back->compulsory_schedule = $l_s->compulsory_schedule;
                       $schedule_back->total_schedule = $l_s->total_schedule;
                       $schedule_back->balance_schedule = $l_s->balance_schedule;
                       $schedule_back->penalty_schedule = $l_s->penalty_schedule;
                       $schedule_back->principle_pd = $l_s->principle_pd;
                       $schedule_back->interest_pd = $l_s->interest_pd;
                       $schedule_back->total_pd = $l_s->total_pd;
                       // $schedule_back->penalty_pd = $l_s->penalty_pd;
                       $schedule_back->service_pd = $l_s->service_pd;
                       $schedule_back->compulsory_pd = $l_s->compulsory_pd;
                       $schedule_back->count_payment = $l_s->count_payment;
                       $schedule_back->save();
                   }
                   else{
                       $schedule_back = new ScheduleBackup();
                       $schedule_back->loan_id = $loan_id;
                       $schedule_back->schedule_id = $l_s->id;
                       $schedule_back->payment_id = $repayment->id;
                       $schedule_back->principal_p = $l_s->principal_p;
                       $schedule_back->interest_p = $l_s->interest_p;
                       $schedule_back->penalty_p = $l_s->penalty_p;
                       $schedule_back->service_charge_p = $l_s->service_charge_p;
                       $schedule_back->balance_p = $l_s->balance_p;
                       $schedule_back->compulsory_p = $l_s->compulsory_p;
                       $schedule_back->charge_schedule = $l_s->charge_schedule;
                       $schedule_back->compulsory_schedule = $l_s->compulsory_schedule;
                       $schedule_back->total_schedule = $l_s->total_schedule;
                       $schedule_back->balance_schedule = $l_s->balance_schedule;
                       $schedule_back->penalty_schedule = $l_s->penalty_schedule;
                       $schedule_back->principle_pd = $l_s->principle_pd;
                       $schedule_back->interest_pd = $l_s->interest_pd;
                       $schedule_back->total_pd = $l_s->total_pd;
                       // $schedule_back->penalty_pd = $l_s->penalty_pd;
                       $schedule_back->service_pd = $l_s->service_pd;
                       $schedule_back->compulsory_pd = $l_s->compulsory_pd;
                       $schedule_back->count_payment = $l_s->count_payment;
                       $schedule_back->save();
                   }
   
                   if($l_s != Null && $pre_rp == 1){
                       $interest_d = ($repayment->interest / count($arr));
                       $l_s->old_interest = $l_s->interest_s;
                       $l_s->interest_s = $interest_d;
                       $l_s->total_s = $l_s->principal_s + $interest_d;
                       $l_s->save();
                   }
                   // elseif($l_s != Null && $pre_rp != 1 && $late_rp != 1){
                   //     $interest_d = ($repayment->interest / count($arr));
                   //     $principal_d = ($repayment->principle / count($arr));
                   //     $l_s->old_interest = $l_s->interest_s;
                   //     $l_s->interest_s = $interest_d;
                   //     $l_s->principal_s = $principal_d;
                   //     $l_s->total_s = $principal_d + $interest_d;
                   //     $l_s->save();
                   // }
                   if($l_s != Null && $late_rp == 1){
                       
                       $interest_d = $repayment->interest;
                       $principal_d = $repayment->principle;
                       $l_s->old_interest = $l_s->interest_s;
                       $l_s->interest_pd = $interest_d + $l_s->interest_pd;
                       $l_s->principle_pd = $principal_d + $l_s->principle_pd;
                       $l_s->principal_p = $principal_d + $l_s->principle_pd;
                       $l_s->interest_p = $interest_d + $l_s->interest_pd;
                       $l_s->total_p = $principal_d + $interest_d;
                       $l_s->save();
                       //dd($l_s);
                   }
                   //================================================
                   //================================================
                   //================================================
   
   
                   $balance_schedule = $l_s->balance_schedule;
                   if($payment >= $balance_schedule || $pre_rp == 1 || $late_rp == 1){
                       if($late_rp == 1){
                           //dd($repayment);
                           $pay_his =  new PaymentHistory();
                           $pay_his->payment_date = $repayment->payment_date;
                           $pay_his->loan_id = $loan_id;
                           $pay_his->schedule_id = $l_s->id;
                           $pay_his->payment_id = $repayment->id;
                           $pay_his->principal_p = $repayment->principle;
                           $pay_his->interest_p = $repayment->interest;
                           $pay_his->penalty_p = $penalty_amount;
                           $pay_his->service_charge_p = $l_s->charge_schedule - $l_s->service_pd;
                           $pay_his->compulsory_p = $l_s->compulsory_schedule - $l_s->compulsory_pd;
                           $pay_his->owed_balance = $balance_schedule - $repayment->principle - $repayment->interest;
                           $pay_his->save();
                       }
                       else{
                           $pay_his =  new PaymentHistory();
                           $pay_his->payment_date = $repayment->payment_date;
                           $pay_his->loan_id = $loan_id;
                           $pay_his->schedule_id = $l_s->id;
                           $pay_his->payment_id = $repayment->id;
                           $pay_his->principal_p = $l_s->principal_s - $l_s->principle_pd;
                           $pay_his->interest_p = $l_s->interest_s - $l_s->interest_pd;
                           $pay_his->penalty_p = $penalty_amount;
                           $pay_his->service_charge_p = $l_s->charge_schedule - $l_s->service_pd;
                           $pay_his->compulsory_p = $l_s->compulsory_schedule - $l_s->compulsory_pd;
                           $pay_his->owed_balance = 0;
                           $pay_his->save();
                       }
                       
                       ////////////////////////////////Principle Accounting//////////////////////////
                       $func_source = ACC::accFundSourceLoanProduct(optional($loan)->loan_production_id);
                       if (companyReportPart() == 'company.mkt'){
                           $c_acc = new GeneralJournalDetailTem();
                       }else{
                           $c_acc = new GeneralJournalDetail();
                       }
   
                       $c_acc->journal_id = $acc->id;
                       $c_acc->currency_id = $currency_id ?? 0;
                       $c_acc->exchange_rate = 1;
                       $c_acc->acc_chart_id = $func_source;
                       $c_acc->dr = 0;
                       if($late_rp == 1){
                           $c_acc->cr = $repayment->principle;
                       }
                       else{
                           $c_acc->cr = $l_s->principal_s - $l_s->principle_pd;
                       }
                      
                       $c_acc->j_detail_date = $row->payment_date;
                       $c_acc->description = 'Principle';
                       $c_acc->class_id = 0;
                       $c_acc->job_id = 0;
                       $c_acc->tran_id = $row->id;
                       $c_acc->tran_type = 'payment';
                       $c_acc->name = $row->client_id;
                       $c_acc->branch_id = companyReportPart() == 'company.moeyan' ? $loan->branch_id : $branch_id;
                       //dd($c_acc->cr);
                       if( $c_acc->cr >0) {
                           $c_acc->save();
                       }
                       ////////////////////////////////Principle Accounting//////////////////////////
   
                       ////////////////////////////////Interest Accounting//////////////////////////
                       if (companyReportPart() == 'company.mkt'){
                           $c_acc = new GeneralJournalDetailTem();
                       }else{
                           $c_acc = new GeneralJournalDetail();
                       }
   
                       $interest_income = ACC::accIncomeForInterestLoanProduct(optional($loan)->loan_production_id);
                       $c_acc->journal_id = $acc->id;
                       $c_acc->currency_id = $currency_id??0;
                       $c_acc->exchange_rate = 1;
                       $c_acc->acc_chart_id = $interest_income;
                       $c_acc->dr = 0;
                       if($late_rp == 1){
                           $c_acc->cr = $repayment->interest;
                       }
                       else{
                           $c_acc->cr = $l_s->interest_s - $l_s->interest_pd;
                       }
                       $c_acc->j_detail_date = $row->payment_date;
                       $c_acc->description = 'Interest Income';
                       $c_acc->class_id  =  0;
                       $c_acc->job_id  =  0;
                       $c_acc->tran_id = $row->id;
                       $c_acc->tran_type = 'payment';
                       $c_acc->name = $row->client_id;
                       $c_acc->branch_id = companyReportPart() == 'company.moeyan' ? $loan->branch_id : $branch_id;
                       if($c_acc->cr >0) {
                           $c_acc->save();
                       }
                       ////////////////////////////////Interest Accounting//////////////////////////
   
                       ////////////////////////////////Compulsory Accounting//////////////////////////
                       if (companyReportPart() == 'company.mkt'){
                           $c_acc = new GeneralJournalDetailTem();
                       }else{
                           $c_acc = new GeneralJournalDetail();
                       }
   
                       $compulsory = LoanCompulsory::where('loan_id',$loan_id)->first();
                       if($compulsory != null) {
                           $c_acc->journal_id = $acc->id;
                           $c_acc->currency_id = $currency_id ?? 0;
                           $c_acc->exchange_rate = 1;
                           $c_acc->acc_chart_id = ACC::accDefaultSavingDepositCumpulsory($compulsory->compulsory_id);
                           $c_acc->dr = 0;
                           $c_acc->cr = $l_s->compulsory_schedule - $l_s->compulsory_pd;
                           $c_acc->j_detail_date = $row->payment_date;
                           $c_acc->description = 'Saving';
                           $c_acc->class_id = 0;
                           $c_acc->job_id = 0;
                           $c_acc->tran_id = $row->id;
                           $c_acc->tran_type = 'payment';
   
                           $c_acc->name = $row->client_id;
                           $c_acc->branch_id = $branch_id;
                           if ($c_acc->cr > 0) {
                               $c_acc->save();
                           }
                       }
                       MFS::serviceChargeAcc($acc->id,$row->payment_date,$loan,$row->id,$row->client_id,$total_service);
                       
                       if (companyReportPart() == 'company.mkt'){
                           $c_acc = new GeneralJournalDetailTem();
                       }else{
                           $c_acc = new GeneralJournalDetail();
                       }
   
                       $penalty_income = ACC::accIncomeFromPenaltyLoanProduct(optional($loan)->loan_production_id);
                       $c_acc->journal_id = $acc->id;
                       $c_acc->currency_id = $currency_id??0;
                       $c_acc->exchange_rate = 1;
                       $c_acc->acc_chart_id = $penalty_income;
                       $c_acc->dr = 0;
                       // $c_acc->cr = $l_s->penalty_schedule - $l_s->penalty_pd;
                       $c_acc->cr = $penalty_amount;
                       $c_acc->j_detail_date = $row->payment_date;
                       $c_acc->description = 'Penalty Payable';
                       $c_acc->class_id  =  0;
                       $c_acc->job_id  =  0;
                       $c_acc->tran_id = $row->id;
                       $c_acc->tran_type = 'payment';
                       $c_acc->name = $row->client_id;
                       $c_acc->branch_id = companyReportPart() == 'company.moeyan' ? $loan->branch_id : $branch_id;
                       if ($c_acc->cr > 0) {
                           $c_acc->save();
                       }
                       ////////////////////////////////Penalty Accounting//////////////////////////
                       if($late_rp != 1){
                           $l_s->principal_p = $l_s->principal_s;
                           $l_s->interest_p = $l_s->interest_s;
                           $l_s->penalty_p = 0;
                           $l_s->total_p = $l_s->total_s;
                           $l_s->balance_p = 0;
                           $l_s->owed_balance_p = 0;
                           $l_s->service_charge_p = $l_s->charge_schedule;
                           $l_s->compulsory_p = $l_s->compulsory_schedule;
                           // $l_s->penalty_p = $l_s->penalty_schedule;
                           $l_s->principle_pd = $l_s->principal_s;
                           $l_s->interest_pd = $l_s->interest_s;
                           $l_s->total_pd = $l_s->total_s;
                           //$l_s->payment_pd = $balance_schedule;
                           $l_s->service_pd = $l_s->charge_schedule;
                           $l_s->compulsory_pd = $l_s->compulsory_schedule;
                           $l_s->payment_status = 'paid';
                           $l_s->save();
                       }
                       
   
                       /////////////////////////////////update loans oustanding /////////////////////
   
                       $payment = $payment - $balance_schedule;
   
                       //=================================================
   
   
                   }else{
   
                       foreach ($repayment_order as $key => $value) {
   
                           if ($key == 'Interest') {
                               ////////////////////////////////Interest Accounting//////////////////////////
                               if (companyReportPart() == 'company.mkt'){
                                   $c_acc = new GeneralJournalDetailTem();
                               }else{
                                   $c_acc = new GeneralJournalDetail();
                               }
   
                               $interest_income = ACC::accIncomeForInterestLoanProduct(optional($loan)->loan_production_id);
                               $c_acc->journal_id = $acc->id;
                               $c_acc->currency_id = $currency_id??0;
                               $c_acc->exchange_rate = 1;
                               $c_acc->acc_chart_id = $interest_income;
                               $c_acc->dr = 0;
   
                               $c_acc->j_detail_date = $row->payment_date;
                               $c_acc->description = 'Interest Income';
                               $c_acc->class_id  =  0;
                               $c_acc->job_id  =  0;
                               $c_acc->tran_id = $row->id;
                               $c_acc->tran_type = 'payment';
                               $c_acc->name = $row->client_id;
                               $c_acc->branch_id = companyReportPart() == 'company.moeyan' ? $loan->branch_id : $branch_id;
   
                               ////////////////////////////////Interest Accounting//////////////////////////
   
                               if($payment >= $l_s->interest_s - $l_s->interest_pd) {
                                   $l_s->interest_p = ($l_s->interest_s - $l_s->interest_pd);
                                   $payment = $payment - ($l_s->interest_s - $l_s->interest_pd);
                                   $c_acc->cr = $l_s->interest_s - $l_s->interest_pd;
   
   
                               }else{
                                   $l_s->interest_p = $payment;
                                   $c_acc->cr = $payment;
   
                                   $payment = 0;
                               }
   
                               if($c_acc->cr>0) {
                                   $c_acc->save();
                               }
                           }
   
                           if ($key == "Penalty") {
                               //=======================Acc Penalty============================
                               if (companyReportPart() == 'company.mkt'){
                                   $c_acc = new GeneralJournalDetailTem();
                               }else{
                                   $c_acc = new GeneralJournalDetail();
                               }
   
                               $penalty_income = ACC::accIncomeFromPenaltyLoanProduct(optional($loan)->loan_production_id);
                               $c_acc->journal_id = $acc->id;
                               $c_acc->currency_id = $currency_id??0;
                               $c_acc->exchange_rate = 1;
                               $c_acc->acc_chart_id = $penalty_income;
                               $c_acc->dr = 0;
                               $c_acc->j_detail_date = $row->payment_date;
                               $c_acc->description = 'Penalty Payable';
                               $c_acc->class_id  =  0;
                               $c_acc->job_id  =  0;
                               $c_acc->tran_id = $row->id;
                               $c_acc->tran_type = 'payment';
                               $c_acc->name = $row->client_id;
                               $c_acc->branch_id = companyReportPart() == 'company.moeyan' ? $loan->branch_id : $branch_id;
   
   
                               if($payment >= $l_s->penalty_schedule - $l_s->penalty_pd) {
                                   // $l_s->penalty_p = ($l_s->penalty_schedule - $l_s->penalty_pd);
                                   $l_s->penalty_p = $penalty_amount;
                                   // $payment = $payment - ($l_s->penalty_schedule - $l_s->penalty_pd);
                                   $payment = $payment - $penalty_amount;
                                   // $c_acc->cr = $l_s->penalty_schedule - $l_s->penalty_pd;
                                   $c_acc->cr = $penalty_amount;
   
                               }else{
                                   // $l_s->penalty_p = $payment;
                                   $l_s->penalty_p = $penalty_amount;
                                   // $c_acc->cr = $payment;
                                   $c_acc->cr = $penalty_amount;
                                   $payment = 0;
                               }
                               if($c_acc->cr>0) {
                                   $c_acc->save();
                               }
                           }
   
                           if ($key == "Service-Fee") {
                               if($payment >= $l_s->charge_schedule - $l_s->service_pd) {
                                   $l_s->service_charge_p = $l_s->charge_schedule - $l_s->service_pd;
                                   ////////////////////////////////Service Accounting//////////////////////////
                                   if($l_s->service_charge_p >0) {
                                       MFS::serviceChargeAcc($acc->id, $row->payment_date, $loan, $row->id, $row->client_id, $l_s->service_charge_p);
                                   }
                                   ////////////////////////////////Service Accounting//////////////////////////
                                   $payment = $payment -($l_s->charge_schedule - $l_s->service_pd);
                               }else{
                                   $l_s->service_charge_p = $payment;
                                   ////////////////////////////////Service Accounting//////////////////////////
                                   if($payment>0) {
                                       MFS::serviceChargeAcc($acc->id, $row->payment_date, $loan, $row->id, $row->client_id, $payment);
                                   }
                                   ////////////////////////////////Service Accounting//////////////////////////
                                   $payment = 0;
                               }
   
                           }
   
                           if ($key == "Saving") {
                               ////////////////////////////////Compulsory Accounting//////////////////////////
                               if (companyReportPart() == 'company.mkt'){
                                   $c_acc = new GeneralJournalDetailTem();
                               }else{
                                   $c_acc = new GeneralJournalDetail();
                               }
   
                               $compulsory = LoanCompulsory::where('loan_id',$loan_id)->first();
                               if($compulsory != null) {
                                   $c_acc->journal_id = $acc->id;
                                   $c_acc->currency_id = $currency_id ?? 0;
                                   $c_acc->exchange_rate = 1;
                                   $c_acc->acc_chart_id = ACC::accDefaultSavingDepositCumpulsory($compulsory->compulsory_id);
                                   $c_acc->dr = 0;
   
                                   $c_acc->j_detail_date = $row->payment_date;
                                   $c_acc->description = 'Saving';
                                   $c_acc->class_id = 0;
                                   $c_acc->job_id = 0;
                                   $c_acc->tran_id = $row->id;
                                   $c_acc->tran_type = 'payment';
   
                                   $c_acc->name = $row->client_id;
                                   $c_acc->branch_id = $branch_id;
                               }
                               ////////////////////////////////Compulsory Accounting//////////////////////////
   
                               if($payment >= $l_s->compulsory_schedule - $l_s->compulsory_pd) {
                                   $l_s->compulsory_p = $l_s->compulsory_schedule - $l_s->compulsory_pd;
                                   $payment = $payment - ($l_s->compulsory_schedule - $l_s->compulsory_pd);
                                   $c_acc->cr =  $l_s->compulsory_schedule - $l_s->compulsory_pd;
                               }else{
                                   $l_s->compulsory_p = $payment;
                                   $c_acc->cr = $payment;
                                   $payment = 0;
                               }
                               if($c_acc->cr>0) {
                                   $c_acc->save();
                               }
                           }
   
                           if ($key == "Principle") {
                               ////////////////////////////////Principle Accounting//////////////////////////
                               $func_source = ACC::accFundSourceLoanProduct(optional($loan)->loan_production_id);
   
                               if (companyReportPart() == 'company.mkt'){
                                   $c_acc = new GeneralJournalDetailTem();
                               }else{
                                   $c_acc = new GeneralJournalDetail();
                               }
   
                               $c_acc->journal_id = $acc->id;
                               $c_acc->currency_id = $currency_id ?? 0;
                               $c_acc->exchange_rate = 1;
                               $c_acc->acc_chart_id = $func_source;
                               $c_acc->dr = 0;
   
                               $c_acc->j_detail_date = $row->payment_date;
                               $c_acc->description = 'Principle';
                               $c_acc->class_id = 0;
                               $c_acc->job_id = 0;
                               $c_acc->tran_id = $row->id;
                               $c_acc->tran_type = 'payment';
                               $c_acc->name = $row->client_id;
                               $c_acc->branch_id = companyReportPart() == 'company.moeyan' ? $loan->branch_id : $branch_id;
   
                               ////////////////////////////////Principle Accounting//////////////////////////
                               if($payment >= $l_s->principal_s - $l_s->principle_pd) {
                                   $l_s->principal_p = $l_s->principal_s - $l_s->principle_pd;
                                   $payment = $payment - ($l_s->principal_s - $l_s->principle_pd);
                                   $c_acc->cr = $l_s->principal_s - $l_s->principle_pd;
                                   $loan->save();
                               }else{
                                   $l_s->principal_p = $payment;
                                   $c_acc->cr = $payment;
                                   $payment = 0;
                                   $loan->save();
                               }
   
                               if($c_acc->cr>0) {
                                   $c_acc->save();
                               }
                           }
                       }
                       if($late_rp != 1){
                           $l_s->save();
                           $l_s->principle_pd += $l_s->principal_p;
                           $l_s->interest_pd += $l_s->interest_p;
                           $l_s->total_pd += $l_s->total_p;
                           //$l_s->payment_pd += $balance_schedule;
                           $l_s->service_pd += $l_s->service_charge_p;
                           $l_s->compulsory_pd += $l_s->compulsory_p;
                           // $l_s->penalty_pd += $l_s->penalty_p;
                           $l_s->save();
                       }
                       
   
   
                       $balance_schedule = $l_s->total_schedule - $l_s->principle_pd - $l_s->interest_pd - $l_s->penalty_pd -$l_s->service_pd - $l_s->compulsory_pd;
                       $l_s->balance_schedule = $balance_schedule;
                       $l_s->count_payment = ($l_s->count_payment??0) +1;
   
                       $l_s->save();
   
   
                       ///============================================
                       ///============================================
                       ///============================================
                       if($late_rp == 1){
                           //dd($l_s->balance_schedule);
                           $pay_his =  new PaymentHistory();
                           $pay_his->payment_date = $repayment->payment_date;
                           $pay_his->loan_id = $loan_id;
                           $pay_his->schedule_id = $l_s->id;
                           $pay_his->payment_id = $repayment->id;
                           $pay_his->principal_p = $repayment->principle;
                           $pay_his->interest_p = $repayment->interest;
                           $pay_his->penalty_p = $penalty_amount;
                           $pay_his->service_charge_p = $l_s->service_charge_p;
                           $pay_his->compulsory_p = $l_s->compulsory_p;
                           $pay_his->owed_balance = $l_s->balance_schedule - $penalty;
                           $pay_his->save();
   
                       }
                       else{
                           $pay_his =  new PaymentHistory();
                           $pay_his->payment_date = $repayment->payment_date;
                           $pay_his->loan_id = $loan_id;
                           $pay_his->schedule_id = $l_s->id;
                           $pay_his->payment_id = $repayment->id;
                           $pay_his->principal_p = $l_s->principal_p;
                           $pay_his->interest_p = $l_s->interest_p;
                           $pay_his->penalty_p = $penalty_amount;
                           $pay_his->service_charge_p = $l_s->service_charge_p;
                           $pay_his->compulsory_p = $l_s->compulsory_p;
                           $pay_his->owed_balance = $l_s->balance_schedule - $penalty;
                           $pay_his->save();
                       }
   
                   }
                }
           }
   
           $data = $repayment;
           MFS::updateOutstanding($loan_id,$late_rp,$data);
   
   
           $loan_cal = LoanCalculate::where('disbursement_id',$loan_id)->where('payment_status','pending')->first();
           if($loan_cal == null){
   
                   Loan2::where('id', $loan_id)
                       ->update(['disbursement_status' => 'Closed']);
   
           }
   
           return response()->json(['status_code' => 200, 'message' => 'Repayment Create Success', 'data' => $repayment]);
           }
           else{
            $repayment->delete();
            return response()->json(['status_code' => 400, 'message' => 'Your Repayment is already activated', 'data' => NULL]);
            
           }
          
    }

    public function getDisbursements(request $request){
        $fields = $request->validate([
            'branch_id' => 'required',
           ]);
        $disbursements = DB::table('paid_disbursements')
                        ->join(getLoanTable(), 'paid_disbursements.contract_id', '=', getLoanTable().'.id')
                        ->where(getLoanTable().'.branch_id',$request->branch_id)
                        ->orderbydesc('paid_disbursements.id')
                        ->get();
        return response()->json(['status_code' => 200, 'message' => 'Disbursements Fetch Success', 'data' => $disbursements]);
    }

    public function createDisbursement(request $request){
        $fields = $request->validate([
            'branch_id' => 'required',
            'contract_id' => 'required',
            'paid_disbursement_date' => 'required',
            'first_payment_date' => 'required',
            'client_id' => 'required',
            'reference' => 'required',
            'client_name' => 'required',
            'client_nrc' => 'required',
            'invoice_no' => 'required',
            'compulsory_saving' => 'required',
            'loan_amount' => 'required',
            'total_money_disburse' => 'required',
            'cash_out_id' => 'required',
            'paid_by_tran_id' => 'required',
            'cash_pay' => 'required',
            'disburse_by' => 'required',
        ]);

        $disbursement = PaidDisbursement::create($fields);
        $total_service = 0;
        $loan = Loan2::find($disbursement->contract_id);
        $paid_disbursement_id = $disbursement->id;
        $service_charge= $request->service_amount;
        $service_charge_id= $request->service_charge_id;
        $charge_id = $request->charge_id;
        if($service_charge != null){
            foreach ($service_charge as $ke => $va){
                $deposit = new DisbursementServiceCharge();
                $total_service += isset($service_charge[$ke])?$service_charge[$ke]:0;
                $deposit->loan_disbursement_id = $paid_disbursement_id;
                $deposit->service_charge_amount = isset($service_charge[$ke])?$service_charge[$ke]:0;
                $deposit->service_charge_id = isset($service_charge_id[$ke])?$service_charge_id[$ke]:0;

                $deposit->charge_id = isset($charge_id[$ke]) ? $charge_id[$ke] : 0;
                $deposit->save();
            }
        }

        if ($disbursement->contract_id != null) {
            $l = Loan2::find($disbursement->contract_id);

            if($l != null) {
                $l_cal = LoanCalculate::where('disbursement_id',$l->id)->sum('interest_s');
                $l->status_note_date_activated = $disbursement->paid_disbursement_date;
                $l->disbursement_status = "Activated";
                $l->principle_receivable = $l->loan_amount;
                $l->interest_receivable = $l_cal??0;
                $l->save();
                LoanCycle::loanCycle($l->client_id,$l->loan_production_id,$l->id);
            }
        }

        $branch_id = optional($loan)->branch_id;
        PaidDisbursement::savingTransction($disbursement);
        PaidDisbursement::accDisburseTransaction($disbursement,$branch_id);

        $acc = AccountChart::find($disbursement->cash_out_id);

        $deburse = PaidDisbursement::find($disbursement->id);
        $deburse->total_service_charge = $total_service;
        $deburse->acc_code = optional($acc)->code;
        $deburse->save();

        LoanCalculate::where('disbursement_id',$loan->id)->delete();
        $date = $disbursement->paid_disbursement_date;
        $first_date_payment = $disbursement->first_payment_date;
        $loan_product = LoanProduct::find($loan->loan_production_id);
        $interest_method = optional($loan_product)->interest_method;
        $principal_amount = $loan->loan_amount;
        $loan_duration = $loan->loan_term_value;
        $loan_duration_unit = $loan->loan_term;
        $repayment_cycle = $loan->repayment_term;
        $loan_interest = $loan->interest_rate;
        $loan_interest_unit = $loan->interest_rate_period;
        $i = 1;
        $monthly_base = optional($loan_product)->monthly_base??'No';

        $repayment = $monthly_base== 'No' ?MFS::getRepaymentSchedule($date,$first_date_payment,$interest_method,
            $principal_amount,$loan_duration,$loan_duration_unit,$repayment_cycle,$loan_interest,$loan_interest_unit):
            MFS::getRepaymentSchedule2($monthly_base,$date,$first_date_payment,$interest_method,
                $principal_amount,$loan_duration,$loan_duration_unit,$repayment_cycle,$loan_interest,$loan_interest_unit);
        if ($repayment != null) {
            if (is_array($repayment)) {
                foreach ($repayment as $r) {
                    $d_cal = new LoanCalculate();

                    $d_cal->no = $i++;
                    $d_cal->day_num = $r['day_num'];
                    $d_cal->disbursement_id = $loan->id;
                    $d_cal->date_s = $r['date'];
                    $d_cal->principal_s = $r['principal'];
                    $d_cal->interest_s = $r['interest'];
                    $d_cal->penalty_s = 0;
                    $d_cal->service_charge_s = 0;
                    $d_cal->total_s = $r['payment'];
                    $d_cal->balance_s = $r['balance'];
                    $d_cal->branch_id = $loan->branch_id;
                    $d_cal->group_id = $loan->group_loan_id;
                    $d_cal->center_id = $loan->center_leader_id;
                    $d_cal->loan_product_id = $loan->loan_production_id;
                    $d_cal->save();
                }
            }
        }
        return response()->json(['status_code' => 200, 'message' => 'Disbursement Create Success', 'data' => $disbursements]);
    }
}
