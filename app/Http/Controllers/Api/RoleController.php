<?php

namespace App\Http\Controllers\Api;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index(Request $request){
        $search_term = $request->term;
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Role::where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }else
        {
            $results = Role::orderbydesc('id')->paginate(10);
        }

        return $results;
    }

}
