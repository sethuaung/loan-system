<?php

namespace App\Http\Controllers\Api;


use App\Models\DepositSaving;
use App\Models\Saving;
use App\Models\SavingTransaction;
use App\Models\LoanCompulsory;
use App\Models\CompulsoryAccrueInterests;
use App\Models\LoanDepositU;
use App\Models\CompulsorySavingTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Charge;
use App\Models\GeneralJournalDetail;
use App\Models\AccountChart;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DB;

class SavingController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Saving::join('clients','clients.id','savings.client_id')
                ->where('saving_number', 'LIKE', '%'.$search_term.'%')
                ->selectRaw('savings.id, savings.saving_number, clients.client_number, clients.name, clients.name_other')
                ->orderBy('savings.id','DESC')
                ->paginate(100);
        }
        else
        {
            $results = Saving::join('clients','clients.id','savings.client_id')
                ->selectRaw('savings.id, savings.saving_number, clients.client_number, clients.name, clients.name_other')
                ->orderBy('savings.id','DESC')
                ->paginate(10);
        }

        return $results;
    }

    public function show($id)
    {
        return Saving::find($id);
    }

    public function saving_ajax($id){



        $last_seq = DepositSaving::max('seq');
        $last_seq = $last_seq > 0 ?$last_seq+1:1;


        $setting = getSetting();
        $s_setting = getSettingKey('saving_deposit_no',$setting);

        $arr_setting = $s_setting != null?json_decode($s_setting,true):[];

        $saving_deposit_no = getAutoRef($last_seq, $arr_setting);

        $saving = Saving::find($id);

        $client = \App\Models\Client::find(optional($saving)->client_id);

        $client_name = $client->name??$client->name_other;

        $saving_tran_last_id = SavingTransaction::where('saving_id', $id)->max('id');
        $saving_tran_last = SavingTransaction::find($saving_tran_last_id);

        return ['saving_deposit_no'=>$saving_deposit_no,
            'saving_type' => optional($saving)->saving_type,
            'client_name' => $client_name,
            'available_balance' => optional($saving_tran_last)->available_balance-optional($saving)->minimum_balance_amount
        ];


    }

    public function caculateSaving(request $request){
        $date_now = date('Y-m-d');
        ////$end_of_month = $date_now;
        $end_of_month = self::getLastDayMonth($date_now);

        if ($date_now == $end_of_month) {
            $loan_compulsory = LoanCompulsory::where('compulsory_status', 'Active')->get();
            
            if ($loan_compulsory) {
                foreach ($loan_compulsory as $saving) {
                    $check_this_month = LoanDepositU::whereMonth('loan_deposit_date',date('m',strtotime($date_now)))
                                                        ->where('applicant_number_id',$saving->loan_id)
                                                        ->first();
                        if(!$check_this_month){
                        $deposit = LoanDepositU::where('applicant_number_id',$saving->loan_id)->first();
                        $months = Saving::getMonthListFromDate(date("Y-m",strtotime($deposit->loan_deposit_date)));
                     
                        //dd($months);
                        foreach($months as $month){
                            $lastDayofMonth =    Carbon::parse($month)->endOfMonth()->toDateString();
                            //dd($lastDayofMonth);
                            $saving_accrue_int = CompulsorySavingTransaction::where('loan_compulsory_id', $saving->id)
                                                                            ->where('train_type', 'accrue-interest')
                                                                            ->where('tran_date',$lastDayofMonth)
                                                                            ->orderBy('tran_date', 'DESC')
                                                                            ->first();
                            $accr_date = optional($saving_accrue_int)->tran_date;
                            if ($accr_date != $lastDayofMonth) {
                                $deposit = LoanDepositU::where('applicant_number_id',$saving->loan_id)->first();
                                $cashWithdrawl = \App\Models\CashWithdrawal::where('loan_id',optional($saving)->loan_id)->where('client_id',optional($saving)->client_id)->orderBy('created_at','desc')->first();
                                if($cashWithdrawl){
                                    $saving_interest_amt = ($cashWithdrawl->principle_remaining * $saving->interest_rate) / 100;
                                }
                                else{
                                    $saving_interest_amt = ($deposit->compulsory_saving_amount * $saving->interest_rate) / 100;
                                }
                                //dd($saving_interest_amt);
                                $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                                $accrue_interrest = new CompulsoryAccrueInterests();
                                $accrue_interrest->compulsory_id = $saving->compulsory_id;
                                $accrue_interrest->loan_compulsory_id = $saving->id;
                                $accrue_interrest->loan_id = $saving->loan_id;
                                $accrue_interrest->client_id = $saving->client_id;
                                $accrue_interrest->train_type = 'accrue-interest';
                                $accrue_interrest->tran_id_ref = $saving->loan_id;
                                $accrue_interrest->tran_date = $lastDayofMonth;
                                $accrue_interrest->reference = $accrue_no;
                                $accrue_interrest->amount = $saving_interest_amt;
                                $accrue_interrest->save();
                                //$accrue_interrest->seq = '';
                                if ($accrue_interrest->save()) {
                                    //dd("$accrue_interrest");
                                    CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest,$_REQUEST['branch_id']);
                                    $com_accrue = new CompulsorySavingTransaction;
                                    $com_accrue->customer_id = $deposit->client_id;
                                    $com_accrue->train_type = 'accrue-interest';
                                    $com_accrue->tran_id = $accrue_interrest->id;
                                    $com_accrue->train_type_ref = 'accrue-interest';
                                    $com_accrue->tran_id_ref = $saving->loan_id;
                                    $com_accrue->tran_date = $lastDayofMonth;
                                    $com_accrue->amount = $saving_interest_amt;
                                    $com_accrue->loan_id = $saving->loan_id;
                                    $com_accrue->loan_compulsory_id = $saving->id;
                                    $com_accrue->branch_id = $saving->branch_id;
                                    $com_accrue->save();
                                }
                            }
                        }
                        
                    }
                    
                }
               
            }
        }
        else{
            $loan_compulsory = LoanCompulsory::where('compulsory_status', 'Active')->get();
            
            if ($loan_compulsory) {
                foreach ($loan_compulsory as $saving) {
                    $check_this_month = LoanDepositU::whereMonth('loan_deposit_date',date('m',strtotime($date_now)))
                                                    ->where('applicant_number_id',$saving->loan_id)
                                                    ->first();
                    if(!$check_this_month){
                        $deposit = LoanDepositU::where('applicant_number_id',$saving->loan_id)->first();
                        $months = Saving::getMonthListFromDate(date("Y-m",strtotime($deposit->loan_deposit_date)));
                        //dd(date('Y-m',strtotime($date_now)));
                        unset($months[date('m-Y',strtotime($date_now))]);
                        //dd($months);
                        foreach($months as $month){
                            $lastDayofMonth =    Carbon::parse($month)->endOfMonth()->toDateString();
                            //dd($lastDayofMonth);
                            $saving_accrue_int = CompulsorySavingTransaction::where('loan_compulsory_id', $saving->id)
                                                                            ->where('train_type', 'accrue-interest')
                                                                            ->where('tran_date',$lastDayofMonth)
                                                                            ->orderBy('tran_date', 'DESC')
                                                                            ->first();
                            $accr_date = optional($saving_accrue_int)->tran_date;
                            //dd($acc)
                            if ($accr_date != $lastDayofMonth) {
                                // $deposit = LoanDepositU::where('applicant_number_id',$saving->loan_id)->first();
                                $cashWithdrawl = \App\Models\CashWithdrawal::where('loan_id',optional($saving)->loan_id)->where('client_id',optional($saving)->client_id)->orderBy('created_at','desc')->first();
                                if($cashWithdrawl){
                                    $saving_interest_amt = ($cashWithdrawl->principle_remaining * $saving->interest_rate) / 100;
                                }
                                else{
                                    $saving_interest_amt = ($deposit->compulsory_saving_amount * $saving->interest_rate) / 100;
                                }
                                //dd($saving_interest_amt);
                                $accrue_no = CompulsoryAccrueInterests::getSeqRef('accrue-interest');

                                $accrue_interrest = new CompulsoryAccrueInterests();
                                $accrue_interrest->compulsory_id = $saving->compulsory_id;
                                $accrue_interrest->loan_compulsory_id = $saving->id;
                                $accrue_interrest->loan_id = $saving->loan_id;
                                $accrue_interrest->client_id = $saving->client_id;
                                $accrue_interrest->train_type = 'accrue-interest';
                                $accrue_interrest->tran_id_ref = $saving->loan_id;
                                $accrue_interrest->tran_date = $lastDayofMonth;
                                $accrue_interrest->reference = $accrue_no;
                                $accrue_interrest->amount = $saving_interest_amt;
                                $accrue_interrest->save();
                                //$accrue_interrest->seq = '';
                                if ($accrue_interrest->save()) {
                                    //dd("$accrue_interrest");
                                    CompulsoryAccrueInterests::accAccurInterestCompulsory($accrue_interrest,$_REQUEST['branch_id']);
                                    $com_accrue = new CompulsorySavingTransaction;
                                    $com_accrue->customer_id = $deposit->client_id;
                                    $com_accrue->train_type = 'accrue-interest';
                                    $com_accrue->tran_id = $accrue_interrest->id;
                                    $com_accrue->train_type_ref = 'accrue-interest';
                                    $com_accrue->tran_id_ref = $saving->loan_id;
                                    $com_accrue->tran_date = $lastDayofMonth;
                                    $com_accrue->amount = $saving_interest_amt;
                                    $com_accrue->loan_id = $saving->loan_id;
                                    $com_accrue->loan_compulsory_id = $saving->id;
                                    $com_accrue->branch_id = $saving->branch_id;
                                    $com_accrue->save();
                                }
                            }
                        }
                        
                    }
                    
                }
               
            }
        }

        return "Okay";
    }

    public static function getLastDayMonth($date)
    {
        $sql = "SELECT LAST_DAY('{$date}') as d";
        $d = DB::select($sql);
        if (count($d) > 0) {
            return $d[0]->d;
        } else {
            return null;
        }
    }

    public function checkSection(Request $request){
        $details = GeneralJournalDetail::select('section_id','acc_chart_id')->where('branch_id',session('branch_id'))->get();
        foreach ($details as $detail){
            $acc = AccountChart::find($detail->acc_chart_id);
            if($detail->section_id != $acc->section_id){
                $detail->section_id = $acc->section_id;
                $detail->save();
            }
        }

        return "Success";
    }

}
